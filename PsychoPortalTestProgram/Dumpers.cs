﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using PsychoPortal;

namespace PsychoPortalTestProgram
{
	static class Helpers
	{
		public static void CreateAllDirectories(params string[] directories)
		{
			foreach ( string dir in directories )
			{
				Directory.CreateDirectory(dir);
			}
		}
	}
	class Builder
	{

	}
	class Dumper
	{
		public async Task DumpPPF(PackPack ppf, string outDir)
		{
			string tpfOut = Path.Combine(outDir, "Textures");
			string mpfOut = Path.Combine(outDir, "Meshes");
			string lpfOut = Path.Combine(outDir, "Scripts");
			string sceneOut = Path.Combine(outDir, "Levels");
			Helpers.CreateAllDirectories(outDir, tpfOut, mpfOut, lpfOut, sceneOut);

			sceneOut = Path.Combine(sceneOut, $"{Path.GetFileName(outDir)}.plb");

			

			await Task.WhenAll(
				DumpTPF(ppf.TexturePack, tpfOut)
				);

			return;
		}

		public async Task DumpLocalisedTextures(TexturePack tpf, string outDir)
		{
			for ( int i = 0; i <= (int)GameLanguage.Nonsense; i++ )
			{
				Console.WriteLine($"{(GameLanguage)i}");
				if (tpf.LocalizedTextures.Length-1 >= i)
				{
					foreach (GameTexture tex in tpf.LocalizedTextures[i].Textures)
					{
						Console.WriteLine(tex.FileName);
					}
				}
			}
		}
		public async Task DumpTPF(TexturePack tpf, string outDir)
		{
			await Task.WhenAll(DumpLocalisedTextures(tpf, outDir));
		}
	}
}
