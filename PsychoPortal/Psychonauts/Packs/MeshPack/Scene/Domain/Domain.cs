﻿namespace PsychoPortal
{
	public class Domain : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public String4b Name { get; set; }
		public Box3 Bounds { get; set; }

		public Mesh[] Meshes { get; set; }
		public EntityInitData[] EntityInitDatas { get; set; }
		public DomainEntityInfo[] DomainEntityInfos { get; set; }
		public String4b[] RuntimeReferences { get; set; }
		public Domain[] Children { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));
			Bounds = s.SerializeObject<Box3>(Bounds, name: nameof(Bounds));

			if (Pre_Version > 302)
				s.SerializeMagicString("MESH", 4, isReversed: true);

			Meshes = s.SerializeArraySize<Mesh, int>(Meshes, name: nameof(Meshes));
			Meshes = s.SerializeObjectArray<Mesh>(Meshes, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(Meshes));

			if (Pre_Version > 302)
			{
				s.SerializeMagicString("EDAT", 4, isReversed: true);

				EntityInitDatas = s.SerializeArraySize<EntityInitData, uint>(EntityInitDatas, name: nameof(EntityInitDatas));
				EntityInitDatas = s.SerializeObjectArray<EntityInitData>(EntityInitDatas, name: nameof(EntityInitDatas));
			}

			if (Pre_Version > 302)
				s.SerializeMagicString("SCRP", 4, isReversed: true);

			DomainEntityInfos = s.SerializeArraySize<DomainEntityInfo, uint>(DomainEntityInfos, name: nameof(DomainEntityInfos));
			DomainEntityInfos = s.SerializeObjectArray<DomainEntityInfo>(DomainEntityInfos, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(DomainEntityInfos));

			s.SerializeMagicString("RTRF", 4, isReversed: true);

			RuntimeReferences = s.SerializeArraySize<String4b, uint>(RuntimeReferences, name: nameof(RuntimeReferences));
			RuntimeReferences = s.SerializeObjectArray<String4b>(RuntimeReferences, name: nameof(RuntimeReferences));

			Children = s.SerializeArraySize<Domain, uint>(Children, name: nameof(Children));
			Children = s.SerializeObjectArray<Domain>(Children, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(Children));
		}
	}
}