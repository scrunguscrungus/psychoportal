﻿using System;

namespace PsychoPortal
{
	public class TextureReference : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public String4b FileName { get; set; }

		// Sets the flags in GameTexture (flag bits 1 and 4)
		public bool GameTextureFlag_1 { get; set; } // Flag bit 1
		public bool RepeatUV { get; set; } // Flag bit 4

		public string GetNormalizedTextureName(PsychonautsVersion version)
		{
			string name = FileName;

			name = name.ToLower();

			name = TrimFrom(name, "workresource");
			name = TrimFrom(name, "testresource");

			name = name.TrimStart('\\', '/');

			name = name.Replace('/', '\\');

			string fileExt = version == PsychonautsVersion.PS2 ? ".ps2" : ".dds";

			if (name.Length > 4 && name[name.Length - 4] == '.')
				name = name.Substring(0, name.Length - 4) + fileExt;

			if (name.EndsWith($"_hf{fileExt}"))
				name = name.Substring(0, name.Length - 7) + $"_norm{fileExt}";

			return name;

			static string TrimFrom(string str, string trimString)
			{
				int i = str.IndexOf(trimString, StringComparison.InvariantCulture);

				if (i == -1)
					return str;
				else
					return str.Substring(i + trimString.Length);
			}
		}

		public void Serialize(IBinarySerializer s)
		{
			FileName = s.SerializeObject<String4b>(FileName, name: nameof(FileName));
			GameTextureFlag_1 = s.Serialize<bool>(GameTextureFlag_1, name: nameof(GameTextureFlag_1));

			if (Pre_Version >= 310)
				RepeatUV = s.Serialize<bool>(RepeatUV, name: nameof(RepeatUV));
			else
				RepeatUV = true;
		}
	}
}