﻿namespace PsychoPortal
{
	public class GameTexture : IBinarySerializable
	{
		public bool Pre_HasTextureHeader { get; set; }

		// New header
		public uint DataLength { get; set; }

		// Runtime struct
		public uint Runtime_DataID { get; set; } // Overwritten during runtime
		public uint Runtime_FramesPointer { get; set; }
		public uint Runtime_PalettePointer { get; set; }
		public uint Runtime_FileNamePointer { get; set; }
		public uint Runtime_AnimInfoPointer { get; set; }
		public float Density { get; set; } // Re-calculated if -1
		public int VisualImportance { get; set; } // Re-calculated if -1
		public int MemoryImportance { get; set; } // Re-calculated if -1
		public int Runtime_ReducedMemory { get; set; }
		public uint Runtime_Flags { get; set; } // & 8 == PurgeOK, & 1 == Locked, & 4 == Procedural, & 0xff0000 is separate value
		
		// 2004 Prototype
		public uint Proto_Uint_04 { get; set; }
		public uint[] Proto_Uints_10 { get; set; }
		public uint[] Proto_Uints_24 { get; set; }
		public uint[] Proto_Uints_34 { get; set; } // Flags?

		// File data
		public String2b FileName { get; set; }
		public TexAnimInfo AnimInfo { get; set; }
		public TextureFrame[] Frames { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_HasTextureHeader)
			{
				s.SerializeMagicString("1TX ", 4, isReversed: true); // Cooky
				DataLength = s.Serialize<uint>(DataLength, name: nameof(DataLength));
			}

			Runtime_DataID = s.Serialize<uint>(Runtime_DataID, name: nameof(Runtime_DataID));

			if (s.Settings.Version == PsychonautsVersion.Xbox_Proto_20041217)
				Proto_Uint_04 = s.Serialize<uint>(Proto_Uint_04, name: nameof(Proto_Uint_04));

			Runtime_FramesPointer = s.Serialize<uint>(Runtime_FramesPointer, name: nameof(Runtime_FramesPointer));
			Runtime_PalettePointer = s.Serialize<uint>(Runtime_PalettePointer, name: nameof(Runtime_PalettePointer));

			if (s.Settings.Version == PsychonautsVersion.Xbox_Proto_20041217)
			{
				Proto_Uints_10 = s.SerializeArray<uint>(Proto_Uints_10, 4, name: nameof(Proto_Uints_10));
				Frames = s.SerializeArraySize<TextureFrame, uint>(Frames, name: nameof(Frames));
				Proto_Uints_24 = s.SerializeArray<uint>(Proto_Uints_24, 2, name: nameof(Proto_Uints_24));
			}

			Runtime_FileNamePointer = s.Serialize<uint>(Runtime_FileNamePointer, name: nameof(Runtime_FileNamePointer));
			Runtime_AnimInfoPointer = s.Serialize<uint>(Runtime_AnimInfoPointer, name: nameof(Runtime_AnimInfoPointer));

			if (s.Settings.Version == PsychonautsVersion.Xbox_Proto_20041217)
			{
				Proto_Uints_34 = s.SerializeArray<uint>(Proto_Uints_34, 2, name: nameof(Proto_Uints_34));
			}
			else
			{
				Density = s.Serialize<float>(Density, name: nameof(Density));
				VisualImportance = s.Serialize<int>(VisualImportance, name: nameof(VisualImportance));
				MemoryImportance = s.Serialize<int>(MemoryImportance, name: nameof(MemoryImportance));
				Runtime_ReducedMemory = s.Serialize<int>(Runtime_ReducedMemory, name: nameof(Runtime_ReducedMemory));
				Runtime_Flags = s.Serialize<uint>(Runtime_Flags, name: nameof(Runtime_Flags));
			}

			if (Runtime_FileNamePointer != 0)
				FileName = s.SerializeObject<String2b>(FileName, name: nameof(FileName));

			if (Runtime_AnimInfoPointer != 0)
				AnimInfo = s.SerializeObject<TexAnimInfo>(AnimInfo, name: nameof(AnimInfo));

			int framesCount = s.Settings.Version == PsychonautsVersion.Xbox_Proto_20041217 ? -1 : AnimInfo?.FramesCount ?? 1;
			Frames = s.SerializeObjectArray<TextureFrame>(Frames, framesCount, name: nameof(Frames));
		}
	}
}