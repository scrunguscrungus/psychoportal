﻿namespace PsychoPortal
{
	public class NavLink : IBinarySerializable
	{
		public uint Flags { get; set; }
		public int NavPolyIndex { get; set; }
		public String4b Name { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Flags = s.Serialize<uint>(Flags, name: nameof(Flags));
			NavPolyIndex = s.Serialize<int>(NavPolyIndex, name: nameof(NavPolyIndex));
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));
		}
	}
}