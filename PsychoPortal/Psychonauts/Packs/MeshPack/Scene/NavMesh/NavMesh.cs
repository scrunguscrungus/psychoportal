﻿namespace PsychoPortal
{
	public class NavMesh : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public NavPoly[] NavPolys { get; set; }
		public Vec3[] Vectors { get; set; }
		public int ExtraVectorsCount { get; set; }
		public Vec3[] ExtraVectors1 { get; set; }
		public Vec3[] ExtraVectors2 { get; set; }
		public int TableSize { get; set; }
		public NavMeshTable[] Tables { get; set; }
		public NavLink[] NavLinks { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			NavPolys = s.SerializeArraySize<NavPoly, int>(NavPolys, name: nameof(NavPolys));

			if (NavPolys.Length == 0)
				return;

			NavPolys = s.SerializeObjectArray<NavPoly>(NavPolys, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(NavPolys));

			Vectors = s.SerializeArraySize<Vec3, int>(Vectors, name: nameof(Vectors));
			Vectors = s.SerializeObjectArray<Vec3>(Vectors, name: nameof(Vectors));

			if (Pre_Version > 301)
			{
				ExtraVectorsCount = s.Serialize<int>(ExtraVectorsCount, name: nameof(ExtraVectorsCount));
				ExtraVectors1 = s.SerializeObjectArray<Vec3>(ExtraVectors1, ExtraVectorsCount, name: nameof(ExtraVectors1));
				ExtraVectors2 = s.SerializeObjectArray<Vec3>(ExtraVectors2, ExtraVectorsCount, name: nameof(ExtraVectors2));
			}

			if (Pre_Version >= 304)
				Tables = s.SerializeArraySize<NavMeshTable, int>(Tables, name: nameof(Tables));
			else
				Tables ??= new NavMeshTable[1];

			if (Tables.Length != 0)
			{
				TableSize = s.Serialize<int>(TableSize, name: nameof(TableSize));
				Tables = s.SerializeObjectArray<NavMeshTable>(Tables, onPreSerialize: (x, _) =>
				{
					x.Pre_Version = Pre_Version;
					x.Pre_TableSize = TableSize;
				}, name: nameof(Tables));
			}

			if (Pre_Version > 314)
			{
				NavLinks = s.SerializeArraySize<NavLink, int>(NavLinks, name: nameof(NavLinks));
				NavLinks = s.SerializeObjectArray<NavLink>(NavLinks, name: nameof(NavLinks));
			}
		}
	}
}