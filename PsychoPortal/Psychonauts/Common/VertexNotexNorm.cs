﻿namespace PsychoPortal
{
	/// <summary>
	/// A vertex and a compressed normal
	/// </summary>
	public class VertexNotexNorm : IBinarySerializable
	{
		public Vec3 Vertex { get; set; }
		public NormPacked3 Normal { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Vertex = s.SerializeObject<Vec3>(Vertex, name: nameof(Vertex));
			Normal = s.SerializeObject<NormPacked3>(Normal, name: nameof(Normal));
		}
	}
}