﻿using System;

namespace PsychoPortal
{
	// A skeleton animation consists of 3 parts:
	// - Joint animation (in a .jan file in the game package)
	// - Event animation (optional, fires entity events at certain key frames of the animation)
	// - Blend animation (optional, .pba file)

	/// <summary>
	/// A shared stub skeleton animation as can be found in an <see cref="AnimPack"/>. This contains all the data except for the
	/// joint animations, which can be found in <see cref="SharedSkelAnim"/>.
	/// </summary>
	public class StubSharedSkelAnim : IBinarySerializable
	{
		/// <summary>
		/// The file name of the .jan file in the master package. This has to be read to get the joint animations.
		/// </summary>
		public String2b JANFileName { get; set; }

		/// <summary>
		/// The .jan file offset in the master package. If set to 0xFFFFFFFF then the file table should be used to get the offset.
		/// On PS2 this is always 0xFFFFFFFF.
		/// </summary>
		public uint JANFileDataOffset { get; set; }

		/// <summary>
		/// The .jan file size in the master package. If set to 0xFFFFFFFF then the file table should be used to get the size.
		/// On PS2 this is always 0xFFFFFFFF.
		/// </summary>
		public uint JANFileSize { get; set; }

		/// <summary>
		/// The name of the root joint in the animation. This is also used to find the correct skeleton to animate if several are present.
		/// </summary>
		public String2b RootJointName { get; set; }

		/// <summary>
		/// The animation header
		/// </summary>
		public SharedSkelAnimHeader Header { get; set; }

		public SharedJointAnim_Channel PositionChannel { get; set; } // Has to be of type position. What is it for?

		// Optional chunks (currently not serialized due to blend data sometimes being broken)
		
		/// <summary>
		/// The optional event animation. This should be serialized as <see cref="PsychoPortal.EventAnim"/>.
		/// </summary>
		public byte[] EventAnim { get; set; }

		/// <summary>
		/// The optional blend animation. This should be serialized as <see cref="SharedBlendAnim"/>.
		/// </summary>
		public byte[] BlendAnim { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("ASTB", 4, isReversed: true);
			JANFileName = s.SerializeObject<String2b>(JANFileName, name: nameof(JANFileName));
			JANFileDataOffset = s.Serialize<uint>(JANFileDataOffset, name: nameof(JANFileDataOffset));
			JANFileSize = s.Serialize<uint>(JANFileSize, name: nameof(JANFileSize));
			RootJointName = s.SerializeObject<String2b>(RootJointName, name: nameof(RootJointName));
			Header = s.SerializeObject<SharedSkelAnimHeader>(Header, name: nameof(Header));

			if ((Header.Flags & 3) == 1 || (Header.Flags & 3) == 2)
			{
				if (Header.Version < 201)
					throw new Exception("Invalid version");

				PositionChannel = s.SerializeObject<SharedJointAnim_Channel>(PositionChannel, x =>
				{
					x.Pre_IsTimeUint = Header.Version == 200;
					x.Pre_HasTime = true;
				}, name: nameof(PositionChannel));
			}

			AnimChunkCooky cooky = AnimChunkCooky.End;

			if (EventAnim != null)
				cooky = AnimChunkCooky.Event;
			else if (BlendAnim != null)
				cooky = AnimChunkCooky.Blend;

			cooky = s.Serialize<AnimChunkCooky>(cooky, name: "Cooky");

			if (cooky == AnimChunkCooky.Event)
			{
				EventAnim = s.SerializeArraySize<byte, uint>(EventAnim, name: nameof(EventAnim));
				EventAnim = s.SerializeArray<byte>(EventAnim, name: nameof(EventAnim));

				cooky = AnimChunkCooky.End;

				if (BlendAnim != null)
					cooky = AnimChunkCooky.Blend;

				cooky = s.Serialize<AnimChunkCooky>(cooky, name: "Cooky");
			}

			if (cooky == AnimChunkCooky.Blend)
			{
				BlendAnim = s.SerializeArraySize<byte, uint>(BlendAnim, name: nameof(BlendAnim));
				BlendAnim = s.SerializeArray<byte>(BlendAnim, name: nameof(BlendAnim));

				cooky = AnimChunkCooky.End;
				cooky = s.Serialize<AnimChunkCooky>(cooky, name: "Cooky");
			}

			if (cooky != AnimChunkCooky.End)
				throw new Exception($"Invalid chunk header {cooky}");
		}

		private enum AnimChunkCooky : uint
		{
			Event = 0x65766e74, // evnt
			Blend = 0x626c6e64, // blnd
			End = 0x656e645f, // end_
		}
	}
}