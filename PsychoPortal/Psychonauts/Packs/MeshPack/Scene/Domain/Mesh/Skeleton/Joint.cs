﻿using System.Diagnostics;

namespace PsychoPortal
{
	[DebuggerDisplay("Joint {ID.JointIndex}: {Name.Value}")]
	public class Joint : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public JointID ID { get; set; }
		public String4b Name { get; set; }
		public Vec3 Position { get; set; }
		public Vec3 Rotation { get; set; }
		public ushort UnknownUshort { get; set; } // Always 0?
		public Joint[] Children { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			ID = s.SerializeObject<JointID>(ID, name: nameof(ID));
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));
			Position = s.SerializeObject<Vec3>(Position, name: nameof(Position));
			Rotation = s.SerializeObject<Vec3>(Rotation, name: nameof(Rotation));

			if (Pre_Version > 294)
				UnknownUshort = s.Serialize<ushort>(UnknownUshort, name: nameof(UnknownUshort));

			Children = s.SerializeArraySize<Joint, uint>(Children, name: nameof(Children));
			Children = s.SerializeObjectArray<Joint>(Children, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(Children));
		}
	}
}