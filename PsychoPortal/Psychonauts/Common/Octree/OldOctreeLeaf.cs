﻿namespace PsychoPortal
{
	public class OldOctreeLeaf : IBinarySerializable
	{
		// TODO: Starts with a cube (Vec3 + float)
		public byte[] Bytes_00 { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Bytes_00 = s.SerializeArray<byte>(Bytes_00, 0x28, name: nameof(Bytes_00));
		}
	}
}