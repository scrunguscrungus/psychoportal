﻿namespace PsychoPortal
{
	// Blend shape related?
	public class VertexStreamBasis : IBinarySerializable
	{
		public NormPacked3 NormPacked1 { get; set; }
		public NormPacked3 NormPacked2 { get; set; }
		public NormPacked3 NormPacked3 { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			NormPacked1 = s.SerializeObject<NormPacked3>(NormPacked1, name: nameof(NormPacked1));
			NormPacked2 = s.SerializeObject<NormPacked3>(NormPacked2, name: nameof(NormPacked2));
			NormPacked3 = s.SerializeObject<NormPacked3>(NormPacked3, name: nameof(NormPacked3));
		}
	}
}