﻿namespace PsychoPortal
{
	public class EntityMeshInfo : IBinarySerializable
	{
		public String4b ScriptClass { get; set; }
		public String4b EditVars { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			ScriptClass = s.SerializeObject<String4b>(ScriptClass, name: nameof(ScriptClass));
			EditVars = s.SerializeObject<String4b>(EditVars, name: nameof(EditVars));
		}
	}
}