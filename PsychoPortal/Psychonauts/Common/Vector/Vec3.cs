﻿namespace PsychoPortal
{
	/// <summary>
	/// A 3-dimensional vector
	/// </summary>
	public class Vec3 : IBinarySerializable, IBinaryShortLog
	{
		public Vec3() { }
		public Vec3(float x, float y, float z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public float X { get; set; }
		public float Y { get; set; }
		public float Z { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => $"Vec3({X}, {Y}, {Z})";

		public void Serialize(IBinarySerializer s)
		{
			X = s.Serialize<float>(X, name: nameof(X));
			Y = s.Serialize<float>(Y, name: nameof(Y));
			Z = s.Serialize<float>(Z, name: nameof(Z));
		}
	}
}