﻿using System;

namespace PsychoPortal
{
	[Flags]
	public enum MaterialFlags
	{
		None = 0,
		Alpha = 1 << 0,
		Incandescent = 1 << 1,
		Skinned = 1 << 2,
		Bumpmap = 1 << 3,
		DoubleSided = 1 << 4,
		Lightmap = 1 << 5,
		Flag_6 = 1 << 6, // Uses vertex colors
		Billboard = 1 << 7,
		FlatBillboard = 1 << 8,
		AdditiveBlending = 1 << 9,
		BinaryAlpha = 1 << 10,
		Decal = 1 << 11,
		Flag_12 = 1 << 12,
		EdgeAlpha = 1 << 13,
		InvEdgeAlpha = 1 << 14,
		Flag_15 = 1 << 15, // Uses vertex colors
		Distortion = 1 << 16,
		Specular = 1 << 17,
		ENVMap = 1 << 18,
		Flag_19 = 1 << 19,
		Flag_20 = 1 << 20,
		FresnelReflection = 1 << 21,
		GlareOnly = 1 << 22,
		PerPixelLighting = 1 << 23,
		Tristrip = 1 << 24,
		Flag_25 = 1 << 25,
		Flag_26 = 1 << 26,
		Flag_27 = 1 << 27,
		Flag_28 = 1 << 28,
		Flag_29 = 1 << 29,
		DetailTexture = 1 << 30,
		Flag_31 = 1 << 31,
	}
}