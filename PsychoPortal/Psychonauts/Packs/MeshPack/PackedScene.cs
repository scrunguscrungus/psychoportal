﻿namespace PsychoPortal
{
	public class PackedScene : IBinarySerializable
	{
		public String2b FileName { get; set; } // PLB file name
		public PLBType Type { get; set; }
		public Scene Scene { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			FileName = s.SerializeObject<String2b>(FileName, name: nameof(FileName));
			Type = s.Serialize<PLBType>(Type, name: nameof(Type));
			s.DoWithPrefixedSize<uint>(_ => Scene = s.SerializeObject<Scene>(Scene, name: nameof(Scene)));
		}
	}
}