﻿namespace PsychoPortal
{
	public class String2b : BaseString
	{
		public static implicit operator String2b(string value) => new() { Value = value };

		public override void Serialize(IBinarySerializer s)
		{
			ushort ValueLength = s.Serialize<ushort>(Value != null ? (ushort)(Value.Length+1) : (ushort)0, name: nameof(ValueLength));
			Value = s.SerializeString(Value, ValueLength, name: nameof(Value));
		}
	}
}