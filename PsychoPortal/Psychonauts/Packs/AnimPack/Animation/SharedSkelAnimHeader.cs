﻿using System;
using System.Linq;

namespace PsychoPortal
{
	public class SharedSkelAnimHeader : IBinarySerializable
	{
		public string Magic { get; set; } // PJAX (older versions use PJAN)
		public uint Version { get; set; }
		public byte[] UnusedBytes1 { get; set; }
		public byte[] UnusedBytes2 { get; set; }
		public byte JointsCount { get; set; }
		public byte Value2 { get; set; }
		public byte Value3 { get; set; }
		public byte Value4 { get; set; }
		public uint Flags => Version == 200 ? V200_Flags : (uint)(V201_Flags | V201_TranslateMode);

		// 200
		public int V200_ConstPosKeys { get; set; }
		public int V200_ConstRotKeys { get; set; }
		public int V200_ConstScaleKeys { get; set; }
		public uint V200_Flags { get; set; }

		// 201
		public ushort V201_Flags { get; set; }
		public ushort V201_AnimLength { get; set; }
		public byte V201_TranslateMode { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Magic = s.SerializeString(Magic, 4, name: nameof(Magic));

			if (Magic != "XAJP" && Magic != "NAJP") // Reversed
				throw new Exception($"Invalid animation magic header {Magic}");

			Version = s.Serialize<uint>(Version, name: nameof(Version));

			if (Version is 0 or 1) // Unsupported by the game, but a few files still use it
			{
				// No header
			}
			else if (Version == 100) // Unsupported by the game, but a few files still use it
			{
				UnusedBytes1 = s.SerializeArray<byte>(UnusedBytes1, 12, name: nameof(UnusedBytes1));
			}
			else if (Version == 200)
			{
				UnusedBytes1 = s.SerializeArray<byte>(UnusedBytes1, 28, name: nameof(UnusedBytes1));
				JointsCount = s.Serialize<byte>(JointsCount, name: nameof(JointsCount));
				s.SerializePadding(3);
				Value2 = s.Serialize<byte>(Value2, name: nameof(Value2));
				s.SerializePadding(3);
				Value3 = s.Serialize<byte>(Value3, name: nameof(Value3));
				s.SerializePadding(3);
				Value4 = s.Serialize<byte>(Value4, name: nameof(Value4));
				s.SerializePadding(3);
				UnusedBytes2 = s.SerializeArray<byte>(UnusedBytes2, 12, name: nameof(UnusedBytes2)); // TODO: 3 32-bit values
				V200_ConstPosKeys = s.Serialize<int>(V200_ConstPosKeys, name: nameof(V200_ConstPosKeys));
				V200_ConstRotKeys = s.Serialize<int>(V200_ConstRotKeys, name: nameof(V200_ConstRotKeys));
				V200_ConstScaleKeys = s.Serialize<int>(V200_ConstScaleKeys, name: nameof(V200_ConstScaleKeys));
				V200_Flags = s.Serialize<uint>(V200_Flags, name: nameof(V200_Flags));
			}
			else if (Version == 201)
			{
				V201_Flags = s.Serialize<ushort>(V201_Flags, name: nameof(V201_Flags));
				V201_AnimLength = s.Serialize<ushort>(V201_AnimLength, name: nameof(V201_AnimLength));
				V201_TranslateMode = s.Serialize<byte>(V201_TranslateMode, name: nameof(V201_TranslateMode));
				JointsCount = s.Serialize<byte>(JointsCount, name: nameof(JointsCount));
				Value2 = s.Serialize<byte>(Value2, name: nameof(Value2));
				Value3 = s.Serialize<byte>(Value3, name: nameof(Value3));
				Value4 = s.Serialize<byte>(Value4, name: nameof(Value4));
				UnusedBytes1 = s.SerializeArray<byte>(UnusedBytes1, 1, name: nameof(UnusedBytes1));
			}
			else
			{
				throw new Exception($"Unsupported animation version {Version}");
			}
		}
	}
}