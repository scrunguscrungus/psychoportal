﻿using System;

namespace PsychoPortal
{
	internal static class MathHelpers
	{
		public const float Deg2Rad = (float)Math.PI / 180f;
		public const float Rad2Deg = 57.29578f;

		public static float Abs(float v) => Math.Abs(v);

		public static float ArcTan(float y, float x)
		{
			return NormalizeAngle((float)Math.Atan2(y, x) * Rad2Deg);
		}

		public static float NormalizeAngle(float v)
		{
			if (540.0 < v || v <= -540.0)
				v %= 360;

			if (180.0 < v)
				v -= 360.0f;

			if (v <= -180.0)
				v += 360.0f;

			return v;
		}
	}
}