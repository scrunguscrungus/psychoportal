﻿using System.Linq;

namespace PsychoPortal
{
	// Incorrect name
	public class LightExtraDataEntry : IBinarySerializable
	{
		public Light.LightType Pre_Type { get; set; }
		public ushort Pre_Ushort1 { get; set; }
		public ushort PlanesCount { get; set; }

		public ushort[] Data1 { get; set; }
		public PlanePacked3[] PackedPlanes { get; set; }
		public Plane[] Planes { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (PlanesCount < 0x101)
				Data1 = s.SerializeArray<byte>(Data1?.Select(x => (byte)x).ToArray(), Pre_Ushort1, name: nameof(Data1))?.
					Select(x => (ushort)x).ToArray();
			else
				Data1 = s.SerializeArray<ushort>(Data1, Pre_Ushort1, name: nameof(Data1));

			if (Pre_Type != Light.LightType.Directional)
				PackedPlanes = s.SerializeObjectArray<PlanePacked3>(PackedPlanes, PlanesCount, name: nameof(PackedPlanes));
			else
				Planes = s.SerializeObjectArray<Plane>(Planes, PlanesCount, name: nameof(Planes));
		}
	}
}