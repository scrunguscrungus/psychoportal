﻿namespace PsychoPortal
{
	public class EntityInitData : IBinarySerializable
	{
		public LineCollision LineCollision { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			// Note: The game allows for multiple "types" of init data identified by this, however only SPLN is used
			s.SerializeMagicString("SPLN", 4, isReversed: true);

			s.DoWithPrefixedSize<uint>(_ => 
				LineCollision = s.SerializeObject<LineCollision>(LineCollision, name: nameof(LineCollision)));
		}
	}
}