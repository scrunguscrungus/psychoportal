﻿using System;
using System.Linq;
using System.Text;

namespace PsychoPortal
{
	/// <summary>
	/// A serializer which doesn't do anything besides log the data and keep track of the position
	/// </summary>
	public class BinaryDummySerializer : IBinarySerializer
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="settings">The settings to use when serializing</param>
		/// <param name="logger">An optional logger to use for logging</param>
		public BinaryDummySerializer(PsychonautsSettings settings, IBinarySerializerLogger logger = null)
		{
			Settings = settings;
			Logger = logger;

			DefaultEncoding = Encoding.UTF8;
		}

		private const string DefaultName = "<no name>";

		/// <summary>
		/// Writes a supported value to the stream
		/// </summary>
		/// <param name="value">The value</param>
		public void Write<T>(T value)
		{
			if (value is byte[] ba)
				Position += ba.Length;

			else if (value is Array a)
				foreach (var item in a)
					Write(item);

			else if (value?.GetType().IsEnum == true)
				Write(Convert.ChangeType(value, Enum.GetUnderlyingType(value.GetType())));

			else if (value is bool)
				Position += 1;

			else if (value is sbyte)
				Position += 1;

			else if (value is byte)
				Position += 1;

			else if (value is short)
				Position += 2;

			else if (value is ushort)
				Position += 2;

			else if (value is int)
				Position += 4;

			else if (value is uint)
				Position += 4;

			else if (value is long)
				Position += 8;

			else if (value is ulong)
				Position += 8;

			else if (value is float)
				Position += 4;

			else if (value is double)
				Position += 8;

			else if (value is string s)
				Position += DefaultEncoding.GetByteCount(s) + 1;

			else if (value is UInt24)
				Position += 3;

			else if (Nullable.GetUnderlyingType(typeof(T)) != null)
			{
				// It's nullable
				Type underlyingType = Nullable.GetUnderlyingType(typeof(T));

				if (underlyingType == typeof(byte))
					Position += 1;
				else
					throw new NotSupportedException($"The specified type {typeof(T)} is not supported.");
			}

			else if (value is null)
				throw new ArgumentNullException(nameof(value));

			else
				throw new NotSupportedException($"The specified type {value.GetType().Name} is not supported.");
		}
		
		/// <summary>
		/// Serializes a value
		/// </summary>
		/// <typeparam name="T">The value type</typeparam>
		/// <param name="value">The value</param>
		/// <param name="name">The value name, for logging</param>
		/// <returns>The serialized value</returns>
		public T Serialize<T>(T value, string name = null)
		{
			if (IsLogEnabled)
				Logger.WriteLogLine($"{LogPrefix}({typeof(T)}) {name ?? DefaultName}: {value}");

			// Write the value
			Write(value);

			// Return the value
			return value;
		}

		/// <summary>
		/// Serializes the size of an array
		/// </summary>
		/// <typeparam name="T">The array value type</typeparam>
		/// <typeparam name="U">The size value type</typeparam>
		/// <param name="array">The array</param>
		/// <param name="name">The array name, for logging</param>
		/// <returns>The serialized array</returns>
		public T[] SerializeArraySize<T, U>(T[] array, string name = null)
		{
			U Size = (U)Convert.ChangeType(array?.Length ?? 0, typeof(U));
			Serialize<U>(Size, name: $"{name}.Length");
			return array;
		}

		/// <summary>
		/// Serializes an array
		/// </summary>
		/// <typeparam name="T">The array value type</typeparam>
		/// <param name="array">The array</param>
		/// <param name="length">The array length</param>
		/// <param name="name">The array name, for logging</param>
		/// <returns>The serialized array</returns>
		public T[] SerializeArray<T>(T[] array, long length = -1, string name = null)
		{
			if (length == -1)
				length = array?.Length ?? 0;

			T[] buffer = GetArray(array, length);
			length = buffer.Length;

			// Use byte writing method if it's a byte array
			if (typeof(T) == typeof(byte))
			{
				if (IsLogEnabled)
				{
					string normalLog = $"{LogPrefix}({typeof(T)}[{length}]) {name ?? DefaultName}: ";
					Logger.WriteLogLine($"{normalLog}{((byte[])(object)buffer).ToHexString(16, new string(' ', normalLog.Length), maxLines: 10)}");
				}

				Position += buffer.Length;
			}
			else
			{
				if (IsLogEnabled)
					Logger.WriteLogLine($"{LogPrefix}({typeof(T)}[{length}]) {name ?? DefaultName}");

				// Write every value
				for (int i = 0; i < length; i++)
					Serialize<T>(buffer[i], name: name == null || !IsLogEnabled ? null : $"{name}[{i}]");
			}

			// Return the array
			return buffer;
		}

		/// <summary>
		/// Serializes a string of a specified length
		/// </summary>
		/// <param name="value">The string value</param>
		/// <param name="length">The string length</param>
		/// <param name="encoding">The string encoding to use, or null for the default one</param>
		/// <param name="name">The string value name, for logging</param>
		/// <returns>The serialized string value</returns>
		public string SerializeString(string value, long length, Encoding encoding = null, string name = null)
		{
			if (IsLogEnabled)
				Logger.WriteLogLine($"{LogPrefix}(string) {name ?? DefaultName}: {value}");

			// Write the string
			Position += length;

			// Return the value
			return value;
		}

		/// <summary>
		/// Serializes a serializable object
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="value">The serializable object</param>
		/// <param name="onPreSerialize">Optional action to run before serializing</param>
		/// <param name="name">The object value name, for logging</param>
		/// <returns>The serialized object</returns>
		public T SerializeObject<T>(T value, Action<T> onPreSerialize = null, string name = null) 
			where T : IBinarySerializable, new()
		{
			string logPrefix = LogPrefix;

			bool isLogTemporarilyDisabled = false;
			if (!DisableLogForObject && value is IBinaryShortLog)
			{
				DisableLogForObject = true;
				isLogTemporarilyDisabled = true;
			}

			if (IsLogEnabled)
				Logger.WriteLogLine($"{logPrefix}(Object: {typeof(T)}) {name ?? DefaultName}");

			Depth++;

			// Run pre-serializing method
			onPreSerialize?.Invoke(value);

			// Serialize the value
			value.Serialize(this);

			Depth--;

			if (isLogTemporarilyDisabled && value is IBinaryShortLog log)
			{
				DisableLogForObject = false;

				if (IsLogEnabled)
					Logger.WriteLogLine($"{logPrefix}({typeof(T)}) {name ?? DefaultName}: {log.ShortLog ?? "null"}");
			}

			// Return the value
			return value;
		}

		/// <summary>
		/// Serializes an array of serializable objects
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="array">The serializable object array</param>
		/// <param name="length">The array length</param>
		/// <param name="onPreSerialize">Optional action to run before serializing each object</param>
		/// <param name="name">The object value name, for logging</param>
		/// <returns>The serialized object array</returns>
		public T[] SerializeObjectArray<T>(T[] array, long length = -1, Action<T, int> onPreSerialize = null, string name = null)
			where T : IBinarySerializable, new()
		{
			if (length == -1)
				length = array?.Length ?? 0;

			T[] buffer = GetArray(array, length);
			length = buffer.Length;

			if (IsLogEnabled)
				Logger.WriteLogLine($"{LogPrefix}(Object[] {typeof(T)}[{length}]) {name ?? DefaultName}");

			// Write every value
			for (int i = 0; i < length; i++)
				SerializeObject<T>(array[i], onPreSerialize: onPreSerialize != null ? x => onPreSerialize(x, i) : null, name: name == null || !IsLogEnabled ? null : $"{name}[{i}]");

			// Return the array
			return array;
		}

		/// <summary>
		/// Serializes an object array of undefined size until a specified condition is met
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="obj">The object array</param>
		/// <param name="conditionCheckFunc">The condition for ending the array serialization</param>
		/// <param name="getLastObjFunc">If specified the last object when read will be ignored and this will be used to prepend an object when writing</param>
		/// <param name="onPreSerialize">Optional action to run before serializing</param>
		/// <param name="name">The name</param>
		/// <returns>The object array</returns>
		public T[] SerializeObjectArrayUntil<T>(T[] obj, Func<T, int, bool> conditionCheckFunc, Func<T> getLastObjFunc = null,
			Action<T, int> onPreSerialize = null, string name = null)
			where T : IBinarySerializable, new()
		{
			T[] array = obj;

			if (getLastObjFunc != null)
				array = array.Append(getLastObjFunc()).ToArray();

			SerializeObjectArray<T>(array, array.Length, onPreSerialize: onPreSerialize, name: name);

			return obj;
		}

		/// <summary>
		/// Serializes bit field values
		/// </summary>
		/// <typeparam name="T">The integer value type</typeparam>
		/// <param name="serializeAction">The serializer action</param>
		public void DoBits<T>(Action<IBitSerializer> serializeAction)
		{
			BitDummySerializer serializer = new(this, LogPrefix, 0);

			// Set bits
			serializeAction(serializer);

			// Serialize value
			Serialize<T>((T)Convert.ChangeType(serializer.Value, typeof(T)), name: "Value");
		}

		protected T[] GetArray<T>(T[] obj, long count)
		{
			// Create or resize array if necessary
			return obj ?? new T[count];
		}

		/// <summary>
		/// Seeks to the specified stream position
		/// </summary>
		/// <param name="position">The position to go to</param>
		public void GoTo(long position)
		{
			Position = position;
		}

		private bool DisableLogForObject { get; set; }

		public bool IsLogEnabled => !DisableLogForObject && Logger is { IsActive: true };

		/// <summary>
		/// The current stream position
		/// </summary>
		public long Position { get; private set; }

		/// <summary>
		/// The current stream length
		/// </summary>
		public long Length => 0;

		/// <summary>
		/// The settings to use when serializing
		/// </summary>
		public PsychonautsSettings Settings { get; }

		/// <summary>
		/// An optional logger to use for logging
		/// </summary>
		public IBinarySerializerLogger Logger { get; }

		/// <summary>
		/// The default string encoding to use
		/// </summary>
		public Encoding DefaultEncoding { get; set; }

		/// <summary>
		/// The log prefix to use when writing a log
		/// </summary>
		protected string LogPrefix => !IsLogEnabled ? null : $"0x{Position:X8}:{new string(' ', (Depth + 1) * 2)}";

		/// <summary>
		/// The depths, for logging
		/// </summary>
		protected int Depth { get; set; }
	}
}