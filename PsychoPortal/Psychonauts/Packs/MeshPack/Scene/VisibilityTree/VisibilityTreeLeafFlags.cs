﻿namespace PsychoPortal
{
	public class VisibilityTreeLeafFlags : IBinarySerializable
	{
		public long Pre_Count { get; set; }

		public uint[] Buffer { get; set; } // Bit array

		public void Serialize(IBinarySerializer s)
		{
			Buffer = s.SerializeArray<uint>(Buffer, Pre_Count, name: nameof(Buffer));
		}
	}
}