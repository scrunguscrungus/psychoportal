﻿namespace PsychoPortal
{
	/// <summary>
	/// A blend animation channel for animating one blend shape
	/// </summary>
	public class SharedBlendAnim_Channel : IBinarySerializable
	{
		/// <summary>
		/// The name, matching to <see cref="BlendshapeStream.BlendMappingName"/>. If multiple mesh frags share the same name then all of them
		/// should be animated as they're part of the same blend shape then.
		/// </summary>
		public String1b Name { get; set; }

		/// <summary>
		/// The blend weight (0-255) for each frame of the animation in 30 fps
		/// </summary>
		public byte[] AnimData { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String1b>(Name, name: nameof(Name));
			AnimData = s.SerializeArraySize<byte, uint>(AnimData, name: nameof(AnimData));
			AnimData = s.SerializeArray<byte>(AnimData, name: nameof(AnimData));
		}
	}
}