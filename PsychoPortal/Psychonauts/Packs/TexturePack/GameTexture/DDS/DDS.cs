﻿namespace PsychoPortal
{
	public class DDS : IBinarySerializable
	{
		public DDS_Header Header { get; set; }
		public byte[] Palette { get; set; }
		public TextureFace[] Faces { get; set; }

		public bool IsCubeMap => Header.Caps2.HasFlag(DDS_Header.DDS_Caps2Flags.DDS_CUBEMAP_ALLFACES);

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagic(0x20534444);

			Header = s.SerializeObject<DDS_Header>(Header, name: nameof(Header));

			TextureFormat format = Header.PixelFormat.GetTextureFormat();

			if (format == TextureFormat.Format_P8)
				Palette = s.SerializeArray<byte>(Palette, 4 * 256, name: nameof(Palette));

			Faces = s.SerializeObjectArray(Faces, IsCubeMap ? 6 : 1, (x, _) =>
			{
				x.Pre_Format = format;
				x.Pre_Width = Header.Width;
				x.Pre_Height = Header.Height;
				x.Pre_MipMapLevels = Header.GetMipMapCount;
			}, name: nameof(Faces));
		}
	}
}