﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PsychoPortal
{
	public class PS2_FileTable : IBinarySerializable
	{
		public uint Uint_08 { get; set; } // 64
		public PS2_FileEntry[] Files { get; set; }

		public PS2_FileEntry GetFileEntry(string filePath)
		{
			// Get the file path hash
			uint hash = PS2_FileEntry.GetFilePathHash(filePath);

			// Return the matching file or null if none is found
			return Files.FirstOrDefault(x => x.FilePathHash == hash);
		}

		public void ExportFiles(string outputDir, string[] fileNames, params Stream[] resourcePaks) =>
			ExportFiles(outputDir, fileNames.ToDictionary(PS2_FileEntry.GetFilePathHash), resourcePaks);

		public void ExportFiles(string outputDir, Dictionary<uint, string> fileNames, params Stream[] resourcePaks)
		{
			foreach (PS2_FileEntry file in Files)
			{
				string name = fileNames.TryGetValue(file.FilePathHash, out string n) ? n : @$"{file.FilePathHash:X8}";

				byte[] fileBuffer = file.ReadFile(resourcePaks);

				if (fileBuffer == null)
					continue;

				string outputFile = Path.Combine(outputDir, name);

				Directory.CreateDirectory(Path.GetDirectoryName(outputFile));
				File.WriteAllBytes(outputFile, fileBuffer);
			}
		}

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagic<uint>(0xFEFE0003);
			Files = s.SerializeArraySize<PS2_FileEntry, uint>(Files, name: nameof(Files));
			Uint_08 = s.Serialize<uint>(Uint_08, name: nameof(Uint_08));
			Files = s.SerializeObjectArray<PS2_FileEntry>(Files, name: nameof(Files));
		}
	}
}