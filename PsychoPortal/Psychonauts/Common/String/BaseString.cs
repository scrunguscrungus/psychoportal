﻿namespace PsychoPortal
{
	public abstract class BaseString : IBinarySerializable, IBinaryShortLog
	{
		public string Value { get; set; }

		public static implicit operator string(BaseString path) => path?.Value;

		public string ShortLog => ToString();
		public override string ToString() => Value;

		public abstract void Serialize(IBinarySerializer s);
	}
}