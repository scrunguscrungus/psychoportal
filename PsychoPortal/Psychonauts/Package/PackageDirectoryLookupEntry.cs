﻿namespace PsychoPortal
{
	public class PackageDirectoryLookupEntry : IBinarySerializable
	{
		public byte NameLetter { get; set; }
		public char NameLetterChar => (char)NameLetter;

		public ushort IndexLess { get; set; }
		public ushort IndexGreater { get; set; }
		public ushort IndexSame { get; set; } // Always this index + 1

		// The range of the files in this directory
		public ushort FilesStartIndex { get; set; }
		public ushort FilesEndIndex { get; set; }

		public bool HasFiles => FilesStartIndex != FilesEndIndex;

		public void Serialize(IBinarySerializer s)
		{
			NameLetter = s.Serialize<byte>(NameLetter, name: nameof(NameLetter));
			s.SerializePadding(1);
			IndexLess = s.Serialize<ushort>(IndexLess, name: nameof(IndexLess));
			IndexGreater = s.Serialize<ushort>(IndexGreater, name: nameof(IndexGreater));
			IndexSame = s.Serialize<ushort>(IndexSame, name: nameof(IndexSame));
			FilesStartIndex = s.Serialize<ushort>(FilesStartIndex, name: nameof(FilesStartIndex));
			FilesEndIndex = s.Serialize<ushort>(FilesEndIndex, name: nameof(FilesEndIndex));
		}
	}
}