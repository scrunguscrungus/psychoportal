﻿using System.IO;
using System.Text;

namespace PsychoPortal
{
	public class PS2_FileEntry : IBinarySerializable
	{
		public uint FilePathHash { get; set; }
		public uint FileOffset { get; set; } // Do * 4 to get actual offset. Can overflow into RESOURC2.PAK file.
		public int FileSize { get; set; }

		public long AbsoluteOffset => FileOffset * 4;

		public static uint GetFilePathHash(string str) => GetFilePathHash(Encoding.UTF8.GetBytes(str), 0);

		public static uint GetFilePathHash(byte[] buffer, int index)
		{
			int var2 = 0;
			uint var3 = 0;

			byte b = index >= buffer.Length ? (byte)0 : buffer[index];

			// Trim any leading slashes
			while (b is 0x2f or 0x5c)
			{
				index++;
				b = index >= buffer.Length ? (byte)0 : buffer[index];
			}

			// Enumerate the bytes until we reach the end or null
			if (b != 0)
			{
				do
				{
					var2 = (b + 0x20) * 0x1000000 >> 0x18;

					// Normalize casing
					if (0x19 < ((b - 0x41) & 0xff))
						var2 = (char)b;

					// Normalize slash
					if (var2 == 0x5c)
						var2 = 0x2f;

					var3 = (uint)(var3 + var2) * 0x401;

					index++;
					b = index >= buffer.Length ? (byte)0 : buffer[index];

					var3 ^= var3 >> 6;
				} while (b != 0);

				var2 = (int)(var3 << 3);
			}

			return (uint)((int)(var3 + var2 ^ (uint)(var3 + var2) >> 0xb) * 0x8001);
		}

		public byte[] ReadFile(params Stream[] resourcePaks) => ReadFile(resourcePaks, FileSize);

		public byte[] ReadFile(Stream[] resourcePaks, long length)
		{
			// TODO: Why are some sizes negative? Game seems to ignore them?
			if (length < 0)
				return null;

			long offset = AbsoluteOffset;

			int resourcePakIndex = 0;

			while (offset >= resourcePaks[resourcePakIndex].Length)
			{
				offset -= resourcePaks[resourcePakIndex].Length;
				resourcePakIndex++;
			}

			byte[] fileBuffer = new byte[length];
			int read = 0;

			while (read < fileBuffer.Length)
			{
				if (resourcePakIndex >= resourcePaks.Length)
					throw new IOException($"The file at 0x{AbsoluteOffset:X8} with size {length} is outside the range of the provided resource paks");

				Stream resourceFile = resourcePaks[resourcePakIndex];
				resourceFile.Position = offset;
				read += resourceFile.Read(fileBuffer, read, fileBuffer.Length - read);
				resourcePakIndex++;
				offset = 0;
			}

			return fileBuffer;
		}

		public void Serialize(IBinarySerializer s)
		{
			FilePathHash = s.Serialize<uint>(FilePathHash, name: nameof(FilePathHash));
			FileOffset = s.Serialize<uint>(FileOffset, name: nameof(FileOffset));
			FileSize = s.Serialize<int>(FileSize, name: nameof(FileSize));
		}
	}
}