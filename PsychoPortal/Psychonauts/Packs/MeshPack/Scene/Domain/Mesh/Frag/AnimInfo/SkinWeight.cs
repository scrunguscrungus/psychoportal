﻿namespace PsychoPortal
{
	public class SkinWeight : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public int Unknown { get; set; } // Join4?
		public int Joint1 { get; set; }
		public int Joint2 { get; set; }
		public int Joint3 { get; set; }
		public Vec2 Weight { get; set; } // In the final game only X is used. Y is always 1 - X.
		public Vec2 Proto_AdditionalWeights { get; set; } // Probably for Joint3 and Joint4

		public void Serialize(IBinarySerializer s)
		{
			// TODO: In the 2004 prototype the game has hacky code for converting pre-308 PLB code
			//       to newer format as it allowed more joints per vertex back then
			s.DoBits<uint>(b =>
			{
				Joint3 = b.SerializeBits<int>(Joint3, 8, name: nameof(Joint3));
				Joint2 = b.SerializeBits<int>(Joint2, 8, name: nameof(Joint2));
				Joint1 = b.SerializeBits<int>(Joint1, 8, name: nameof(Joint1));
				Unknown = b.SerializeBits<int>(Unknown, 8, name: nameof(Unknown));
			});
			Weight = s.SerializeObject<Vec2>(Weight, name: nameof(Weight));

			if (Pre_Version < 308)
				Proto_AdditionalWeights = s.SerializeObject<Vec2>(Proto_AdditionalWeights, name: nameof(Proto_AdditionalWeights));
		}
	}
}