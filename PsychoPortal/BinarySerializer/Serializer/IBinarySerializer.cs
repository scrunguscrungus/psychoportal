﻿using System;
using System.Text;

namespace PsychoPortal
{
	/// <summary>
	/// Defines a binary serializer
	/// </summary>
	public interface IBinarySerializer
	{
		/// <summary>
		/// Serializes a value
		/// </summary>
		/// <typeparam name="T">The value type</typeparam>
		/// <param name="value">The value</param>
		/// <param name="name">The value name, for logging</param>
		/// <returns>The serialized value</returns>
		T Serialize<T>(T value, string name = null);

		/// <summary>
		/// Serializes the size of an array
		/// </summary>
		/// <typeparam name="T">The array value type</typeparam>
		/// <typeparam name="U">The size value type</typeparam>
		/// <param name="array">The array</param>
		/// <param name="name">The array name, for logging</param>
		/// <returns>The serialized array</returns>
		T[] SerializeArraySize<T, U>(T[] array, string name = null);

		/// <summary>
		/// Serializes an array
		/// </summary>
		/// <typeparam name="T">The array value type</typeparam>
		/// <param name="array">The array</param>
		/// <param name="length">The array length, or -1 to use its current length</param>
		/// <param name="name">The array name, for logging</param>
		/// <returns>The serialized array</returns>
		T[] SerializeArray<T>(T[] array, long length = -1, string name = null);

		/// <summary>
		/// Serializes a string of a specified length
		/// </summary>
		/// <param name="value">The string value</param>
		/// <param name="length">The string length</param>
		/// <param name="encoding">The string encoding to use, or null for the default one</param>
		/// <param name="name">The string value name, for logging</param>
		/// <returns>The serialized string value</returns>
		string SerializeString(string value, long length, Encoding encoding = null, string name = null);

		/// <summary>
		/// Serializes a serializable object
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="value">The serializable object</param>
		/// <param name="onPreSerialize">Optional action to run before serializing</param>
		/// <param name="name">The object value name, for logging</param>
		/// <returns>The serialized object</returns>
		T SerializeObject<T>(T value, Action<T> onPreSerialize = null, string name = null)
			where T : IBinarySerializable, new();

		/// <summary>
		/// Serializes an array of serializable objects
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="array">The serializable object array</param>
		/// <param name="length">The array length, or -1 to use its current length</param>
		/// <param name="onPreSerialize">Optional action to run before serializing each object</param>
		/// <param name="name">The object value name, for logging</param>
		/// <returns>The serialized object array</returns>
		T[] SerializeObjectArray<T>(T[] array, long length = -1, Action<T, int> onPreSerialize = null, string name = null)
			where T : IBinarySerializable, new();

		/// <summary>
		/// Serializes an object array of undefined size until a specified condition is met
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="obj">The object array</param>
		/// <param name="conditionCheckFunc">The condition for ending the array serialization</param>
		/// <param name="getLastObjFunc">If specified the last object when read will be ignored and this will be used to prepend an object when writing</param>
		/// <param name="onPreSerialize">Optional action to run before serializing</param>
		/// <param name="name">The name</param>
		/// <returns>The object array</returns>
		T[] SerializeObjectArrayUntil<T>(T[] obj, Func<T, int, bool> conditionCheckFunc, Func<T> getLastObjFunc = null, Action<T, int> onPreSerialize = null, string name = null)
			where T : IBinarySerializable, new();

		/// <summary>
		/// Serializes bit field values
		/// </summary>
		/// <typeparam name="T">The integer value type</typeparam>
		/// <param name="serializeAction">The serializer action</param>
		void DoBits<T>(Action<IBitSerializer> serializeAction);

		/// <summary>
		/// Seeks to the specified stream position
		/// </summary>
		/// <param name="position">The position to go to</param>
		void GoTo(long position);

		/// <summary>
		/// The current stream position
		/// </summary>
		public long Position { get; }
		
		/// <summary>
		/// The current stream length
		/// </summary>
		public long Length { get; }

		/// <summary>
		/// The settings to use when serializing
		/// </summary>
		public PsychonautsSettings Settings { get; }
	}
}