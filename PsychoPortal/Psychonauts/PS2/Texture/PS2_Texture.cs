﻿using System;

namespace PsychoPortal
{
	public class PS2_Texture : IBinarySerializable
	{
		public bool Pre_IsPacked { get; set; }

		public uint Magic { get; set; } // PS2
		public PS2_TextureFormat Format { get; set; }
		public byte UnknownFormatFlags { get; set; } // 3 unknown bits. Highest should always be 0. Other 2 are unknown.
		public bool IsSwizzled { get; set; }
		public ushort Runtime_Flags { get; set; }
		public ushort Ushort_06 { get; set; }
		public uint Width { get; set; }
		public uint Height { get; set; }
		public ushort Ushort_0C { get; set; }
		public ushort Ushort_0E { get; set; }
		public ushort Ushort_10 { get; set; }
		public ushort Ushort_12 { get; set; }
		public byte[] Bytes_14 { get; set; }
		public float Float_24 { get; set; }
		public float Float_28 { get; set; }
		public byte[] Bytes_2C { get; set; }
		public uint PaletteOffset { get; set; }
		public byte[] ImgData { get; set; }
		public byte[] Palette { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			// TODO: There are 3 special format headers: "mupi", "aupi" and "aupj". They however appear unused.
			s.DoBits<uint>(b =>
			{
				Magic = b.SerializeBits<uint>(Magic, 24, name: nameof(Magic));
				Format = b.SerializeBits<PS2_TextureFormat>(Format, 4, name: nameof(Format));
				UnknownFormatFlags = b.SerializeBits<byte>(UnknownFormatFlags, 3, name: nameof(UnknownFormatFlags));
				IsSwizzled = b.SerializeBits<bool>(IsSwizzled, 1, name: nameof(IsSwizzled));
			});

			if (Magic != 0x325350)
				throw new Exception($"PS2 texture magic 0x{Magic:X6} is invalid");

			if (!Enum.IsDefined(typeof(PS2_TextureFormat), Format))
				throw new Exception($"PS2 texture has invalid format 0x{(byte)Format:X2}");

			if (Pre_IsPacked)
			{
				Runtime_Flags = s.Serialize<ushort>(Runtime_Flags, name: nameof(Runtime_Flags));
				Ushort_06 = s.Serialize<ushort>(Ushort_06, name: nameof(Ushort_06));
				Width = s.Serialize<ushort>((ushort)Width, name: nameof(Width));
				Height = s.Serialize<ushort>((ushort)Height, name: nameof(Height));
				Ushort_0C = s.Serialize<ushort>(Ushort_0C, name: nameof(Ushort_0C));
				Ushort_0E = s.Serialize<ushort>(Ushort_0E, name: nameof(Ushort_0E));
				Ushort_10 = s.Serialize<ushort>(Ushort_10, name: nameof(Ushort_10));
				Ushort_12 = s.Serialize<ushort>(Ushort_12, name: nameof(Ushort_12));
				Bytes_14 = s.SerializeArray<byte>(Bytes_14, 16, name: nameof(Bytes_14));
				Float_24 = s.Serialize<float>(Float_24, name: nameof(Float_24));
				Float_28 = s.Serialize<float>(Float_28, name: nameof(Float_28));
				Bytes_2C = s.SerializeArray<byte>(Bytes_2C, 4, name: nameof(Bytes_2C));
				ImgData = s.SerializeArraySize<byte, uint>(ImgData, name: nameof(ImgData));
				Palette = s.SerializeArraySize<byte, uint>(Palette, name: nameof(Palette));
				ImgData = s.SerializeArray<byte>(ImgData, name: nameof(ImgData));
				Palette = s.SerializeArray<byte>(Palette, name: nameof(Palette));
			}
			else
			{
				Width = s.Serialize<uint>(Width, name: nameof(Width));
				Height = s.Serialize<uint>(Height, name: nameof(Height));
				PaletteOffset = s.Serialize<uint>(PaletteOffset, name: nameof(PaletteOffset));
				ImgData = s.SerializeArray<byte>(ImgData, PaletteOffset - 16, name: nameof(ImgData));
				Palette = s.SerializeArray<byte>(Palette, Format.GetPaletteLength(), name: nameof(Palette));
			}
		}
	}
}