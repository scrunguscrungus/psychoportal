﻿using System;

namespace PsychoPortal
{
	[Flags]
	public enum SurfaceFlags : int
	{
		None = 0,
		Hang = 1 << 0,
		Climb = 1 << 1,
		ClimbUDRL = 1 << 2,
		Cantilever = 1 << 3,
		Pole = 1 << 4,
		Tightrope = 1 << 5,
		Slippery = 1 << 6,
		Grippy = 1 << 7,
		Pipe = 1 << 8,
		NoTeeter = 1 << 9,
		Physical = 1 << 10,
		Visibility = 1 << 11,
		Camera = 1 << 12,
		RailSlide = 1 << 13,
		Water = 1 << 14,
		DoubleSided = 1 << 15,
		Gravity = 1 << 16,
		Nav = 1 << 17,
		NonManifold = 1 << 18, // Game colors this red in debug mode
		Terrain = 1 << 19,

		Flag_20 = 1 << 20,
		Flag_21 = 1 << 21,
		Flag_22 = 1 << 22,
		Flag_23 = 1 << 23,
		Flag_24 = 1 << 24,
		Flag_25 = 1 << 25,
		Flag_26 = 1 << 26,
		Flag_27 = 1 << 27,
		Flag_28 = 1 << 28,
		Flag_29 = 1 << 29,
		Flag_30 = 1 << 30,
		Flag_31 = 1 << 31,
	}
}