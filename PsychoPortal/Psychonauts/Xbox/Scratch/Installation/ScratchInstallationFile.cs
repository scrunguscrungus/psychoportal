﻿using System.IO;

namespace PsychoPortal
{
	public class ScratchInstallationFile : IBinarySerializable
	{
		public String2b Name { get; set; }
		public uint FileSize { get; set; }
		public long FileDataOffset { get; set; }

		public Stream GetFileStream(Stream packageStream)
		{
			byte[] buffer = new byte[FileSize];
			packageStream.Position = FileDataOffset;
			int read = packageStream.Read(buffer, 0, (int)FileSize);

			if (read != FileSize)
				throw new EndOfStreamException();

			return new MemoryStream(buffer);
		}

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String2b>(Name, name: nameof(Name));
			FileSize = s.Serialize<uint>(FileSize, name: nameof(FileSize));
			
			// Don't read the file into memory to avoid large memory usage
			FileDataOffset = s.Position;
			s.GoTo(s.Position + FileSize);
		}
	}
}