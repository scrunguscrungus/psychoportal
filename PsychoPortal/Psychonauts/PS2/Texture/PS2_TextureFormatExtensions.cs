﻿using System;

namespace PsychoPortal
{
	public static class PS2_TextureFormatExtensions
	{
		public static int GetBPP(this PS2_TextureFormat format) => format switch
		{
			PS2_TextureFormat.PSMCT32 => 32,
			PS2_TextureFormat.PSMCT24 => 24,
			PS2_TextureFormat.PSMCT16S => 16,
			PS2_TextureFormat.PSMT8_32A => 8,
			PS2_TextureFormat.PSMT8_32 => 8,
			PS2_TextureFormat.PSMT8_16 => 8,
			PS2_TextureFormat.PSMT4_32A => 4,
			PS2_TextureFormat.PSMT4_32 => 4,
			PS2_TextureFormat.PSMT4_16 => 4,
			PS2_TextureFormat.PSMZ24 => 32,
			PS2_TextureFormat.PSMT8H => 8,
			_ => throw new ArgumentOutOfRangeException(nameof(format), format, null)
		};

		public static int GetPaletteLength(this PS2_TextureFormat format) => format switch
		{
			PS2_TextureFormat.PSMCT32 => 0,
			PS2_TextureFormat.PSMCT24 => 0,
			PS2_TextureFormat.PSMCT16S => 0,
			PS2_TextureFormat.PSMT8_32A => 4 * 256, // 1024
			PS2_TextureFormat.PSMT8_32 => 4 * 256, // 1024
			PS2_TextureFormat.PSMT8_16 => 2 * 256, // 512
			PS2_TextureFormat.PSMT4_32A => 4 * 16,  // 64
			PS2_TextureFormat.PSMT4_32 => 4 * 16,  // 64
			PS2_TextureFormat.PSMT4_16 => 2 * 16,  // 32
			PS2_TextureFormat.PSMZ24 => 0,
			PS2_TextureFormat.PSMT8H => 4 * 256, // 512
			_ => throw new ArgumentOutOfRangeException(nameof(format), format, null)
		};

		public static PS2_PixelStorageMode GetTexturePixelStorageMode(this PS2_TextureFormat format) => format switch
		{
			PS2_TextureFormat.PSMCT32 => PS2_PixelStorageMode.PSMCT32,
			PS2_TextureFormat.PSMCT24 => PS2_PixelStorageMode.PSMCT24,
			PS2_TextureFormat.PSMCT16S => PS2_PixelStorageMode.PSMCT16S,
			PS2_TextureFormat.PSMT8_32A => PS2_PixelStorageMode.PSMT8,
			PS2_TextureFormat.PSMT8_32 => PS2_PixelStorageMode.PSMT8,
			PS2_TextureFormat.PSMT8_16 => PS2_PixelStorageMode.PSMT8,
			PS2_TextureFormat.PSMT4_32A => PS2_PixelStorageMode.PSMT4,
			PS2_TextureFormat.PSMT4_32 => PS2_PixelStorageMode.PSMT4,
			PS2_TextureFormat.PSMT4_16 => PS2_PixelStorageMode.PSMT4,
			PS2_TextureFormat.PSMZ24 => PS2_PixelStorageMode.PSMZ24,
			PS2_TextureFormat.PSMT8H => PS2_PixelStorageMode.PSMT8H,
			_ => throw new ArgumentOutOfRangeException(nameof(format), format, null)
		};
	}
}