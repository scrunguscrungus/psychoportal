﻿namespace PsychoPortal
{
	/// <summary>
	/// A 32-bit RGBA color
	/// </summary>
	public class BGRA8888Color : IBinarySerializable, IBinaryShortLog
	{
		public byte Blue { get; set; }
		public byte Green { get; set; }
		public byte Red { get; set; }
		public byte Alpha { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => $"RGBA({Red}, {Green}, {Blue}, {Alpha})";

		public void Serialize(IBinarySerializer s)
		{
			Blue = s.Serialize<byte>(Blue, name: nameof(Blue));
			Green = s.Serialize<byte>(Green, name: nameof(Green));
			Red = s.Serialize<byte>(Red, name: nameof(Red));
			Alpha = s.Serialize<byte>(Alpha, name: nameof(Alpha));
		}
	}
}