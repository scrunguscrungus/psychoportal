﻿using System;

namespace PsychoPortal
{
	public class Scene : IBinarySerializable
	{
		public uint Version { get; set; }
		public SceneFlags Flags { get; set; }
		public TextureReference[] TextureTranslationTable { get; set; }
		public Domain RootDomain { get; set; }
		public NavMesh[] NavMeshes { get; set; }
		public VisibilityTree VisibilityTree { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("PSYC", 4, isReversed: true);
			Version = s.Serialize<uint>(Version, name: nameof(Version));

			// Modern versions support 317-319 and the 2004 prototype supports 300-316.
			if (Version is < 300 or > 319)
				throw new Exception($"Unsupported plb version {Version}");

			Flags = s.Serialize<SceneFlags>(Flags, name: nameof(Flags));

			TextureTranslationTable = s.SerializeArraySize<TextureReference, int>(TextureTranslationTable, name: nameof(TextureTranslationTable));
			TextureTranslationTable = s.SerializeObjectArray<TextureReference>(TextureTranslationTable, onPreSerialize: (x, _) => x.Pre_Version = Version, name: nameof(TextureTranslationTable));
			
			RootDomain = s.SerializeObject<Domain>(RootDomain, x => x.Pre_Version = Version, name: nameof(RootDomain));

			NavMeshes = s.SerializeArraySize<NavMesh, int>(NavMeshes, name: nameof(NavMeshes));
			
			if (Version >= 291)
				NavMeshes = s.SerializeObjectArray<NavMesh>(NavMeshes, onPreSerialize: (x, _) => x.Pre_Version = Version, name: nameof(NavMeshes));

			if (Flags.HasFlag(SceneFlags.HasVisibilityTree))
				VisibilityTree = s.SerializeObject<VisibilityTree>(VisibilityTree, x => x.Pre_Version = Version, name: nameof(VisibilityTree));
		}

		[Flags]
		public enum SceneFlags : uint
		{
			None = 0,
			HasVisibilityTree = 1 << 0,
		}
	}
}