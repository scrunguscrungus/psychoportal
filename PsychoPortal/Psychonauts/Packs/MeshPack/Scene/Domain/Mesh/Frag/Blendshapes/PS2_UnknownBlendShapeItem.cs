﻿namespace PsychoPortal
{
	public class PS2_UnknownBlendShapeItem : IBinarySerializable
	{
		public byte[] Data { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Data = s.SerializeArray<byte>(Data, 6, name: nameof(Data));
		}
	}
}