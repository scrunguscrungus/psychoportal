﻿namespace PsychoPortal
{
	public class BoneMaxWeight : IBinarySerializable
	{
		// In later versions of the game only 2 influences per vertex are allowed, but earlier versions supported up to 4.
		// The sum of every weight has to be 1. In later versions the game only reads the first one and assumes the second
		// one is 1 - Weight1.
		public float Weight1 { get; set; }
		public float Weight2 { get; set; }
		public float Weight3 { get; set; }
		public float Weight4 { get; set; }

		public int Joint1 { get; set; }
		public int Joint2 { get; set; }
		public int Joint3 { get; set; }
		public int Joint4 { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Weight1 = s.Serialize<float>(Weight1, name: nameof(Weight1));
			Weight2 = s.Serialize<float>(Weight2, name: nameof(Weight2));
			Weight3 = s.Serialize<float>(Weight3, name: nameof(Weight3));
			Weight4 = s.Serialize<float>(Weight4, name: nameof(Weight4));

			if (s.Settings.Version == PsychonautsVersion.PS2)
			{
				Joint1 = s.Serialize<byte>((byte)Joint1, name: nameof(Joint1));
				Joint2 = s.Serialize<byte>((byte)Joint2, name: nameof(Joint2));
				Joint3 = s.Serialize<byte>((byte)Joint3, name: nameof(Joint3));
				Joint4 = s.Serialize<byte>((byte)Joint4, name: nameof(Joint4));
			}
			else
			{
				Joint1 = s.Serialize<int>(Joint1, name: nameof(Joint1));
				Joint2 = s.Serialize<int>(Joint2, name: nameof(Joint2));
				Joint3 = s.Serialize<int>(Joint3, name: nameof(Joint3));
				Joint4 = s.Serialize<int>(Joint4, name: nameof(Joint4));
			}
		}
	}
}