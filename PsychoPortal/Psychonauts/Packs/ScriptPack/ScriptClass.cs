﻿namespace PsychoPortal
{
	public class ScriptClass : IBinarySerializable
	{
		public String2b ClassName { get; set; }
		public byte[] ScriptCode { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			ClassName = s.SerializeObject<String2b>(ClassName, name: nameof(ClassName));
			ScriptCode = s.SerializeArraySize<byte, uint>(ScriptCode, name: nameof(ScriptCode));
			ScriptCode = s.SerializeArray<byte>(ScriptCode, name: nameof(ScriptCode));
		}
	}
}