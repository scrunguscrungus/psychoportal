﻿namespace PsychoPortal
{
	// The game uses different affectors as components to the entities. This one acts like an animation for the entire mesh.
	public class AnimAffector : IBinarySerializable
	{
		public int AffectorType { get; set; }
		public AnimAffectorType Type { get; set; }
		public int TotalFramesCount { get; set; }
		public AnimAffector_KeyFrame[] KeyFrames { get; set; } // Key frames the game interpolates between

		public void Serialize(IBinarySerializer s)
		{
			AffectorType = s.Serialize<int>(AffectorType, name: nameof(AffectorType));

			if (AffectorType != 0)
				return;

			Type = s.Serialize<AnimAffectorType>(Type, name: nameof(Type));
			TotalFramesCount = s.Serialize<int>(TotalFramesCount, name: nameof(TotalFramesCount));
			KeyFrames = s.SerializeArraySize<AnimAffector_KeyFrame, int>(KeyFrames, name: nameof(KeyFrames));
			KeyFrames = s.SerializeObjectArray<AnimAffector_KeyFrame>(KeyFrames, name: nameof(KeyFrames));
		}

		public enum AnimAffectorType : int
		{
			Position = 0,
			Rotation = 1,
			Scale = 2,
		}
	}
}