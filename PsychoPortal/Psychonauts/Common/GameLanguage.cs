﻿namespace PsychoPortal
{
	public enum GameLanguage : ushort
	{
		English = 0,
		French = 1,
		German = 2,
		Nonsense = 3,
	}
}