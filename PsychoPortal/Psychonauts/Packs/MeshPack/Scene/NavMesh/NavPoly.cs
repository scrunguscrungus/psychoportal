﻿namespace PsychoPortal
{
	public class NavPoly : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public uint Flags { get; set; }
		public UInt24 CollisionTriIndex { get; set; }
		public byte Byte_07 { get; set; }
		public int[] NavPolyIndices { get; set; }
		public int[] VectorIndices { get; set; }

		public bool IsSolid => CollisionTriIndex != 0xFFFFFF;

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_Version >= 304)
				Flags = s.Serialize<uint>(Flags, name: nameof(Flags));

			CollisionTriIndex = s.Serialize<UInt24>(CollisionTriIndex, name: nameof(CollisionTriIndex));
			Byte_07 = s.Serialize<byte>(Byte_07, name: nameof(Byte_07));
			NavPolyIndices = s.SerializeArray<int>(NavPolyIndices, 3, name: nameof(NavPolyIndices));
			VectorIndices = s.SerializeArray<int>(VectorIndices, 3, name: nameof(VectorIndices));
		}
	}
}