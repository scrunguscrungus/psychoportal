﻿namespace PsychoPortal
{
	/// <summary>
	/// A compressed <see cref="Vec3"/>
	/// </summary>
	public class NormPacked3 : IBinarySerializable, IBinaryShortLog
	{
		public int Value { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => ToVec3().ShortLog;

		public void Serialize(IBinarySerializer s)
		{
			Value = s.Serialize<int>(Value, name: nameof(Value));
		}

		public Vec3 ToVec3() => new(
			((short)((short)Value << 5) >> 5) * 0.0009775171f, 
			(short) ((Value << 10) >> 0x15) * 0.0009775171f, 
			((short)(Value >> 16) >> 6) * 0.001956947f);

		public static NormPacked3 FromVec3(Vec3 vec)
		{
			float pin(float f1, float f2, float f3)
			{
				if (f1 > f3) 
					return f3;
				
				if (f2 > f1)
					return f2;

				return f1;
			}

			var packed = new NormPacked3();

			// X
			float v = pin(vec.X, -1.0f, 1.0f);
			v *= 1023.0f;
			
			if (v <= 0.0)
				v -= 0.5f;
			else
				v += 0.5f;

			packed.Value = BitHelpers.SetBits(packed.Value, (short)((int)v << 5) >> 5, 11, 0);

			// Y
			v = pin(vec.Y, -1.0f, 1.0f);
			v *= 1023.0f;
			
			if (v <= 0.0)
				v -= 0.5f;
			else
				v += 0.5f;

			packed.Value = BitHelpers.SetBits(packed.Value, (short)((int)v << 5) >> 5, 11, 11);

			// Z
			v = pin(vec.Z, -1.0f, 1.0f);
			v *= 511.0f;
			
			if (v <= 0.0)
				v -= 0.5f;
			else
				v += 0.5f;

			packed.Value = BitHelpers.SetBits(packed.Value, (short)((int)v << 6) >> 6, 10, 22);

			return packed;
		}
	}
}