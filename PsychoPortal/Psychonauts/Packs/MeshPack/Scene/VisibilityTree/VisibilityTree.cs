﻿namespace PsychoPortal
{
	public class VisibilityTree : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public Octree Octree { get; set; }
		public VisibilityTreeLeafFlags[] LeafFlags { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Octree = s.SerializeObject<Octree>(Octree, x =>
			{
				x.Pre_Version = Pre_Version;
				x.Pre_OldHasPrimitives = false;
			}, name: nameof(Octree));

			int bufferLength = Octree.LeavesCount - 1;

			if (bufferLength < 0)
				bufferLength = Octree.LeavesCount + 0x1E;

			bufferLength = (bufferLength >> 5) + 1;
			
			LeafFlags = s.SerializeObjectArray<VisibilityTreeLeafFlags>(LeafFlags, Octree.LeavesCount, 
				(x, _) => x.Pre_Count = bufferLength, name: nameof(LeafFlags));
		}
	}
}