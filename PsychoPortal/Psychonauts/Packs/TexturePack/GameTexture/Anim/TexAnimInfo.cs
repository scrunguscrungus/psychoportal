﻿namespace PsychoPortal
{
	public class TexAnimInfo : IBinarySerializable
	{
		public int FramesCount { get; set; }
		public float CurrentFrame { get; set; }
		public float LoopFrame { get; set; } // Only used for LoopTail
		public float Delay { get; set; } // In milliseconds - is this correct?
		public float FramesPerSecond { get; set; }
		public AnimPlayMode PlayMode { get; set; }
		public bool Runtime_IsAnimatingForward { get; set; }
		public byte[] Padding { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			FramesCount = s.Serialize<int>(FramesCount, name: nameof(FramesCount));
			CurrentFrame = s.Serialize<float>(CurrentFrame, name: nameof(CurrentFrame));
			LoopFrame = s.Serialize<float>(LoopFrame, name: nameof(LoopFrame));
			Delay = s.Serialize<float>(Delay, name: nameof(Delay));
			FramesPerSecond = s.Serialize<float>(FramesPerSecond, name: nameof(FramesPerSecond));
			PlayMode = s.Serialize<AnimPlayMode>(PlayMode, name: nameof(PlayMode));

			if (s.Settings.Version != PsychonautsVersion.PS2)
				s.SerializePadding(3);

			Runtime_IsAnimatingForward = s.Serialize<bool>(Runtime_IsAnimatingForward, name: nameof(Runtime_IsAnimatingForward));
			Padding = s.SerializeArray<byte>(Padding, s.Settings.Version == PsychonautsVersion.PS2 ? 2 : 3, name: nameof(Padding));
		}
	}
}