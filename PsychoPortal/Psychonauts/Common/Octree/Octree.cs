﻿using System;

namespace PsychoPortal
{
	public class Octree : IBinarySerializable
	{
		public uint Pre_Version { get; set; }
		public bool Pre_OldHasPrimitives { get; set; }

		public uint Uint_00 { get; set; } // Unused
		public SSECube SSECube { get; set; }
		public int LeavesCount { get; set; }
		public int NodesCount { get; set; }
		public OctreeNode[] Nodes { get; set; }
		public OctreeLeaf[] Leaves { get; set; }
		public uint[] Primitives { get; set; }
		public UInt24[] PS2_Primitives { get; set; }

		// TODO: Implement converting old format to new format like game does
		// Old format
		public OldOctreeNode[] OldNodes { get; set; }
		public OldOctreeLeaf[] OldLeaves { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_Version < 314)
			{
				OldNodes = s.SerializeArraySize<OldOctreeNode, uint>(OldNodes, name: nameof(OldNodes));
				OldLeaves = s.SerializeArraySize<OldOctreeLeaf, uint>(OldLeaves, name: nameof(OldLeaves));

				if (Pre_OldHasPrimitives)
					Primitives = s.SerializeArraySize<uint, int>(Primitives, name: nameof(Primitives));

				OldNodes = s.SerializeObjectArray<OldOctreeNode>(OldNodes, name: nameof(OldNodes));
				OldLeaves = s.SerializeObjectArray<OldOctreeLeaf>(OldLeaves, name: nameof(OldLeaves));

				if (Pre_OldHasPrimitives)
					Primitives = s.SerializeArray<uint>(Primitives, name: nameof(Primitives));
			}
			else
			{
				Uint_00 = s.Serialize<uint>(Uint_00, name: nameof(Uint_00));
				SSECube = s.SerializeObject<SSECube>(SSECube, name: nameof(SSECube));
				LeavesCount = s.Serialize<int>(LeavesCount, name: nameof(LeavesCount));
				NodesCount = s.Serialize<int>(NodesCount, name: nameof(NodesCount));
				Nodes = s.SerializeObjectArray<OctreeNode>(Nodes, NodesCount, name: nameof(Nodes));
				Leaves = s.SerializeArraySize<OctreeLeaf, int>(Leaves, name: nameof(Leaves)); // StoredLeaves

				if (Leaves.Length != 0 && Leaves.Length != LeavesCount)
					throw new Exception("Invalid leaves count");

				Leaves = s.SerializeArray<OctreeLeaf>(Leaves, name: nameof(Leaves));

				if (s.Settings.Version == PsychonautsVersion.PS2)
				{
					PS2_Primitives = s.SerializeArraySize<UInt24, int>(PS2_Primitives, name: nameof(PS2_Primitives));
					PS2_Primitives = s.SerializeArray<UInt24>(PS2_Primitives, name: nameof(PS2_Primitives));
				}
				else
				{
					Primitives = s.SerializeArraySize<uint, int>(Primitives, name: nameof(Primitives));
					Primitives = s.SerializeArray<uint>(Primitives, name: nameof(Primitives));
				}
			}
		}
	}
}