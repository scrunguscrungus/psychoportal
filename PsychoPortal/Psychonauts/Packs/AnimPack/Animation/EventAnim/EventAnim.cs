﻿using System;

namespace PsychoPortal
{
	/// <summary>
	/// A shared event animation (.eve file). This is usually a part of a <see cref="SharedSkelAnim"/> and broadcasts events during certain keyframes.
	/// Every event animation has 16 channels.
	/// </summary>
	public class EventAnim : IBinarySerializable
	{
		/// <summary>
		/// The char used to split the string buffer. In version 101 this was updated to tab.
		/// </summary>
		private char SplitChar => Version == 100 ? ' ' : '\t';

		/// <summary>
		/// The file version. This is either 100 or 101 with the only difference being the char used to split the strings in the event tables.
		/// </summary>
		public uint Version { get; set; }

		/// <summary>
		/// The amount of event objects. This has to be 1.
		/// </summary>
		public uint EventObjectsCount { get; set; }

		/// <summary>
		/// The event tables, one per channel. Each string is split into multiple items, one for each keyframe.
		/// </summary>
		public String2b[] EventAnimTables { get; set; }

		/// <summary>
		/// The event channels. There are always 16 of these. Channels 0-7 are common channels, with only 0 and 1 being implemented, both of
		/// which play a sound. Channels 8-15 are handled by the entity script.
		/// </summary>
		public EventAnim_Channel[] Channels { get; set; }

		/// <summary>
		/// Gets the string items, one for each keyframe, for a specific channel
		/// </summary>
		/// <param name="channel">The channel to get the items for</param>
		/// <returns>The string items, one for each keyframe</returns>
		public string[] GetItems(int channel) => EventAnimTables[channel].Value.Split(SplitChar);

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("PEVE", 4, isReversed: true);
			Version = s.Serialize<uint>(Version, name: nameof(Version));
			EventObjectsCount = s.Serialize<uint>(EventObjectsCount, name: nameof(EventObjectsCount));

			if (EventObjectsCount != 1)
				throw new Exception($"Invalid event objects count {EventObjectsCount}. Should be 1.");

			EventAnimTables = s.SerializeArraySize<String2b, uint>(EventAnimTables, name: nameof(EventAnimTables));
			EventAnimTables = s.SerializeObjectArray<String2b>(EventAnimTables, name: nameof(EventAnimTables));

			Channels = s.SerializeArraySize<EventAnim_Channel, uint>(Channels, name: nameof(Channels));
			Channels = s.SerializeObjectArray<EventAnim_Channel>(Channels, name: nameof(Channels));
		}
	}
}