﻿namespace PsychoPortal
{
	public enum PsychonautsVersion
	{
		Xbox_Proto_20041217,
		Xbox_Proto_20050214,
		Xbox,
		PS2,
		PC_Original,
		PC_Digital,
	}
}