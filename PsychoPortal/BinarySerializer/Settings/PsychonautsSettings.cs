﻿namespace PsychoPortal
{
	public class PsychonautsSettings
	{
		public PsychonautsSettings(PsychonautsVersion version)
		{
			Version = version;
		}

		public PsychonautsVersion Version { get; }
	}
}