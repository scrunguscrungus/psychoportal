﻿namespace PsychoPortal
{
	/// <summary>
	/// An event animation channel containing its keyframes
	/// </summary>
	public class EventAnim_Channel : IBinarySerializable
	{
		/// <summary>
		/// The key frames for this channel. Not all channels have keyframes.
		/// </summary>
		public EventAnim_KeyFrame[] KeyFrames { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			KeyFrames = s.SerializeArraySize<EventAnim_KeyFrame, uint>(KeyFrames, name: nameof(KeyFrames));
			KeyFrames = s.SerializeObjectArray<EventAnim_KeyFrame>(KeyFrames, name: nameof(KeyFrames));
		}
	}
}