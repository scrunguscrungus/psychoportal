﻿namespace PsychoPortal
{
	public class VertexStreamSWBlend : IBinarySerializable
	{
		public uint Pre_Version { get; set; }
		public ushort Pre_Index { get; set; }
		public byte Pre_Flags { get; set; }

		public ushort Index { get; set; }
		public NormPacked3 Vertex { get; set; }
		public NormPacked3 Normal { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_Version < 319 || (Pre_Flags & 2) == 0)
				Index = Pre_Index;
			else
				Index = s.Serialize<ushort>(Index, name: nameof(Index));

			Vertex = s.SerializeObject<NormPacked3>(Vertex, name: nameof(Vertex));
			Normal = s.SerializeObject<NormPacked3>(Normal, name: nameof(Normal));
		}
	}
}