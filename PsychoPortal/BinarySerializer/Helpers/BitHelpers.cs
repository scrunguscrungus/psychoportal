﻿using System.Runtime.CompilerServices;

namespace PsychoPortal
{
	public static class BitHelpers
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int ExtractBits(int value, int count, int offset)
		{
			return ((1 << count) - 1) & (value >> offset);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long ExtractBits64(long value, int count, int offset)
		{
			return ((((long)1 << count) - 1) & (value >> (offset)));
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int SetBits(int bits, int value, int count, int offset)
		{
			int mask = ((1 << count) - 1) << offset;
			bits = (bits & ~mask) | (value << offset);
			return bits;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long SetBits64(long bits, long value, int count, int offset)
		{
			long mask = (((long)1 << count) - 1) << offset;
			bits = (bits & ~mask) | (value << offset);
			return bits;
		}
	}
}