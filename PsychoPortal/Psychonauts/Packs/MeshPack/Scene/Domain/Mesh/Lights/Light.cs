﻿using System;

namespace PsychoPortal
{
	public class Light : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public LightType Type { get; set; }
		public LightFlags Flags { get; set; }
		public String4b Name { get; set; }
		public String4b HaloTextureName { get; set; }
		public float HaloFloat1 { get; set; } // Default is 400
		public float HaloFloat2 { get; set; } // Default is 0.15
		public Vec3 Color { get; set; }
		public Vec3 Vector2 { get; set; } // Can't be negative
		public Vec3 Position { get; set; }
		public Vec3 Direction { get; set; }

		// Spotlight
		public float SpotlightFloat1 { get; set; }
		public float SpotlightFloat2 { get; set; }

		// Projected texture
		public String4b ProjectedTexture { get; set; }
		public Vec3 ProjectedTextureUpVector { get; set; }
		public ProjectedLight ProjectedLight { get; set; }

		// TODO: This is shadow/depth mapping: https://learnopengl.com/Advanced-Lighting/Shadows/Shadow-Mapping stored as 32x32
		// Extra data
		public ushort[] Extra_Ushorts1 { get; set; }
		public ushort[] Extra_PlanesCounts { get; set; }
		public Mat4 Extra_Directional_Matrix { get; set; }
		public Box3 Extra_Directional_Box { get; set; }
		public LightExtraDataEntry[] Extra_Entries { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Type = s.Serialize<LightType>(Type, name: nameof(Type));
			Flags = s.Serialize<LightFlags>(Flags, name: nameof(Flags));
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));

			if (Flags.HasFlag(LightFlags.Flag_2))
			{
				HaloTextureName = s.SerializeObject<String4b>(HaloTextureName, name: nameof(HaloTextureName));
				HaloFloat1 = s.Serialize<float>(HaloFloat1, name: nameof(HaloFloat1)); // Game keeps half of this

				if (Pre_Version >= 310)
					HaloFloat2 = s.Serialize<float>(HaloFloat2, name: nameof(HaloFloat2));
				else
					HaloFloat2 = 0.150000005960464f;
			}

			Color = s.SerializeObject<Vec3>(Color, name: nameof(Color));
			Vector2 = s.SerializeObject<Vec3>(Vector2, name: nameof(Vector2));
			Position = s.SerializeObject<Vec3>(Position, name: nameof(Position));

			if (Type == LightType.Directional)
			{
				Direction = s.SerializeObject<Vec3>(Direction, name: nameof(Direction));
			}
			else if (Type == LightType.Spot)
			{
				Direction = s.SerializeObject<Vec3>(Direction, name: nameof(Direction));
				SpotlightFloat1 = s.Serialize<float>(SpotlightFloat1, name: nameof(SpotlightFloat1));
				SpotlightFloat2 = s.Serialize<float>(SpotlightFloat2, name: nameof(SpotlightFloat2));
			}
			else if (Type == LightType.Projected)
			{
				ProjectedTexture = s.SerializeObject<String4b>(ProjectedTexture, name: nameof(ProjectedTexture));
				Direction = s.SerializeObject<Vec3>(Direction, name: nameof(Direction));

				if (Pre_Version >= 307)
					ProjectedTextureUpVector = s.SerializeObject<Vec3>(ProjectedTextureUpVector, name: nameof(ProjectedTextureUpVector));

				ProjectedLight = s.SerializeObject<ProjectedLight>(ProjectedLight, x => x.Pre_Version = Pre_Version, name: nameof(ProjectedLight));
			}

			if (Flags.HasFlag(LightFlags.Flag_9) && Pre_Version >= 312)
			{
				if (Pre_Version == 312)
				{
					// The game reads 6 uints which get added to a total value. This is the size of a buffer of 6-byte values. Then it returns.
					throw new NotImplementedException("Not implemented PLB version 312 additional light data");
				}

				int count = Type == LightType.Directional ? 1 : 6;

				Extra_Ushorts1 = s.SerializeArray<ushort>(Extra_Ushorts1, count, name: nameof(Extra_Ushorts1));
				Extra_PlanesCounts = s.SerializeArray<ushort>(Extra_PlanesCounts, count, name: nameof(Extra_PlanesCounts));

				if (Type == LightType.Directional)
				{
					Extra_Directional_Matrix = s.SerializeObject<Mat4>(Extra_Directional_Matrix, name: nameof(Extra_Directional_Matrix));
					Extra_Directional_Box = s.SerializeObject<Box3>(Extra_Directional_Box, name: nameof(Extra_Directional_Box));
				}

				Extra_Entries = s.SerializeObjectArray<LightExtraDataEntry>(Extra_Entries, count, (x, i) =>
				{
					x.Pre_Type = Type;
					x.Pre_Ushort1 = Extra_Ushorts1[i];
					x.PlanesCount = Extra_PlanesCounts[i];
				}, name: nameof(Extra_Entries));
			}
		}

		public enum LightType : uint
		{
			Unknown = 0,
			Ambient = 1,
			Point = 2,
			Directional = 3,
			Spot = 4,
			Projected = 5,
		}

		[Flags]
		public enum LightFlags : int
		{
			None = 0,
			Dynamic = 1 << 0,
			AddHalo = 1 << 1,
			Flag_2 = 1 << 2,
			Flag_3 = 1 << 3,
			Switchable = 1 << 4,
			Flag_5 = 1 << 5,
			Flag_6 = 1 << 6,
			Flag_7 = 1 << 7,
			LightRig = 1 << 8,
			Flag_9 = 1 << 9,
			Flag_10 = 1 << 10,
			Flag_11 = 1 << 11,
			Flag_12 = 1 << 12,
			Flag_13 = 1 << 13,
			Flag_14 = 1 << 14,
			Flag_15 = 1 << 15,
			IsEnabled = 1 << 16,
			Flag_17 = 1 << 17,
			Flag_18 = 1 << 18,
			Flag_19 = 1 << 19,
			Flag_20 = 1 << 20,
			Flag_21 = 1 << 21,
			Flag_22 = 1 << 22,
			Flag_23 = 1 << 23,
			Flag_24 = 1 << 24,
			Flag_25 = 1 << 25,
			Flag_26 = 1 << 26,
			Flag_27 = 1 << 27,
			Flag_28 = 1 << 28,
			Flag_29 = 1 << 29,
			Flag_30 = 1 << 30,
			Flag_31 = 1 << 31,
		}
	}
}