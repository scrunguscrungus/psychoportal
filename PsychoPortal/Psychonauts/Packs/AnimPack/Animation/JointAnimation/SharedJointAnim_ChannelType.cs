﻿namespace PsychoPortal
{
	public enum SharedJointAnim_ChannelType : ushort
	{
		Position, // CHANNEL_POS
		Rotation,
		Scale,
		CompressedRotation, // Does not exist on PS2
	}
}