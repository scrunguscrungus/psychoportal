﻿namespace PsychoPortal
{
	public class UV : IBinarySerializable, IBinaryShortLog
	{
		public uint Pre_Version { get; set; }

		public bool UsesFloats => Pre_Version < 317;

		public short U { get; set; }
		public short V { get; set; }

		public float U_Float { get; set; }
		public float V_Float { get; set; }

		public string ShortLog => ToString();
		public override string ToString()
		{
			Vec2 v = ToVec2();
			return $"UV({v.X}, {v.Y})";
		}

		public Vec2 ToVec2(float scale = 1) => UsesFloats 
			? new Vec2(U_Float * scale, V_Float * scale) 
			: new Vec2(U / (float)0x7FFF * scale, V / (float)0x7FFF * scale);

		public void Serialize(IBinarySerializer s)
		{
			if (UsesFloats)
			{
				U_Float = s.Serialize<float>(U_Float, name: nameof(U_Float));
				V_Float = s.Serialize<float>(V_Float, name: nameof(V_Float));
			}
			else
			{
				U = s.Serialize<short>(U, name: nameof(U));
				V = s.Serialize<short>(V, name: nameof(V));
			}
		}
	}
}