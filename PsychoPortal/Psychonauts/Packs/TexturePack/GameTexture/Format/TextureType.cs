﻿namespace PsychoPortal
{
	// A uint on all versions besides PS2
	public enum TextureType : byte
	{
		Bitmap = 0,
		Cubemap = 1,
		VolumeMap = 2,
		DepthBuffer = 3,
	}
}