﻿namespace PsychoPortal
{
	/// <summary>
	/// A box with a minimum and maximum position
	/// </summary>
	public class Box3 : IBinarySerializable
	{
		public Vec3 Min { get; set; }
		public Vec3 Max { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Min = s.SerializeObject<Vec3>(Min, name: nameof(Min));
			Max = s.SerializeObject<Vec3>(Max, name: nameof(Max));
		}

		public override string ToString() => $"{Min} - {Max}";
	}
}