﻿namespace PsychoPortal
{
	public interface IBitSerializer
	{
		public IBinarySerializer Serializer { get; }
		public long Value { get; set; }
		public int Position { get; set; }

		public abstract T SerializeBits<T>(T value, int length, string name = null);
	}
}