﻿using System.Runtime.InteropServices;
using System.Diagnostics;

namespace PsychoPortal
{
	[DebuggerDisplay("Index = {PrimitiveIndex}, Count = {PrimitiveCount}")]
	[StructLayout(LayoutKind.Sequential)]
	public readonly struct OctreeLeaf
	{
		public OctreeLeaf(uint value)
		{
			_b3 = (byte)(value & 0xFF);
			_b2 = (byte)((value >> 8) & 0xFF);
			_b1 = (byte)((value >> 16) & 0xFF);
			_b0 = (byte)((value >> 24) & 0xFF);
		}

		public static OctreeLeaf Pack(uint primIndex, uint primCount)
		{
			uint packedLeaf = (primIndex << 24) | (primIndex & 0xFF00) << 8 | (primIndex & 0x70000) >> 8 | (primCount & 0x1F) << 11 | (primCount >> 5);
			return new OctreeLeaf(packedLeaf);
		}

		private readonly byte _b0;
		private readonly byte _b1;
		private readonly byte _b2;
		private readonly byte _b3;

		uint PrimitiveIndex
		{
			get
			{
				uint c1 = (uint)(_b1 << 8);	 // Turn _b1 into the upper byte of a short
				uint c2 = (uint)(_b0 | c1); // Create a short using _b1 as the upper byte and _b0 as the lower

				uint c3 = (uint)(_b2 & 7); //Get the last 3 bits of _b2
				uint c4 = (uint)(c3 << 16);
				return c4 | c2;
			}
		}

		uint PrimitiveCount
		{
			get
			{
				return (uint)((_b3 << 5) | (_b2 >> 3));
			}
		}
	}
}