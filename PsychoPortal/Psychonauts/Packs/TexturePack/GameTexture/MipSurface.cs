﻿namespace PsychoPortal
{
	public class MipSurface : IBinarySerializable
	{
		public long Pre_ImageDataLength { get; set; }

		public byte[] ImageData { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			ImageData = s.SerializeArray<byte>(ImageData, Pre_ImageDataLength, name: nameof(ImageData));
		}
	}
}