# PsychoPortal - format library for Psychonauts

### What PsychoPortal is

PsychoPortal is a .NET library made to provide a means to read and write the various file formats of the very excellent game Psychonauts so that tools can be created to work with and manipulate them. It can (or will be able to) read and write things such as pack files used by the game.


### What PsychoPortal is not
Note, PsychoPortal is not a Lua decompiler, nor a DDS parser. It will provide the data for these things (as well as essential metadata for textures) but tools using PsychoPortal must parse this data on their own. Once parsed, the data may be modified in any way you wish and fed back into PsychoPortal's structures to write out files that Psychonauts can read.

## PsychoPortalTestProgram
Also provided is the incredibly well named *PsychoPortalTestProgram*. This application serves as a basic tool for working with the formats as well as something of an example of how PsychoPortal itself is used.

Note that it's very simple, and not super ideal for working with the files in any major capacity. Hence why PsychoPortal is a library - so someone better than me can write a good tool using it!

## Current Status
While PsychoPortal by its nature is always a work in progress, it now supports almost every major format across most versions of the game including the the digital PC re-release as well as the retail and console releases.

## Sister Projects

### Astralathe
A DLL that injects into Psychonauts to provide extended functionality.

Astralathe is a valuable tool for modding as it implements a proper mod structure system for ease of distribution and creation, allows access to debug tools such as log output as well as a level select and debug menu and more.

https://gitlab.com/scrunguscrungus/astralathe

### Psychonauts Lua API Documentation
EXTREMELY WIP documentation of the game's Lua API can be found here. It is massively out of date and pending a rewrite, however.

https://psycholuaapi.readthedocs.io/en/latest/index.html
