﻿namespace PsychoPortal
{
	/// <summary>
	/// A joint animation channel containing its keyframes
	/// </summary>
	public class SharedJointAnim_Channel : IBinarySerializable
	{
		public bool Pre_IsTimeUint { get; set; }
		public bool Pre_HasTime { get; set; }

		public SharedJointAnim_ChannelType Type { get; set; }
		public SharedJointAnim_KeyFrame[] KeyFrames { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Type = s.Serialize<SharedJointAnim_ChannelType>(Type, name: nameof(Type));
			KeyFrames = s.SerializeArraySize<SharedJointAnim_KeyFrame, ushort>(KeyFrames, name: nameof(KeyFrames));

			if (KeyFrames.Length == 0)
			{
				KeyFrames = new SharedJointAnim_KeyFrame[1];
				Pre_HasTime = false;
			}

			KeyFrames = s.SerializeObjectArray<SharedJointAnim_KeyFrame>(KeyFrames, onPreSerialize: (x, _) =>
			{
				x.Pre_IsTimeUint = Pre_IsTimeUint;
				x.Pre_HasTime = Pre_HasTime;
				x.Pre_Type = Type;
			}, name: nameof(KeyFrames));
		}
	}
}