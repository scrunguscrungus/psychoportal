﻿namespace PsychoPortal
{
	public class ProjectedLight : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		// No idea what ANY of these are for, but it does help the library read more levels for now so... figure 'em out later.
		public float Float1 { get; set; }
		public float Float2 { get; set; }
		public float Float3 { get; set; }
		public float Float4 { get; set; }
		public float Float5 { get; set; }
		public int Int1 { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Float1 = s.Serialize<float>(Float1, name: nameof(Float1));
			Float2 = s.Serialize<float>(Float2, name: nameof(Float2));
			Float3 = s.Serialize<float>(Float3, name: nameof(Float3));

			if (Pre_Version >= 305)
			{
				// AttenRange
				Float4 = s.Serialize<float>(Float4, name: nameof(Float4));
				Float5 = s.Serialize<float>(Float5, name: nameof(Float5));
			}

			Int1 = s.Serialize<int>(Int1, name: nameof(Int1));
		}
	}
}
