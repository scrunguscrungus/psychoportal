﻿namespace PsychoPortal
{
	/// <summary>
	/// A compressed <see cref="Plane"/>
	/// </summary>
	public class PlanePacked3 : IBinarySerializable
	{
		public byte[] Bytes_00 { get; set; }

		// TODO: Convert to uncompressed Plane

		public void Serialize(IBinarySerializer s)
		{
			Bytes_00 = s.SerializeArray<byte>(Bytes_00, 6, name: nameof(Bytes_00));
		}
	}
}