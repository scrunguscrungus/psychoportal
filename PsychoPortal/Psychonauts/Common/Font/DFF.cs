﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace PsychoPortal
{
	public class DFF : IBinarySerializable
	{
		public byte[] CharacterMap { get; set; }

		public FontCharacter[] Characters { get; set; }

		//Gets translated to TextureFormat by GameApp::LoadFont
		//Formats 0 and 1 are Format_8888
		//Format 3 is Format_A8
		//Format 2 is not valid
		public int Format { get; set; }

		public int Width { get; set; }
		public int Height { get; set; }

		public byte[] Bitmap { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("DFFF", 4, isReversed: true); //"DFFF" - Double Fine Font File

			_ = s.Serialize<int>(256, name: "Unused"); //Unknown, doesn't get used by the game.

			Characters = s.SerializeArraySize<FontCharacter, int>(Characters, name: nameof(Characters));
			CharacterMap = s.SerializeArray<byte>(CharacterMap, length: 256, name: nameof(CharacterMap)); //Hardcoded to 256

			Characters = s.SerializeObjectArray<FontCharacter>(Characters, name: nameof(Characters));

			Format = s.Serialize<int>(Format, name: nameof(Format));
			Width = s.Serialize<int>(Width, name: nameof(Width));
			Height = s.Serialize<int>(Height, name: nameof(Height));

			Bitmap = s.SerializeArray<byte>(Bitmap, length: Height * Width * 4, name: nameof(Bitmap));
		}
	}
}
