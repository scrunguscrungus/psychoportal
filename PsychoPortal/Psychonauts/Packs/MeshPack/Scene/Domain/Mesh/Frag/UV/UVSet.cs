﻿namespace PsychoPortal
{
	public class UVSet : IBinarySerializable
	{
		public long Pre_UVsCount { get; set; }
		public uint Pre_Version { get; set; }

		public BGRA8888Color Proto_VertexColor { get; set; }
		public UV[] UVs { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_Version < 317)
				Proto_VertexColor = s.SerializeObject<BGRA8888Color>(Proto_VertexColor, name: nameof(Proto_VertexColor));

			UVs = s.SerializeObjectArray<UV>(UVs, Pre_UVsCount, (x, _) => x.Pre_Version = Pre_Version, name: nameof(UVs));
		}
	}
}