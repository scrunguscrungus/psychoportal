﻿namespace PsychoPortal
{
	// NOTE: This might not always work when writing back. Blend files are often broken and the game uses many hacks to read them.

	/// <summary>
	/// A shared blend animation (.pba file). This is usually a part of a <see cref="SharedSkelAnim"/>.
	/// </summary>
	public class SharedBlendAnim : IBinarySerializable
	{
		public long? Pre_Length { get; set; }

		public bool SmallHeaderOnly { get; set; }
		public bool HeaderOnly { get; set; }

		/// <summary>
		/// The file version. This is always 100.
		/// </summary>
		public uint Version { get; set; }

		public uint ChannelsCount { get; set; }

		/// <summary>
		/// The animation channels. If the mesh has multiple blend shapes which should animate then multiple channels will be used.
		/// </summary>
		public SharedBlendAnim_Channel[] Channels { get; set; }

		/// <summary>
		/// Some files have additional unused data
		/// </summary>
		public byte[] UnusedData { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			long size = Pre_Length ?? s.Length;
			long start = s.Position;

			s.SerializeMagicString("EPBA", 4, isReversed: true);
			Version = s.Serialize<uint>(Version, name: nameof(Version));

			// Some files only have a small header
			if (size == 8 || SmallHeaderOnly)
			{
				SmallHeaderOnly = true;
				return;
			}

			ChannelsCount = s.Serialize<uint>(ChannelsCount, name: nameof(ChannelsCount));

			// Some files only have a header
			if (size == 12 || HeaderOnly)
			{
				HeaderOnly = true;
				return;
			}

			long endPos = start + size;

			// In ASGR there is a blend animation with one fewer entries than the count value suggests
			Channels = s.SerializeObjectArrayUntil<SharedBlendAnim_Channel>(Channels,
				(_, i) => i == ChannelsCount - 1 || s.Position == endPos, name: nameof(Channels));

			// Some files (rarely) have additional unused data
			UnusedData = s.SerializeArray<byte>(UnusedData, size - (s.Position - start), name: nameof(UnusedData));
		}
	}
}