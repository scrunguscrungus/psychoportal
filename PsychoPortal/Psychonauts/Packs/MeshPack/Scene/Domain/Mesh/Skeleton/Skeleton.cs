﻿namespace PsychoPortal
{
	public class Skeleton : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public String4b Name { get; set; }
		public uint JointsCount { get; set; }
		public Joint RootJoint { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));
			JointsCount = s.Serialize<uint>(JointsCount, name: nameof(JointsCount));
			RootJoint = s.SerializeObject<Joint>(RootJoint, x => x.Pre_Version = Pre_Version, name: nameof(RootJoint));
		}
	}
}