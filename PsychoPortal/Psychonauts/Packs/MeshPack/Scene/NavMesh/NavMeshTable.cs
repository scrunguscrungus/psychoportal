﻿namespace PsychoPortal
{
	public class NavMeshTable : IBinarySerializable
	{
		public uint Pre_Version { get; set; }
		public int Pre_TableSize { get; set; }

		public int Index { get; set; }
		public byte[] TableBuffer { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_Version > 0x12F)
				Index = s.Serialize<int>(Index, name: nameof(Index));

			TableBuffer = s.SerializeArray<byte>(TableBuffer, Pre_TableSize, name: nameof(TableBuffer));
		}
	}
}