﻿namespace PsychoPortal
{
	public class ScriptDoFile : IBinarySerializable
	{
		public bool Pre_HasName { get; set; }

		public String2b Name { get; set; }
		public byte[] Buffer { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_HasName)
				Name = s.SerializeObject<String2b>(Name, name: nameof(Name));

			Buffer = s.SerializeArraySize<byte, uint>(Buffer, name: nameof(Buffer));
			Buffer = s.SerializeArray<byte>(Buffer, name: nameof(Buffer));
		}
	}
}