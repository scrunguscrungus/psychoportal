﻿namespace PsychoPortal
{
	public class CollisionTree : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public CollisionPoly2[] CollisionPolys { get; set; }
		public Vec3[] Vertices { get; set; }
		public Octree Octree { get; set; }
		public Box3 Bounds { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			CollisionPolys = s.SerializeArraySize<CollisionPoly2, uint>(CollisionPolys, name: nameof(CollisionPolys));
			CollisionPolys = s.SerializeObjectArray<CollisionPoly2>(CollisionPolys, name: nameof(CollisionPolys));
			Vertices = s.SerializeArraySize<Vec3, uint>(Vertices, name: nameof(Vertices));
			Vertices = s.SerializeObjectArray<Vec3>(Vertices, name: nameof(Vertices));

			if (CollisionPolys.Length != 0)
				Octree = s.SerializeObject<Octree>(Octree, x =>
				{
					x.Pre_Version = Pre_Version;
					x.Pre_OldHasPrimitives = true;
				}, name: nameof(Octree));

			Bounds = s.SerializeObject<Box3>(Bounds, name: nameof(Bounds));
		}
	}
}