﻿namespace PsychoPortal
{
	public class SSECube : IBinarySerializable
	{
		public Vec3 Position { get; set; }
		public float Length { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Position = s.SerializeObject<Vec3>(Position, name: nameof(Position));
			Length = s.Serialize<float>(Length, name: nameof(Length));
		}
	}
}