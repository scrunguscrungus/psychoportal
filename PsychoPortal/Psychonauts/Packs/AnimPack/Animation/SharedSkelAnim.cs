﻿namespace PsychoPortal
{
	/// <summary>
	/// A shared skeleton animation containing the joint animations. The event and blend animations can be found
	/// in the <see cref="StubSharedSkelAnim"/>. The file extension for this is .jan.
	/// </summary>
	public class SharedSkelAnim : IBinarySerializable
	{
		/// <summary>
		/// The animation header
		/// </summary>
		public SharedSkelAnimHeader Header { get; set; }

		/// <summary>
		/// The root joint to be animated. This contains the child joints as well.
		/// </summary>
		public SharedJointAnim RootJoint { get; set; }

		public SharedJointAnim V200_ConstRootJoint { get; set; }
		public SharedJointAnim V200_DeltaTransRootJoint { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Header = s.SerializeObject<SharedSkelAnimHeader>(Header, name: nameof(Header));
			RootJoint = s.SerializeObject<SharedJointAnim>(RootJoint, x =>
			{
				x.Pre_IsTimeUint = Header.Version <= 200;
				x.Pre_HasTime = true;
			}, name: nameof(RootJoint));

			// Version 200 sometimes has additional data
			if (Header.Version <= 200)
			{
				// TODO: These type of checks will break when writing
				if (s.Position >= s.Length)
					return;

				// Constant channels
				if (Header.V200_ConstPosKeys + Header.V200_ConstRotKeys + Header.V200_ConstScaleKeys != 0)
					V200_ConstRootJoint = s.SerializeObject<SharedJointAnim>(V200_ConstRootJoint, x =>
					{
						x.Pre_HasTime = false;
					}, name: nameof(V200_ConstRootJoint));

				if (s.Position >= s.Length)
					return;

				// Delta trans
				if ((Header.Flags & 3) != 0)
					V200_ConstRootJoint = s.SerializeObject<SharedJointAnim>(V200_ConstRootJoint, x =>
					{
						x.Pre_IsTimeUint = false;
						x.Pre_HasTime = true;
					}, name: nameof(V200_ConstRootJoint));
			}
		}
	}
}