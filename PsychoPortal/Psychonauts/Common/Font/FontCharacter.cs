﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PsychoPortal
{
	public class FontCharacter : IBinarySerializable
	{
		public Rect Bounds { get; set; }
		public int Baseline { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Bounds = s.SerializeObject<Rect>(Bounds, name: nameof(Bounds));
			Baseline = s.Serialize<int>(Baseline, name: nameof(Baseline));
		}
	}
}
