﻿namespace PsychoPortal
{
	public class ScratchInstallationDirectory : IBinarySerializable
	{
		public String2b Name { get; set; }
		public ScratchInstallationFile[] Files { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String2b>(Name, name: nameof(Name));
			Files = s.SerializeArraySize<ScratchInstallationFile, ushort>(Files, name: nameof(Files));
			Files = s.SerializeObjectArray<ScratchInstallationFile>(Files, name: nameof(Files));
		}
	}
}