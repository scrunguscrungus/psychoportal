﻿namespace PsychoPortal
{
	public interface IBinaryShortLog
	{
		public string ShortLog { get; }
	}
}