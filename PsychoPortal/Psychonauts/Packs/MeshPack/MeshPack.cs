﻿namespace PsychoPortal
{
	public class MeshPack : IBinarySerializable
	{
		public PackedScene[] MeshFiles { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("MPAK", 4);
			MeshFiles = s.SerializeArraySize<PackedScene, ushort>(MeshFiles, name: nameof(MeshFiles));
			MeshFiles = s.SerializeObjectArray<PackedScene>(MeshFiles, name: nameof(MeshFiles));
		}
	}
}