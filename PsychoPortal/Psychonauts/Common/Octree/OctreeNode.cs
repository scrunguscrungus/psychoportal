﻿using System;
using System.ComponentModel;
using System.Linq;

namespace PsychoPortal
{
	public class OctreeNode : IBinarySerializable
	{
		public const int IsLeafFlag = 0x00400000;
		public UInt24[] Data { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Data = s.SerializeArray<UInt24>(Data, 8, name: nameof(Data));
		}
	}
}