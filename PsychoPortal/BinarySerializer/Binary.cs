﻿using System;
using System.IO;

namespace PsychoPortal
{
	/// <summary>
	/// Helper methods for serializing binary data
	/// </summary>
	public static class Binary
	{
		/// <summary>
		/// Reads a binary file using the default deserializer
		/// </summary>
		/// <typeparam name="T">The type to deserialize to</typeparam>
		/// <param name="filePath">The file path</param>
		/// <param name="settings">The settings to use when serializing (defaults to the digital PC version)</param>
		/// <param name="endian">The endianness to use</param>
		/// <param name="logger">An optional logger to use for logging</param>
		/// <param name="onPreSerializing">Optional action to run before serializing</param>
		/// <returns>The serialized file data</returns>
		public static T ReadFromFile<T>(string filePath, PsychonautsSettings settings = null, Endian endian = Endian.Little, IBinarySerializerLogger logger = null, Action<IBinarySerializer, T> onPreSerializing = null)
			where T : IBinarySerializable, new()
		{
			// Open the file as a stream
			using FileStream stream = File.OpenRead(filePath);

			// Read from the stream
			return ReadFromStream<T>(stream, settings, endian, logger, onPreSerializing, name: Path.GetFileName(filePath));
		}

		/// <summary>
		/// Reads from a binary stream using the default deserializer
		/// </summary>
		/// <typeparam name="T">The type to deserialize to</typeparam>
		/// <param name="stream">The stream</param>
		/// <param name="settings">The settings to use when serializing (defaults to the digital PC version)</param>
		/// <param name="endian">The endianness to use</param>
		/// <param name="logger">An optional logger to use for logging</param>
		/// <param name="onPreSerializing">Optional action to run before serializing</param>
		/// <param name="name">An optional name for logging</param>
		/// <returns>The serialized data</returns>
		public static T ReadFromStream<T>(Stream stream, PsychonautsSettings settings = null, Endian endian = Endian.Little, IBinarySerializerLogger logger = null, Action<IBinarySerializer, T> onPreSerializing = null, string name = null)
			where T : IBinarySerializable, new()
		{
			// Create the deserializer
			BinaryDeserializer s = new(stream, settings ?? new PsychonautsSettings(PsychonautsVersion.PC_Digital), endian, logger);

			// Serialize the object
			T obj = s.SerializeObject<T>(default, onPreSerializing != null ? x => onPreSerializing(s, x) : null, name: name);

			// Return the object
			return obj;
		}

		/// <summary>
		/// Reads from a byte array using the default deserializer
		/// </summary>
		/// <typeparam name="T">The type to deserialize to</typeparam>
		/// <param name="buffer">The buffer</param>
		/// <param name="settings">The settings to use when serializing (defaults to the digital PC version)</param>
		/// <param name="endian">The endianness to use</param>
		/// <param name="logger">An optional logger to use for logging</param>
		/// <param name="onPreSerializing">Optional action to run before serializing</param>
		/// <param name="name">An optional name for logging</param>
		/// <returns>The serialized data</returns>
		public static T ReadFromBuffer<T>(byte[] buffer, PsychonautsSettings settings = null, Endian endian = Endian.Little, IBinarySerializerLogger logger = null, Action<IBinarySerializer, T> onPreSerializing = null, string name = null)
			where T : IBinarySerializable, new()
		{
			using MemoryStream memStream = new(buffer);
			return ReadFromStream<T>(memStream, settings, endian, logger, onPreSerializing, name);
		}

		/// <summary>
		/// Writes to a binary file using the default serializer
		/// </summary>
		/// <typeparam name="T">The type to serialize from</typeparam>
		/// <param name="obj">The object to serialize</param>
		/// <param name="filePath">The file path</param>
		/// <param name="settings">The settings to use when serializing (defaults to the digital PC version)</param>
		/// <param name="endian">The endianness to use</param>
		/// <param name="logger">An optional logger to use for logging</param>
		/// <param name="onPreSerializing">Optional action to run before serializing</param>
		public static void WriteToFile<T>(T obj, string filePath, PsychonautsSettings settings = null, Endian endian = Endian.Little, IBinarySerializerLogger logger = null, Action<IBinarySerializer, T> onPreSerializing = null)
			where T : IBinarySerializable, new()
		{
			// Open the file as a stream
			using FileStream stream = File.Create(filePath);

			// Write to the stream
			WriteToStream<T>(obj, stream, settings, endian, logger, onPreSerializing, Path.GetFileName(filePath));
		}

		/// <summary>
		/// Writes to a binary stream using the default serializer
		/// </summary>
		/// <typeparam name="T">The type to serialize from</typeparam>
		/// <param name="obj">The object to serialize</param>
		/// <param name="stream">The stream</param>
		/// <param name="settings">The settings to use when serializing (defaults to the digital PC version)</param>
		/// <param name="endian">The endianness to use</param>
		/// <param name="logger">An optional logger to use for logging</param>
		/// <param name="onPreSerializing">Optional action to run before serializing</param>
		/// <param name="name">An optional name for logging</param>
		public static void WriteToStream<T>(T obj, Stream stream, PsychonautsSettings settings = null, Endian endian = Endian.Little, IBinarySerializerLogger logger = null, Action<IBinarySerializer, T> onPreSerializing = null, string name = null)
			where T : IBinarySerializable, new()
		{
			// Create the serializer
			BinarySerializer s = new(stream, settings ?? new PsychonautsSettings(PsychonautsVersion.PC_Digital), endian, logger);

			// Serialize the object
			s.SerializeObject<T>(obj, onPreSerializing != null ? x => onPreSerializing(s, x) : null, name: name);
		}

		/// <summary>
		/// Writes to a byte array using the default serializer
		/// </summary>
		/// <typeparam name="T">The type to serialize from</typeparam>
		/// <param name="obj">The object to serialize</param>
		/// <param name="settings">The settings to use when serializing (defaults to the digital PC version)</param>
		/// <param name="endian">The endianness to use</param>
		/// <param name="logger">An optional logger to use for logging</param>
		/// <param name="onPreSerializing">Optional action to run before serializing</param>
		/// <param name="name">An optional name for logging</param>
		/// <returns>The buffer</returns>
		public static byte[] WriteToBuffer<T>(T obj, PsychonautsSettings settings = null, Endian endian = Endian.Little, IBinarySerializerLogger logger = null, Action<IBinarySerializer, T> onPreSerializing = null, string name = null)
			where T : IBinarySerializable, new()
		{
			using MemoryStream memStream = new();
			WriteToStream<T>(obj, memStream, settings, endian, logger, onPreSerializing, name);
			return memStream.ToArray();
		}
	}
}