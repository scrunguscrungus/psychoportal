﻿using System;
using System.Linq;

namespace PsychoPortal
{
	public class Rect : IBinarySerializable
	{
		public int MinX { get; set; }
		public int MinY { get; set; }
		public int MaxX { get; set; }
		public int MaxY { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			MinX = s.Serialize<int>(MinX, name: nameof(MinX));
			MinY = s.Serialize<int>(MinY, name: nameof(MinY));
			MaxX = s.Serialize<int>(MaxX, name: nameof(MaxX));
			MaxY = s.Serialize<int>(MaxY, name: nameof(MaxY));
		}
	}
}
