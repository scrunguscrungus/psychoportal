﻿namespace PsychoPortal
{
	/// <summary>
	/// An event animation key frame
	/// </summary>
	public class EventAnim_KeyFrame : IBinarySerializable
	{
		/// <summary>
		/// The time, in 30 fps frames, for when to process this key frame
		/// </summary>
		public uint Time { get; set; }

		public float Float_04 { get; set; } // Always 0?

		public void Serialize(IBinarySerializer s)
		{
			Time = s.Serialize<uint>(Time, name: nameof(Time));
			Float_04 = s.Serialize<float>(Float_04, name: nameof(Float_04));
		}
	}
}