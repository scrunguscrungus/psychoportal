﻿using System;

namespace PsychoPortal
{
	public class TextureFrame : IBinarySerializable
	{
		public uint Uint_00 { get; set; }
		public TextureFormat Format { get; set; }
		public TextureType Type { get; set; }
		public uint Flags { get; set; }
		public uint Width { get; set; }
		public uint Height { get; set; }
		public uint MipMapLevels { get; set; }
		public uint Uint_1C { get; set; }
		public uint Uint_20 { get; set; }
		public uint Uint_24 { get; set; }
		public uint Uint_28 { get; set; }

		public ushort HasPalette { get; set; }
		public byte[] Palette { get; set; }

		public TextureFace[] Faces { get; set; }
		public PS2_Texture[] PS2_Faces { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Uint_00 = s.Serialize<uint>(Uint_00, name: nameof(Uint_00));
			Format = s.Serialize<TextureFormat>(Format, name: nameof(Format));

			if (s.Settings.Version != PsychonautsVersion.PS2)
				s.SerializePadding(3);

			Type = s.Serialize<TextureType>(Type, name: nameof(Type));

			if (s.Settings.Version != PsychonautsVersion.PS2)
				s.SerializePadding(3);

			if (s.Settings.Version == PsychonautsVersion.PS2)
				s.SerializePadding(2);

			Flags = s.Serialize<uint>(Flags, name: nameof(Flags));
			Width = s.Serialize<uint>(Width, name: nameof(Width));
			Height = s.Serialize<uint>(Height, name: nameof(Height));
			MipMapLevels = s.Serialize<uint>(MipMapLevels, name: nameof(MipMapLevels));
			Uint_1C = s.Serialize<uint>(Uint_1C, name: nameof(Uint_1C));
			Uint_20 = s.Serialize<uint>(Uint_20, name: nameof(Uint_20));
			Uint_24 = s.Serialize<uint>(Uint_24, name: nameof(Uint_24));
			Uint_28 = s.Serialize<uint>(Uint_28, name: nameof(Uint_28));

			if (Format == TextureFormat.Format_P8)
			{
				HasPalette = s.Serialize<ushort>(HasPalette, name: nameof(HasPalette));

				if (HasPalette != 0)
					Palette = s.SerializeArray<byte>(Palette, 4 * 256, name: nameof(Palette));
			}

			int facesCount = Type switch
			{
				TextureType.Bitmap => 1,
				TextureType.Cubemap => 6,
				_ => throw new Exception($"Unsupported texture type {Type}"),
			};

			if (s.Settings.Version == PsychonautsVersion.PS2)
			{
				PS2_Faces = s.SerializeObjectArray<PS2_Texture>(PS2_Faces, facesCount, (x, _) => x.Pre_IsPacked = true, name: nameof(PS2_Faces));
			}
			else
			{
				Faces = s.SerializeObjectArray<TextureFace>(Faces, facesCount, (x, _) =>
				{
					x.Pre_Format = Format;
					x.Pre_Width = Width;
					x.Pre_Height = Height;
					x.Pre_MipMapLevels = MipMapLevels;
				}, name: nameof(Faces));
			}
		}
	}
}