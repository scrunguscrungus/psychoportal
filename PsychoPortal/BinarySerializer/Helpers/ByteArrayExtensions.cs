﻿using System.Text;

namespace PsychoPortal
{
	public static class ByteArrayExtensions
	{
		public static string ToHexString(this byte[] bytes, int? align = null, string newLinePrefix = null, int? maxLines = null)
		{
			StringBuilder Result = new(bytes.Length * 2);
			const string HexAlphabet = "0123456789ABCDEF";
			int curLine = 0;
			for (int i = 0; i < bytes.Length; i++)
			{
				if (i > 0 && align.HasValue && i % align == 0)
				{
					curLine++;
					if (curLine >= maxLines)
					{
						Result.Append("...");
						return Result.ToString();
					}
					Result.Append("\n" + newLinePrefix);
				}

				byte B = bytes[i];
				Result.Append(HexAlphabet[B >> 4]);
				Result.Append(HexAlphabet[B & 0xF]);

				if (i < bytes.Length - 1)
					Result.Append(' ');
			}

			return Result.ToString();
		}
	}
}