﻿namespace PsychoPortal
{
	public class PackPack : IBinarySerializable
	{
		public bool Pre_IsCommon { get; set; }

		public TexturePack TexturePack { get; set; }	
		public MeshPack MeshPack { get; set; }	
		public ScriptPack ScriptPack { get; set; }

		public Scene Scene { get; set; }
		public Scene[] ReferencedScenes { get; set; } // Usually LSOs

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("PPAK", 4);
			TexturePack = s.SerializeObject<TexturePack>(TexturePack, name: nameof(TexturePack));
			MeshPack = s.SerializeObject<MeshPack>(MeshPack, name: nameof(MeshPack));
			ScriptPack = s.SerializeObject<ScriptPack>(ScriptPack, name: nameof(ScriptPack));

			// The common pack has no level scene
			if (Pre_IsCommon) 
				return;
			
			Scene = s.SerializeObject<Scene>(Scene, name: nameof(Scene));

			int refScenesLength = Scene.RootDomain.RuntimeReferences.Length;
			ReferencedScenes = s.SerializeObjectArray<Scene>(ReferencedScenes, refScenesLength, name: nameof(ReferencedScenes));
		}
	}
}