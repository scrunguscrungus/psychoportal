﻿using System.Text;

namespace PsychoPortal
{
	/// <summary>
	/// Logs the logs from binary serialization to a string builder
	/// </summary>
	public class BinarySerializerStringLogger : IBinarySerializerLogger
	{
		private bool _reachedMaxLength;

		public StringBuilder StringBuilder { get; } = new();
		public int MaxLength { get; set; } = -1;

		public bool IsActive => !_reachedMaxLength;

		/// <summary>
		/// Writes a new log line
		/// </summary>
		/// <param name="log">The log to write</param>
		public void WriteLogLine(string log)
		{
			if (MaxLength != -1 && StringBuilder.Length >= MaxLength)
			{
				if (!_reachedMaxLength)
				{
					StringBuilder.AppendLine("...");
					_reachedMaxLength = true;
				}

				return;
			}

			StringBuilder.AppendLine(log);
		}

		public void Dispose() { }
	}
}