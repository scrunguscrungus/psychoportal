﻿namespace PsychoPortal
{
	public class String4b : BaseString
	{
		public static implicit operator String4b(string value) => new() { Value = value };

		public override void Serialize(IBinarySerializer s)
		{
			uint ValueLength = s.Serialize<uint>(Value != null ? (uint)Value.Length+1 : 0, name: nameof(ValueLength));
			Value = s.SerializeString(Value, ValueLength, name: nameof(Value));
		}
	}
}