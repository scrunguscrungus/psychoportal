﻿using System;

namespace PsychoPortal
{
	/// <summary>
	/// A compressed <see cref="Quat"/>
	/// </summary>
	public class CompQuat : IBinarySerializable, IBinaryShortLog
	{
		public int Int_00 { get; set; }
		public short Short_04 { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => ToQuat().ShortLog;

		public void Serialize(IBinarySerializer s)
		{
			Int_00 = s.Serialize<int>(Int_00, name: nameof(Int_00));
			Short_04 = s.Serialize<short>(Short_04, name: nameof(Short_04));
		}

		public Quat ToQuat()
		{
			int[] local_62 = new int[3];
			float[] local_5c = new float[4];
			int[] local_4c = new int[7];

			int local_2c = Int_00 >> 0x1d;
			uint uVar1 = (uint)(Int_00 ^ local_2c << 0x1d);
			local_4c[0] = (int)uVar1 >> 0x1c;
			uVar1 = (uint)(uVar1 ^ local_4c[0] << 0x1c);
			local_4c[3] = (int)uVar1 >> 0xe;
			uVar1 = (uint)(uVar1 ^ local_4c[3] << 0xe);
			local_4c[1] = (int)uVar1 >> 0xd;
			local_4c[4] = (int)(uVar1 ^ local_4c[1] << 0xd);
			local_4c[6] = 0;
			local_4c[2] = Short_04 >> 0xe;
			uint local_30 = (uint)(Short_04 ^ local_4c[2] << 0xe);
			local_4c[5] = (int)local_30 >> 1;
			local_30 = (uint)(local_30 ^ local_4c[5] * 2);
			uint local_28 = local_30;
			var local_24 = 0;

			int iVar2 = 0;

			for (int local_e = 0; local_e < 4; local_e += 1) 
			{
				iVar2 = local_2c;

				if (local_e == local_2c) 
					continue;
				
				local_5c[local_e] = local_4c[local_24 + 3] / 8192.0f;

				if (local_4c[local_24] == 1)
					local_5c[local_e] = -local_5c[local_e];

				local_62[local_24] = local_e;
				local_24 += 1;
			}

			float local_14 = 1.0f - (local_5c[local_62[2]] * local_5c[local_62[2]] + 
			                         local_5c[local_62[1]] * local_5c[local_62[1]] + 
			                         local_5c[local_62[0]] * local_5c[local_62[0]]);
			float fVar3 = (float)Math.Sqrt(local_14);
			
			local_5c[iVar2] = fVar3;
			
			if (local_28 == 1)
				local_5c[local_2c] = -local_5c[local_2c];

			return new Quat(local_5c[0], local_5c[1], local_5c[2], local_5c[3]);
		}
	}
}