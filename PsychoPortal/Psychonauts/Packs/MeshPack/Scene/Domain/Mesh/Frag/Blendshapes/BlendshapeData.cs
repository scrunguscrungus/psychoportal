﻿namespace PsychoPortal
{
	public class BlendshapeData : IBinarySerializable
	{
		public uint Pre_Version { get; set; }
		public int Pre_MeshVerticesCount { get; set; }
		public byte Pre_Flags { get; set; }

		public BlendshapeStream[] Streams { get; set; }

		public uint[] PS2_UnknownUints { get; set; } // Offsets?
		public PS2_UnknownBlendShapeItem[] PS2_UnknownItems { get; set; }
		public uint PS2_UnknownUint1 { get; set; }
		public uint PS2_UnknownUint2 { get; set; }
		public String4b PS2_Name { get; set; } // "baselist"

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("SPBS", 4, isReversed: true); // Start tag

			if (s.Settings.Version == PsychonautsVersion.PS2)
			{
				PS2_UnknownUints = s.SerializeArraySize<uint, uint>(PS2_UnknownUints, name: nameof(PS2_UnknownUints));
				PS2_UnknownUints = s.SerializeArray<uint>(PS2_UnknownUints, name: nameof(PS2_UnknownUints));
				Streams = s.SerializeArraySize<BlendshapeStream, uint>(Streams, name: nameof(Streams));
				PS2_UnknownItems = s.SerializeArraySize<PS2_UnknownBlendShapeItem, uint>(PS2_UnknownItems, name: nameof(PS2_UnknownItems));
				PS2_UnknownUint1 = s.Serialize<uint>(PS2_UnknownUint1, name: nameof(PS2_UnknownUint1));
				PS2_UnknownUint2 = s.Serialize<uint>(PS2_UnknownUint2, name: nameof(PS2_UnknownUint2));
				PS2_Name = s.SerializeObject<String4b>(PS2_Name, name: nameof(PS2_Name));
				PS2_UnknownItems = s.SerializeObjectArray<PS2_UnknownBlendShapeItem>(PS2_UnknownItems, name: nameof(PS2_UnknownItems));
			}
			else
			{
				Streams = s.SerializeArraySize<BlendshapeStream, byte>(Streams, name: nameof(Streams));
			}

			Streams = s.SerializeObjectArray<BlendshapeStream>(Streams, onPreSerialize: (x, _) =>
			{
				x.Pre_Version = Pre_Version;
				x.Pre_MeshVerticesCount = Pre_MeshVerticesCount;
				x.Pre_Flags = Pre_Flags;
			}, name: nameof(Streams));

			s.SerializeMagicString("EPBS", 4, isReversed: true); // End tag
		}
	}
}