﻿namespace PsychoPortal
{
	// A uint on all versions besides PS2
	public enum TextureFormat : byte
	{
		Format_8888 = 0,
		Format_0888 = 1,
		Format_4444 = 2,
		Format_1555 = 3,
		Format_0555 = 4,
		Format_565 = 5,
		Format_A8 = 6, // Alpha
		Format_L8 = 7, // Luminance
		Format_AL8 = 8,
		Format_DXT1 = 9, // Compressed
		Format_DXT3 = 10, // Compressed
		Format_DXT5 = 11, // Compressed
		Format_V8U8 = 12,
		Format_V16U16 = 13,
		Format_P8 = 14, // Paletted
	}
}