﻿using System;

namespace PsychoPortal
{
	/// <summary>
	/// Interface for a logger for a binary serializer
	/// </summary>
	public interface IBinarySerializerLogger : IDisposable
	{
		/// <summary>
		/// Indicates if this logger is currently active
		/// </summary>
		bool IsActive { get; }

		/// <summary>
		/// Writes a new log line
		/// </summary>
		/// <param name="log">The log to write</param>
		void WriteLogLine(string log);
	}
}