﻿namespace PsychoPortal
{
	public class AnimPack : IBinarySerializable
	{
		public StubSharedSkelAnim[] StubSharedAnims { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("APAK", 4, isReversed: true);

			StubSharedAnims = s.SerializeArraySize<StubSharedSkelAnim, uint>(StubSharedAnims, name: nameof(StubSharedAnims));
			StubSharedAnims = s.SerializeObjectArray<StubSharedSkelAnim>(StubSharedAnims, name: nameof(StubSharedAnims));
		}
	}
}