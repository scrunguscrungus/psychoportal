﻿namespace PsychoPortal
{
	public class DomainEntityInfo : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public String4b Name { get; set; }
		public String4b ScriptClass { get; set; }
		public int HasEditVars { get; set; }
		public String4b EditVars { get; set; }
		public Vec3 Position { get; set; }
		public Vec3 Rotation { get; set; }
		public Vec3 Scale { get; set; }
		public uint UnknownUint { get; set; }

		public int NewInt1 { get; set; }
		public int NewInt2 { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));
			ScriptClass = s.SerializeObject<String4b>(ScriptClass, name: nameof(ScriptClass));
			HasEditVars = s.Serialize<int>(HasEditVars, name: nameof(HasEditVars));

			if (HasEditVars != 0)
				EditVars = s.SerializeObject<String4b>(EditVars, name: nameof(EditVars));

			Position = s.SerializeObject<Vec3>(Position, name: nameof(Position));
			Rotation = s.SerializeObject<Vec3>(Rotation, name: nameof(Rotation));
			Scale = s.SerializeObject<Vec3>(Scale, name: nameof(Scale));
			UnknownUint = s.Serialize<uint>(UnknownUint, name: nameof(UnknownUint));

			if (Pre_Version > 0x12E)
			{
				NewInt1 = s.Serialize<int>(NewInt1, name: nameof(NewInt1));
				NewInt2 = s.Serialize<int>(NewInt2, name: nameof(NewInt2));
			}
		}
	}
}