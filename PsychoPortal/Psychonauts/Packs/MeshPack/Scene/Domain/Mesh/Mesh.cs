﻿namespace PsychoPortal
{
	public class Mesh : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public String4b Name { get; set; } // "DomainFirstMesh" is checked for
		public Vec3 Position { get; set; }
		public Vec3 Rotation { get; set; }
		public Vec3 Scale { get; set; }
		public Box3 Bounds { get; set; }
		public TriggerOBB[] Triggers { get; set; }
		public uint EntityFlags { get; set; }
		public EntityMeshInfo EntityMeshInfo { get; set; }
		public byte LODCount { get; set; }
		public float[] LODs { get; set; }
		public Light[] Lights { get; set; }
		public Skeleton[] Skeletons { get; set; }
		public MeshFrag[] MeshFrags { get; set; }
		public AnimAffector[] AnimAffectors { get; set; }
		public uint HasCollisionTree { get; set; }
		public CollisionTree CollisionTree { get; set; }
		public Mesh[] Children { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));
			Position = s.SerializeObject<Vec3>(Position, name: nameof(Position));
			Rotation = s.SerializeObject<Vec3>(Rotation, name: nameof(Rotation));
			Scale = s.SerializeObject<Vec3>(Scale, name: nameof(Scale));
			Bounds = s.SerializeObject<Box3>(Bounds, name: nameof(Bounds));
			Triggers = s.SerializeArraySize<TriggerOBB, uint>(Triggers, name: nameof(Triggers));
			Triggers = s.SerializeObjectArray<TriggerOBB>(Triggers, name: nameof(Triggers));
			EntityFlags = s.Serialize<uint>(EntityFlags, name: nameof(EntityFlags));

			if ((EntityFlags & 1) != 0)
				EntityMeshInfo = s.SerializeObject<EntityMeshInfo>(EntityMeshInfo, name: nameof(EntityMeshInfo));

			if ((EntityFlags & 2) != 0)
			{
				LODCount = s.Serialize<byte>(LODCount, name: nameof(LODCount));
				LODs = s.SerializeArray<float>(LODs, LODCount - 1, name: nameof(LODs));
			}

			Lights = s.SerializeArraySize<Light, uint>(Lights, name: nameof(Lights));
			Lights = s.SerializeObjectArray<Light>(Lights, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(Lights));

			Skeletons = s.SerializeArraySize<Skeleton, uint>(Skeletons, name: nameof(Skeletons));
			Skeletons = s.SerializeObjectArray<Skeleton>(Skeletons, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(Skeletons));

			MeshFrags = s.SerializeArraySize<MeshFrag, uint>(MeshFrags, name: nameof(MeshFrags));
			MeshFrags = s.SerializeObjectArray<MeshFrag>(MeshFrags, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(MeshFrags));

			AnimAffectors = s.SerializeArraySize<AnimAffector, uint>(AnimAffectors, name: nameof(AnimAffectors));
			AnimAffectors = s.SerializeObjectArray<AnimAffector>(AnimAffectors, name: nameof(AnimAffectors));

			HasCollisionTree = s.Serialize<uint>(HasCollisionTree, name: nameof(HasCollisionTree));

			if (HasCollisionTree != 0)
				CollisionTree = s.SerializeObject<CollisionTree>(CollisionTree, x => x.Pre_Version = Pre_Version, name: nameof(CollisionTree));

			Children = s.SerializeArraySize<Mesh, uint>(Children, name: nameof(Children));
			Children = s.SerializeObjectArray<Mesh>(Children, onPreSerialize: (x, _) => x.Pre_Version = Pre_Version, name: nameof(Children));
		}
	}
}