﻿namespace PsychoPortal
{
	public class CollisionPoly2 : IBinarySerializable
	{
		public SurfaceFlags SurfaceFlags { get; set; }
		public ushort[] VertexIndices { get; set; }
		public float Float_0C { get; set; } // Game multiplies by this to get triangle plane value
		public byte? NavMeshIndex { get; set; } // Null is SelectedNavMesh
		public UInt24 NavPolyIndex { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			SurfaceFlags = s.Serialize<SurfaceFlags>(SurfaceFlags, name: nameof(SurfaceFlags));
			VertexIndices = s.SerializeArray<ushort>(VertexIndices, 3, name: nameof(VertexIndices));
			s.SerializePadding(2);
			Float_0C = s.Serialize<float>(Float_0C, name: nameof(Float_0C));
			NavMeshIndex = s.Serialize<byte?>(NavMeshIndex, name: nameof(NavMeshIndex));
			NavPolyIndex = s.Serialize<UInt24>(NavPolyIndex, name: nameof(NavPolyIndex));
		}
	}
}