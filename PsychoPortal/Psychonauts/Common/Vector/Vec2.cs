﻿namespace PsychoPortal
{
	/// <summary>
	/// A 2-dimensional vector
	/// </summary>
	public class Vec2 : IBinarySerializable, IBinaryShortLog
	{
		public Vec2() { }
		public Vec2(float x, float y)
		{
			X = x;
			Y = y;
		}

		public float X { get; set; }
		public float Y { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => $"Vec2({X}, {Y})";

		public void Serialize(IBinarySerializer s)
		{
			X = s.Serialize<float>(X, name: nameof(X));
			Y = s.Serialize<float>(Y, name: nameof(Y));
		}
	}
}