﻿using System;

namespace PsychoPortal
{
	/// <summary>
	/// A joint animation key frame
	/// </summary>
	public class SharedJointAnim_KeyFrame : IBinarySerializable
	{
		public bool Pre_IsTimeUint { get; set; }
		public bool Pre_HasTime { get; set; }
		public SharedJointAnim_ChannelType Pre_Type { get; set; }

		/// <summary>
		/// The time, in 30 fps frames, for when to process this key frame
		/// </summary>
		public uint Time { get; set; }

		public Vec3 Position { get; set; }
		public Vec3 Scale { get; set; }
		public Quat Rotation { get; set; }
		public CompQuat CompressedRotation { get; set; }
		public PS2_CompQuat PS2_Rotation { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			if (Pre_HasTime)
			{
				if (Pre_IsTimeUint) // Old format
					Time = s.Serialize<uint>(Time, name: nameof(Time));
				else
					Time = s.Serialize<ushort>((ushort)Time, name: nameof(Time));
			}

			if (Pre_Type == SharedJointAnim_ChannelType.Position)
			{
				Position = s.SerializeObject<Vec3>(Position, name: nameof(Position));
			}
			else if (Pre_Type == SharedJointAnim_ChannelType.Scale)
			{
				Scale = s.SerializeObject<Vec3>(Scale, name: nameof(Scale));
			}
			else if (Pre_Type == SharedJointAnim_ChannelType.Rotation)
			{
				if (s.Settings.Version == PsychonautsVersion.PS2)
					PS2_Rotation = s.SerializeObject<PS2_CompQuat>(PS2_Rotation, name: nameof(PS2_Rotation));
				else
					Rotation = s.SerializeObject<Quat>(Rotation, name: nameof(Rotation));
			}
			else if (Pre_Type == SharedJointAnim_ChannelType.CompressedRotation &&
			         s.Settings.Version != PsychonautsVersion.PS2)
			{
				CompressedRotation = s.SerializeObject<CompQuat>(CompressedRotation, name: nameof(CompressedRotation));
			}
			else
			{
				throw new Exception($"Invalid channel type {Pre_Type}");
			}
		}
	}
}