﻿namespace PsychoPortal
{
	/// <summary>
	/// A shared joint animation. This is usually a part of a <see cref="SharedSkelAnim"/> and animates the joint of a skeleton.
	/// </summary>
	public class SharedJointAnim : IBinarySerializable
	{
		public bool Pre_IsTimeUint { get; set; }
		public bool Pre_HasTime { get; set; }

		/// <summary>
		/// The joint name. This is only a single name, so if a joint has multiple combined names this won't include all of them.
		/// On PS2 this is empty. The game doesn't use this to find the matching joint, but rather does so based off of the order they appear
		/// starting from the root bone defined in <see cref="StubSharedSkelAnim.RootJointName"/>.
		/// </summary>
		public String4b Name { get; set; }

		/// <summary>
		/// The channels. Each channel will animate the joint differently, modifying its position, rotation or scale.
		/// </summary>
		public SharedJointAnim_Channel[] Channels { get; set; }

		/// <summary>
		/// The child joints
		/// </summary>
		public SharedJointAnim[] Children { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));
			Channels = s.SerializeArraySize<SharedJointAnim_Channel, ushort>(Channels, name: nameof(Channels));
			Channels = s.SerializeObjectArray<SharedJointAnim_Channel>(Channels, onPreSerialize: (x, _) =>
			{
				x.Pre_IsTimeUint = Pre_IsTimeUint;
				x.Pre_HasTime = Pre_HasTime;
			}, name: nameof(Channels));
			Children = s.SerializeArraySize<SharedJointAnim, ushort>(Children, name: nameof(Children));
			Children = s.SerializeObjectArray<SharedJointAnim>(Children, onPreSerialize: (x, _) =>
			{
				x.Pre_IsTimeUint = Pre_IsTimeUint;
				x.Pre_HasTime = Pre_HasTime;
			}, name: nameof(Children));
		}
	}
}