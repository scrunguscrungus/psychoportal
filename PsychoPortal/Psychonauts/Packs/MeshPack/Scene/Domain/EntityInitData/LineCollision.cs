﻿namespace PsychoPortal
{
	public class LineCollision : IBinarySerializable
	{
		public Vec3[] Positions { get; set; }
		public Vec3[] Normals { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("POSN", 4, isReversed: true);
			Positions = s.SerializeArraySize<Vec3, uint>(Positions, name: nameof(Positions));
			Positions = s.SerializeObjectArray<Vec3>(Positions, name: nameof(Positions));

			s.SerializeMagicString("NORM", 4, isReversed: true);
			Normals = s.SerializeArraySize<Vec3, uint>(Normals, name: nameof(Normals));
			Normals = s.SerializeObjectArray<Vec3>(Normals, name: nameof(Normals));
		}
	}
}