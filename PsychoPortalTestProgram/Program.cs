﻿using System;
using System.IO;
using System.Threading.Tasks;
using PsychoPortal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Collections.Generic;

namespace PsychoPortalTestProgram
{
	public struct LuaClassIndex
	{
		public string[] ClassLoadOrder;
	}
	class Program
	{
		static string GetInputFile(string prompt = null)
		{
			//Reads a line, but trims quotes to ensure that dragdropped files work
			if (prompt != null)
				Console.Write($"{prompt}: ");
			return Console.ReadLine().Trim('"'); ;
		}

		static async Task Main(string[] args)
		{
			Dumper dumper = new Dumper();

			//TODO: This is gross, refactor
			StringBuilder optionsList = new StringBuilder();

			optionsList.AppendLine("Mode:");
			optionsList.AppendLine("\t\t1. PPF - Full Dump");
			//optionsList.AppendLine("\t\t2. ISB Test");
			optionsList.AppendLine();
			Console.Write(optionsList);

			while ( true )
			{
				Console.Write("Choice: ");
				switch ( int.Parse(Console.ReadLine() ))
				{
					case 1:
						{
							string ppfFile = GetInputFile("PPF file");
							string outDir = GetInputFile("Output directory");
							PackPack ppf = Binary.ReadFromFile<PackPack>(ppfFile);
							await dumper.DumpPPF(ppf, outDir);
						}
						break;
						/*
					case 2:
						{
							using (FileStream fs = File.OpenRead(inFile))
							{
								ISB isb = null;
								try
								{
									isb = Binary.ReadFromStream<ISB>(fs, logger: logger_d, name: "Read Test");
									Console.WriteLine($"Read Success");
								}
								catch ( Exception except )
								{
									Console.WriteLine($"Read's buggered: {except.Message}");
									return;
								}
								
								Binary.WriteToFile(isb, "./isbtest.isb", logger: logger_s);
								Console.WriteLine("Write success");
							}
						}
						break;
						*/
					default:
						Console.WriteLine("Not implemented or invalid choice");
						break;
				}
			}
			

		}
	}
}
