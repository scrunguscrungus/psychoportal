﻿using System.IO;

namespace PsychoPortal
{
	public class ScratchInstallation : IBinarySerializable
	{
		public ScratchInstallationDirectory[] Directories { get; set; }

		public void ExportFiles(string outputDir, Stream fileStream)
		{
			foreach (ScratchInstallationDirectory dir in Directories)
			{
				foreach (ScratchInstallationFile file in dir.Files)
				{
					byte[] fileBuffer = new byte[file.FileSize];
					fileStream.Position = file.FileDataOffset;
					int read = fileStream.Read(fileBuffer, 0, fileBuffer.Length);

					if (read != fileBuffer.Length)
						throw new EndOfStreamException();

					string outputFileDir = Path.Combine(outputDir, dir.Name);
					string outputFile = Path.Combine(outputFileDir, file.Name);

					Directory.CreateDirectory(outputFileDir);
					File.WriteAllBytes(outputFile, fileBuffer);
				}
			}
		}

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("SCIN", 4);
			Directories = s.SerializeObjectArrayUntil(Directories, (_, _) => s.Position >= s.Length, name: nameof(Directories));
		}
	}
}