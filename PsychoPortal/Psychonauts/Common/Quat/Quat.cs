﻿namespace PsychoPortal
{
	/// <summary>
	/// A quaternion
	/// </summary>
	public class Quat : IBinarySerializable, IBinaryShortLog
	{
		public Quat() { }
		public Quat(float x, float y, float z, float w)
		{
			X = x;
			Y = y;
			Z = z;
			W = w;
		}

		public float X { get; set; }
		public float Y { get; set; }
		public float Z { get; set; }
		public float W { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => $"Quat({X}, {Y}, {Z}, {W})";

		public Mat4 ToMatrix()
		{
			Mat4 mat = new();
			
			mat.M00 = (Z * Z + Y * Y) * -2.0f + 1.0f;
			
			float v = W * Z + Y * X;
			mat.M10 = v + v;
			
			v = Z * X - W * Y;
			mat.M20 = v + v;
			
			v = Y * X - W * Z;
			mat.M01 = v + v;
			mat.M11 = (Z * Z + X * X) * -2.0f + 1.0f;
			
			v = W * X + Z * Y;
			mat.M21 = v + v;
			
			v = W * Y + Z * X;
			mat.M02 = v + v;
			
			v = Z * Y - W * X;
			mat.M12 = v + v;
			mat.M22 = (Y * Y + X * X) * -2.0f + 1.0f;
			
			return mat;
		}

		public Vec3 ToEuler()
		{
			Mat4 mat = ToMatrix();

			Vec3 eul = new();
			Vec3 tempVec = new(mat.M00, mat.M10, mat.M20);

			if (MathHelpers.Abs(tempVec.X) < 1e-05f && MathHelpers.Abs(tempVec.Y) < 1e-05f)
				eul.Z = 0.0f;
			else
				eul.Z = MathHelpers.ArcTan(tempVec.Y, tempVec.X);
			
			if (-90.0 <= eul.Z)
			{
				if (90.0 < eul.Z)
					eul.Z -= 180.0f;
			}
			else
			{
				eul.Z += 180.0f;
			}

			mat = Mat4.Mul(Mat4.RotateZ(-eul.Z), mat);
			tempVec = new Vec3(mat.M00, mat.M10, mat.M20);
			eul.Y = MathHelpers.ArcTan(-tempVec.Z, tempVec.X);

			mat = Mat4.Mul(Mat4.RotateY(-eul.Y), mat);
			tempVec = new Vec3(mat.M01, mat.M11, mat.M21);
			eul.X = MathHelpers.ArcTan(tempVec.Z, tempVec.Y);

			return eul;
		}

		public void Serialize(IBinarySerializer s)
		{
			X = s.Serialize<float>(X, name: nameof(X));
			Y = s.Serialize<float>(Y, name: nameof(Y));
			Z = s.Serialize<float>(Z, name: nameof(Z));
			W = s.Serialize<float>(W, name: nameof(W));
		}
	}
}