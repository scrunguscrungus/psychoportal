﻿namespace PsychoPortal
{
	public class String1b : BaseString
	{
		public static implicit operator String1b(string value) => new() { Value = value };

		public override void Serialize(IBinarySerializer s)
		{
			byte ValueLength = s.Serialize<byte>(Value != null ? (byte)(Value.Length+1) : (byte)0, name: nameof(ValueLength));
			Value = s.SerializeString(Value, ValueLength, name: nameof(Value));
		}
	}
}