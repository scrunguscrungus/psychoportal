﻿using System;
using System.Text;

namespace PsychoPortal
{
	public static class BinarySerializerExtensions
	{
		public static void SerializeMagic<T>(this IBinarySerializer s, T magic, bool throwIfNoMatch = true, string name = null)
		{
			T value = s.Serialize<T>(magic, name: name ?? "Magic");

			if (throwIfNoMatch && !value.Equals(magic))
				throw new Exception($"Magic '{value}' does not match expected magic of '{magic}'");
		}

		public static void SerializeMagicString(this IBinarySerializer s, string magic, int length, bool isReversed = false, Encoding encoding = null, bool throwIfNoMatch = true, string name = null)
		{
			if (isReversed)
			{
				char[] charArray = magic.ToCharArray();
				Array.Reverse(charArray);
				magic = new string(charArray);
			}

			string value = s.SerializeString(magic, length, encoding: encoding, name: name ?? "Magic");

			if (!throwIfNoMatch)
				return;

			if (value != magic)
				throw new Exception($"Magic '{value}' does not match expected magic of '{magic}'");
		}

		public static void DoAt(this IBinarySerializer s, long position, Action action)
		{
			long currentPos = s.Position;
			
			s.GoTo(position);

			try
			{
				action();
			}
			finally
			{
				s.GoTo(currentPos);
			}
		}

		/// <summary>
		/// Serializes data prefixed with a data length
		/// </summary>
		/// <typeparam name="T">The data length value type</typeparam>
		/// <param name="s">The serializer</param>
		/// <param name="action">The data serialization action. The value passed in is the size. When writing this will be 0.</param>
		public static void DoWithPrefixedSize<T>(this IBinarySerializer s, Action<T> action)
		{
			// Keep track of the position for the length value
			long lengthPos = s.Position;
			
			// Serialize the prefixed length
			T length = s.Serialize<T>(default, name: "Length");
			
			// Convert the length to an int
			int lengthInt = Convert.ToInt32(length);

			// Keep track of the position for the start of the data
			long dataPos = s.Position;

			// Serialize the data
			action(length);

			// Calculate the length of the serialized data
			int newLength = (int)(s.Position - dataPos);

			if (s is BinaryDeserializer && newLength != lengthInt)
				throw new Exception($"Serialized data doesn't match the length at {dataPos:X8}");

			// If the length doesn't match we re-serialize it with the calculated value
			if (lengthInt != newLength)
				s.DoAt(lengthPos, () => s.Serialize<T>((T)Convert.ChangeType(newLength, typeof(T)), name: "Length"));
		}

		public static void SerializePadding(this IBinarySerializer s, int length, string name = "Padding")
		{
			if (length == 1)
				s.Serialize<byte>(default, name: name);
			else
				s.SerializeArray<byte>(new byte[length], length, name: name);
		}
	}
}