﻿namespace PsychoPortal
{
	public class AnimAffector_KeyFrame : IBinarySerializable
	{
		public int Time { get; set; } // The frame index this should start being used from
		public Vec3 Vector { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Time = s.Serialize<int>(Time, name: nameof(Time));
			Vector = s.SerializeObject<Vec3>(Vector, name: nameof(Vector));
		}
	}
}