﻿using System;

namespace PsychoPortal
{
	public static class TextureFormatExtensions
	{
		public static bool IsCompressed(this TextureFormat format) => 
			format is TextureFormat.Format_DXT1 or TextureFormat.Format_DXT3 or TextureFormat.Format_DXT5;

		public static int GetUncompressedBytesPerPixel(this TextureFormat format)
		{
			if ((uint)format >= 15)
				throw new ArgumentOutOfRangeException(nameof(format), format, null);

			int uVar2 = 1 << ((byte)format & 0x1f);

			if ((uVar2 & 0x41c0) != 0)
				return 1;

			if ((uVar2 & 0x103c) != 0)
				return 2;

			if ((uVar2 & 0x2003) != 0)
				return 4;

			throw new ArgumentException("The format is compressed", nameof(format));
		}
	}
}