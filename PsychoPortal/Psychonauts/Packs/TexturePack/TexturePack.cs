﻿using System;
using System.Linq;

namespace PsychoPortal
{
	public class TexturePack : IBinarySerializable
	{
		public bool HasPackHeader { get; set; }
		public ushort HasTextureHeader { get; set; }
		public LocalizedTextures[] LocalizedTextures { get; set; }
		public GameTexture[] Textures { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			ushort controlValue = (ushort?)Textures?.Length ?? 0;

			if (LocalizedTextures?.Length > 0)
				controlValue = 0xFFFF;

			if (HasPackHeader)
			{
				s.Serialize<ushort>(0xFDFD, name: "HeaderSignature");
			}
			else
			{
				controlValue = s.Serialize<ushort>(controlValue, name: "ControlValue");
				HasPackHeader = controlValue == 0xFDFD; // MAGIC_VERSION_TEXTURE_MARKER
			}

			if (HasPackHeader)
			{
				HasTextureHeader = s.Serialize<ushort>(HasTextureHeader, name: nameof(HasTextureHeader));
				controlValue = s.Serialize<ushort>(controlValue, name: "ControlValue");
			}

			bool hasTextureHeader = HasPackHeader && HasTextureHeader != 0;

			if (controlValue == 0xFFFF)
				LocalizedTextures = s.SerializeObjectArrayUntil<LocalizedTextures>(LocalizedTextures, 
					conditionCheckFunc: (x, _) => x.PrimaryTexturesCount != 0xFFFF, 
					onPreSerialize: (x, _) => x.Pre_HasTextureHeader = hasTextureHeader, 
					name: nameof(LocalizedTextures));
			else
				LocalizedTextures = Array.Empty<LocalizedTextures>();

			ushort texturesCount = LocalizedTextures?.LastOrDefault()?.PrimaryTexturesCount ?? controlValue;
			Textures = s.SerializeObjectArray<GameTexture>(Textures, texturesCount, (x, _) => x.Pre_HasTextureHeader = hasTextureHeader, name: nameof(Textures));
		}
	}
}