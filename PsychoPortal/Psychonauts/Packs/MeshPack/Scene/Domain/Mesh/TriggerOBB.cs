﻿namespace PsychoPortal
{
	public class TriggerOBB : IBinarySerializable
	{
		public String4b Name { get; set; }
		public string TriggerName => IsPlane ? PlaneName : Name;

		public Vec3 PlaneNormal { get; set; }
		public String4b PlaneName { get; set; }

		public Vec3 Position { get; set; }
		public Vec3 Rotation { get; set; } // In radians
		public Box3 Bounds { get; set; }
		public Box3 UnusedBounds { get; set; }
		
		public bool IsPlane => Name == "PLANE";

		public void Serialize(IBinarySerializer s)
		{
			Name = s.SerializeObject<String4b>(Name, name: nameof(Name));

			if (IsPlane)
			{
				PlaneNormal = s.SerializeObject<Vec3>(PlaneNormal, name: nameof(PlaneNormal));
				PlaneName = s.SerializeObject<String4b>(PlaneName, name: nameof(PlaneName));
			}

			Position = s.SerializeObject<Vec3>(Position, name: nameof(Position));
			Rotation = s.SerializeObject<Vec3>(Rotation, name: nameof(Rotation));
			Bounds = s.SerializeObject<Box3>(Bounds, name: nameof(Bounds));
			UnusedBounds = s.SerializeObject<Box3>(UnusedBounds, name: nameof(UnusedBounds));
		}
	}
}