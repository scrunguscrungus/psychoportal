﻿using System;
using System.IO;
using System.Text;

namespace PsychoPortal
{
	/// <summary>
	/// Logs the logs from binary serialization to a stream or file
	/// </summary>
	public class BinarySerializerLogger : IBinarySerializerLogger
	{
		/// <summary>
		/// Creates a new logger which creates a new log file
		/// </summary>
		/// <param name="logPath">The log file path</param>
		public BinarySerializerLogger(string logPath)
		{
			_filePath = logPath;
			_outputStream = new StreamWriter(logPath);
		}

		/// <summary>
		/// Creates a new logger from the stream
		/// </summary>
		/// <param name="stream">The log output stream</param>
		public BinarySerializerLogger(Stream stream)
		{
			_outputStream = new StreamWriter(stream);
		}

		/// <summary>
		/// Creates a new logger from the stream with a buffer size
		/// </summary>
		/// <param name="stream">The log output stream</param>
		/// <param name="bufferSize">The buffer size</param>
		public BinarySerializerLogger(Stream stream, int bufferSize)
		{
			_outputStream = new StreamWriter(stream, Encoding.UTF8, bufferSize);
		}

		private readonly string _filePath;
		private bool _isDisposed;
		private StreamWriter _outputStream;

		/// <summary>
		/// The file stream
		/// </summary>
		protected StreamWriter OutputStream
		{
			get
			{
				if (_isDisposed)
				{
					if (_filePath == null)
						throw new ObjectDisposedException("The logger has been disposed and can't be reopened");
					
					// Reopen if we have a file path
					_outputStream = new StreamWriter(_filePath, append: true);
					_isDisposed = false;
				}

				return _outputStream;
			}
		}

		public bool IsActive => !_isDisposed || _filePath != null;

		/// <summary>
		/// Writes a new log line
		/// </summary>
		/// <param name="log">The log to write</param>
		public void WriteLogLine(string log)
		{
			OutputStream.WriteLine(log);
		}

		public void Dispose()
		{
			if (_isDisposed)
				return;

			OutputStream?.Dispose();
			_isDisposed = true;
		}
	}
}