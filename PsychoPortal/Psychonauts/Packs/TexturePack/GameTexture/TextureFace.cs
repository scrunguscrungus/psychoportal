﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PsychoPortal
{
	public class TextureFace : IBinarySerializable
	{
		public TextureFormat Pre_Format { get; set; }
		public uint Pre_Width { get; set; }
		public uint Pre_Height { get; set; }
		public uint Pre_MipMapLevels { get; set; }

		public MipSurface[] Surfaces { get; set; }

		public IEnumerable<(uint Width, uint Heigth)> GetMipMapLevels()
		{
			// If MipMapLevels is 0 we do as many mipmaps as the size supports
			int count = Pre_MipMapLevels == 0 ? -1 : (int)Pre_MipMapLevels;

			uint w = Pre_Width;
			uint h = Pre_Height;

			while ((w != 0 || h != 0) && count != 0)
			{
				yield return (w, h);
				count--;
				w >>= 1;
				h >>= 1;
			}
		}

		public void Serialize(IBinarySerializer s)
		{
			(uint Width, uint Heigth)[] levels = GetMipMapLevels().ToArray();

			// Raw
			if (!Pre_Format.IsCompressed())
			{
				int bytesPerPixel = Pre_Format.GetUncompressedBytesPerPixel();

				Surfaces = s.SerializeObjectArray<MipSurface>(
					array: Surfaces, 
					length: levels.Length, 
					onPreSerialize: (x, i) => x.Pre_ImageDataLength = levels[i].Width * levels[i].Heigth * bytesPerPixel, 
					name: nameof(Surfaces));
			}
			// Block-compressed
			else
			{
				uint blockSize = Pre_Format == TextureFormat.Format_DXT1 ? 8U : 16U;
				uint w = blockSize * (Pre_Width + 3 >> 2);

				Surfaces = s.SerializeObjectArray<MipSurface>(
					array: Surfaces,
					length: levels.Length,
					onPreSerialize: (x, i) =>
					{
						x.Pre_ImageDataLength = w * Math.Max(levels[i].Heigth >> 2, 1);

						w >>= 1;

						w = Math.Max(w, blockSize);
					},
					name: nameof(Surfaces));
			}
		}
	}
}