﻿namespace PsychoPortal
{
	public class LocalizedTextures : IBinarySerializable
	{
		public bool Pre_HasTextureHeader { get; set; }

		public GameLanguage Language { get; set; }
		public int DataLength { get; set; }
		public GameTexture[] Textures { get; set; }

		public ushort PrimaryTexturesCount { get; set; } // For the TexturePack

		public void Serialize(IBinarySerializer s)
		{
			Language = s.Serialize<GameLanguage>(Language, name: nameof(Language));
			DataLength = s.Serialize<int>(DataLength, name: nameof(DataLength));
			Textures = s.SerializeArraySize<GameTexture, ushort>(Textures, name: nameof(Textures));
			Textures = s.SerializeObjectArray<GameTexture>(Textures, onPreSerialize: (x, _) => x.Pre_HasTextureHeader = Pre_HasTextureHeader, name: nameof(Textures));
			PrimaryTexturesCount = s.Serialize<ushort>(PrimaryTexturesCount, name: nameof(PrimaryTexturesCount));
		}
	}
}