﻿namespace PsychoPortal
{
	public enum PLBType : ushort
	{
		Unknown = 0,
		Common = 1,
		LevelWorld = 2,
		LevelCharacter = 3,
		LevelMisc = 4,
	}
}