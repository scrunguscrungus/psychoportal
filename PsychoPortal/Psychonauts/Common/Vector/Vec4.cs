﻿namespace PsychoPortal
{
	/// <summary>
	/// A 4-dimensional vector. Usually used for colors where each value has a range of 0-1.
	/// </summary>
	public class Vec4 : IBinarySerializable, IBinaryShortLog
	{
		public Vec4() { }
		public Vec4(float x, float y, float z, float w)
		{
			X = x;
			Y = y;
			Z = z;
			W = w;
		}

		public float X { get; set; }
		public float Y { get; set; }
		public float Z { get; set; }
		public float W { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => $"Vec4({X}, {Y}, {Z}, {W})";

		public void Serialize(IBinarySerializer s)
		{
			X = s.Serialize<float>(X, name: nameof(X));
			Y = s.Serialize<float>(Y, name: nameof(Y));
			Z = s.Serialize<float>(Z, name: nameof(Z));
			W = s.Serialize<float>(W, name: nameof(W));
		}
	}
}