﻿namespace PsychoPortal
{
	// A uint on all versions besides PS2
	public enum AnimPlayMode : byte
	{
		Loop = 0,
		LoopOnce = 1,
		LoopTail = 2,
		Oscillate = 3,
		OscillateOnce = 4,
		OscillateOutOnce = 5,
		OscillateBackOnce = 6,
		Stop = 7,
	}
}