﻿namespace PsychoPortal
{
	public class ScriptPack : IBinarySerializable
	{
		public bool HasPackHeader { get; set; }
		public ushort HasDoFileNames { get; set; }
		public ushort ClassesCount { get; set; }
		public ScriptClass[] Classes { get; set; }
		public ScriptDoFile[] DoFiles { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			ushort controlValue = ClassesCount;

			if (HasPackHeader)
			{
				s.Serialize<ushort>(0xFCFC, name: "HeaderSignature");
			}
			else
			{
				controlValue = s.Serialize<ushort>(controlValue, name: "ControlValue");
				HasPackHeader = controlValue == 0xFCFC;
			}

			if (HasPackHeader)
			{
				HasDoFileNames = s.Serialize<ushort>(HasDoFileNames, name: nameof(HasDoFileNames));
				ClassesCount = s.Serialize<ushort>(ClassesCount, name: nameof(ClassesCount));
			}
			else
			{
				ClassesCount = controlValue;
			}

			Classes = s.SerializeObjectArray<ScriptClass>(Classes, ClassesCount, name: nameof(Classes));

			if (s.Settings.Version != PsychonautsVersion.Xbox_Proto_20041217)
			{
				DoFiles = s.SerializeArraySize<ScriptDoFile, ushort>(DoFiles, name: nameof(DoFiles));
				DoFiles = s.SerializeObjectArray<ScriptDoFile>(DoFiles, onPreSerialize: (x, _) => x.Pre_HasName = HasDoFileNames != 0, name: nameof(DoFiles));
			}
		}
	}
}