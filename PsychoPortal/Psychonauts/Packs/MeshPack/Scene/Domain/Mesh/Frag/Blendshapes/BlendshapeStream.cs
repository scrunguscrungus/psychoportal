﻿namespace PsychoPortal
{
	public class BlendshapeStream : IBinarySerializable
	{
		public uint Pre_Version { get; set; }
		public int Pre_MeshVerticesCount { get; set; }
		public byte Pre_Flags { get; set; }

		public String4b BlendMappingName { get; set; }
		public float Scale { get; set; }
		public VertexStreamSWBlend[] Vertices { get; set; }

		public byte[] PS2_Data { get; set; } // Vertices?

		public void Serialize(IBinarySerializer s)
		{
			BlendMappingName = s.SerializeObject<String4b>(BlendMappingName, name: nameof(BlendMappingName));

			if (s.Settings.Version == PsychonautsVersion.PS2)
			{
				PS2_Data = s.SerializeArraySize<byte, uint>(PS2_Data, name: nameof(PS2_Data));
				PS2_Data = s.SerializeArray<byte>(PS2_Data, name: nameof(PS2_Data));
			}
			else
			{
				if (Pre_Version < 319 || (Pre_Flags & 2) == 0)
					Vertices ??= new VertexStreamSWBlend[Pre_MeshVerticesCount];
				else
					Vertices = s.SerializeArraySize<VertexStreamSWBlend, uint>(Vertices, name: nameof(Vertices));

				Scale = s.Serialize<float>(Scale, name: nameof(Scale));
				Vertices = s.SerializeObjectArray<VertexStreamSWBlend>(Vertices, onPreSerialize: (x, i) =>
				{
					x.Pre_Version = Pre_Version;
					x.Pre_Index = (ushort)i;
					x.Pre_Flags = Pre_Flags;
				}, name: nameof(Vertices));
			}
		}
	}
}