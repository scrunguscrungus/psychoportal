﻿using System.IO;

namespace PsychoPortal
{
	public class PackageFileEntry : IBinarySerializable
	{
		public Package Pre_Package { get; set; }

		public byte Byte_00 { get; set; }
		public byte FileExtensionOffset { get; set; }
		public short SubDirIndex { get; set; }
		public uint FileNameOffset { get; set; }
		public uint FileDataOffset { get; set; }
		public uint FileSize { get; set; }

		// Parsed from offsets
		public string FileExtension { get; set; }
		public string FileName { get; set; }

		public string FullFileName => $"{FileName}.{FileExtension}";

		public Stream GetFileStream(Stream packageStream)
		{
			byte[] buffer = new byte[FileSize];
			packageStream.Position = FileDataOffset;
			int read = packageStream.Read(buffer, 0, (int)FileSize);

			if (read != FileSize)
				throw new EndOfStreamException();

			return new MemoryStream(buffer);
		}

		public void Serialize(IBinarySerializer s)
		{
			Byte_00 = s.Serialize<byte>(Byte_00, name: nameof(Byte_00));
			FileExtensionOffset = s.Serialize<byte>(FileExtensionOffset, name: nameof(FileExtensionOffset));
			SubDirIndex = s.Serialize<short>(SubDirIndex, name: nameof(SubDirIndex));
			FileNameOffset = s.Serialize<uint>(FileNameOffset, name: nameof(FileNameOffset));
			FileDataOffset = s.Serialize<uint>(FileDataOffset, name: nameof(FileDataOffset));
			FileSize = s.Serialize<uint>(FileSize, name: nameof(FileSize));

			// TODO: Cache reading these?
			s.DoAt(Pre_Package.FileExtensionsBufferOffset + FileExtensionOffset, () =>
				FileExtension = s.Serialize<string>(FileExtension, name: nameof(FileExtension)));
			s.DoAt(Pre_Package.FileNamesBufferOffset + FileNameOffset, () =>
				FileName = s.Serialize<string>(FileName, name: nameof(FileName)));
		}
	}
}