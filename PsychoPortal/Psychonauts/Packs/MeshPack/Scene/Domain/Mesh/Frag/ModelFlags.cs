﻿using System;

namespace PsychoPortal
{
	[Flags]
	public enum ModelFlags
	{
		None = 0,
		HasBlendAnimations = 1 << 0,
		Flag_1 = 1 << 1,
		Flag_2 = 1 << 2,
		HasAnimations = 1 << 3,
		Flag_4 = 1 << 4,
		DistantGeometry = 1 << 5,
		HasSnowTexture = 1 << 6,
		Flag_7 = 1 << 7,
	}
}