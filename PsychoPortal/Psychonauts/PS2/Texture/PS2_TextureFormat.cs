﻿namespace PsychoPortal
{
	public enum PS2_TextureFormat : byte
	{
		PSMCT32 = 0x0,
		PSMCT24 = 0x1,
		PSMCT16S = 0x2,
		PSMT8_32A = 0x3, // 32-bit colors with alpha
		PSMT8_32 = 0x4, // 32-bit colors without alpha
		PSMT8_16 = 0x5,
		PSMT4_32A = 0x6,
		PSMT4_32 = 0x7,
		PSMT4_16 = 0x8,
		PSMZ24 = 0x9,
		PSMT8H = 0xA,
	}
}