﻿namespace PsychoPortal
{
	public class JointID : IBinarySerializable, IBinaryShortLog
	{
		public int JointIndex { get; set; }
		public int SkeletonIndex { get; set; }

		public string ShortLog => ToString();
		public override string ToString() => $"JointID(Skeleton: {SkeletonIndex}, Joint: {JointIndex})";

		public void Serialize(IBinarySerializer s)
		{
			s.DoBits<ushort>(b =>
			{
				JointIndex = b.SerializeBits<int>(JointIndex, 12, name: nameof(JointIndex));
				SkeletonIndex = b.SerializeBits<int>(SkeletonIndex, 4, name: nameof(SkeletonIndex));
			});
		}
	}
}