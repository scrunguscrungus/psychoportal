﻿namespace PsychoPortal
{
	public class Plane : IBinarySerializable
	{
		public Vec3 Normal { get; set; }
		public float Distance { get; set; }

		public void Serialize(IBinarySerializer s)
		{
			Normal = s.SerializeObject<Vec3>(Normal, name: nameof(Normal));
			Distance = s.Serialize<float>(Distance, name: nameof(Distance));
		}
	}
}