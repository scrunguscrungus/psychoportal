﻿using System;

namespace PsychoPortal
{
	/// <summary>
	/// A 4x4 matrix
	/// </summary>
	public class Mat4 : IBinarySerializable
	{
		public Mat4() { }

		public Mat4(
			float m00, float m10, float m20, float m30, 
			float m01, float m11, float m21, float m31, 
			float m02, float m12, float m22, float m32, 
			float m03, float m13, float m23, float m33)
		{
			M00 = m00;
			M10 = m10;
			M20 = m20;
			M30 = m30;
			M01 = m01;
			M11 = m11;
			M21 = m21;
			M31 = m31;
			M02 = m02;
			M12 = m12;
			M22 = m22;
			M32 = m32;
			M03 = m03;
			M13 = m13;
			M23 = m23;
			M33 = m33;
		}

		public float M00 { get; set; }
		public float M10 { get; set; }
		public float M20 { get; set; }
		public float M30 { get; set; }
		public float M01 { get; set; }
		public float M11 { get; set; }
		public float M21 { get; set; }
		public float M31 { get; set; }
		public float M02 { get; set; }
		public float M12 { get; set; } 
		public float M22 { get; set; }
		public float M32 { get; set; }
		public float M03 { get; set; }
		public float M13 { get; set; }
		public float M23 { get; set; }
		public float M33 { get; set; }

		public static Mat4 RotateZ(float v)
		{
			if (v == 0)
				return new Mat4();

			float fVar1 = (float)Math.Sin(v * MathHelpers.Deg2Rad);
			float fVar2 = (float)Math.Cos(v * MathHelpers.Deg2Rad);
			return new Mat4(fVar2, fVar1, 0.0f, 0.0f, -fVar1, fVar2, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
		}

		public static Mat4 RotateY(float v)
		{
			if (v == 0)
				return new Mat4();

			float fVar1 = (float)Math.Sin(v * MathHelpers.Deg2Rad);
			float fVar2 = (float)Math.Cos(v * MathHelpers.Deg2Rad);

			return new Mat4(fVar2, 0.0f, -fVar1, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, fVar1, 0.0f, fVar2, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
		}

		public static Mat4 Mul(Mat4 mat1, Mat4 mat2) => new Mat4(
			m00: mat1.M03 * mat2.M30 + mat1.M02 * mat2.M20 + mat1.M01 * mat2.M10 + mat1.M00 * mat2.M00,
			m10: mat1.M13 * mat2.M30 + mat1.M12 * mat2.M20 + mat1.M11 * mat2.M10 + mat1.M10 * mat2.M00, 
			m20: mat1.M23 * mat2.M30 + mat1.M22 * mat2.M20 + mat1.M21 * mat2.M10 + mat1.M20 * mat2.M00, 
			m30: mat1.M33 * mat2.M30 + mat1.M32 * mat2.M20 + mat1.M31 * mat2.M10 + mat1.M30 * mat2.M00,
			m01: mat1.M03 * mat2.M31 + mat1.M02 * mat2.M21 + mat1.M01 * mat2.M11 + mat1.M00 * mat2.M01,
			m11: mat1.M13 * mat2.M31 + mat1.M12 * mat2.M21 + mat1.M11 * mat2.M11 + mat1.M10 * mat2.M01, 
			m21: mat1.M23 * mat2.M31 + mat1.M22 * mat2.M21 + mat1.M21 * mat2.M11 + mat1.M20 * mat2.M01, 
			m31: mat1.M33 * mat2.M31 + mat1.M32 * mat2.M21 + mat1.M31 * mat2.M11 + mat1.M30 * mat2.M01,
			m02: mat1.M03 * mat2.M32 + mat1.M02 * mat2.M22 + mat1.M01 * mat2.M12 + mat1.M00 * mat2.M02,
			m12: mat1.M13 * mat2.M32 + mat1.M12 * mat2.M22 + mat1.M11 * mat2.M12 + mat1.M10 * mat2.M02, 
			m22: mat1.M23 * mat2.M32 + mat1.M22 * mat2.M22 + mat1.M21 * mat2.M12 + mat1.M20 * mat2.M02, 
			m32: mat1.M33 * mat2.M32 + mat1.M32 * mat2.M22 + mat1.M31 * mat2.M12 + mat1.M30 * mat2.M02,
			m03: mat1.M03 * mat2.M33 + mat1.M02 * mat2.M23 + mat1.M01 * mat2.M13 + mat1.M00 * mat2.M03, 
			m13: mat1.M13 * mat2.M33 + mat1.M12 * mat2.M23 + mat1.M11 * mat2.M13 + mat1.M10 * mat2.M03, 
			m23: mat1.M23 * mat2.M33 + mat1.M22 * mat2.M23 + mat1.M21 * mat2.M13 + mat1.M20 * mat2.M03,
			m33: mat1.M33 * mat2.M33 + mat1.M32 * mat2.M23 + mat1.M31 * mat2.M13 + mat1.M30 * mat2.M03);

		public void Serialize(IBinarySerializer s)
		{
			M00 = s.Serialize<float>(M00, name: nameof(M00));
			M10 = s.Serialize<float>(M10, name: nameof(M10));
			M20 = s.Serialize<float>(M20, name: nameof(M20));
			M30 = s.Serialize<float>(M30, name: nameof(M30));
			M01 = s.Serialize<float>(M01, name: nameof(M01));
			M11 = s.Serialize<float>(M11, name: nameof(M11));
			M21 = s.Serialize<float>(M21, name: nameof(M21));
			M31 = s.Serialize<float>(M31, name: nameof(M31));
			M02 = s.Serialize<float>(M02, name: nameof(M02));
			M12 = s.Serialize<float>(M12, name: nameof(M12));
			M22 = s.Serialize<float>(M22, name: nameof(M22));
			M32 = s.Serialize<float>(M32, name: nameof(M32));
			M03 = s.Serialize<float>(M03, name: nameof(M03));
			M13 = s.Serialize<float>(M13, name: nameof(M13));
			M23 = s.Serialize<float>(M23, name: nameof(M23));
			M33 = s.Serialize<float>(M33, name: nameof(M33));
		}
	}
}