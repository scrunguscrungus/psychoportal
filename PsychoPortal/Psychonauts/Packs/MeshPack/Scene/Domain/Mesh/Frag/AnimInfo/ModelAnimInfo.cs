﻿namespace PsychoPortal
{
	public class ModelAnimInfo : IBinarySerializable
	{
		public uint Pre_Version { get; set; }
		public int Pre_VerticesCount { get; set; }

		public ushort UnknownUshort { get; set; } // Always 0? Unused by game?
		public ushort BonesCount { get; set; }
		public ushort InfluencesPerVertex { get; set; } // 1-2 for final game, 1-4 for older versions
		public JointID[] JointIDs { get; set; }
		public BoneMaxWeight[] BoneMaxWeights { get; set; }
		public Box3[] BoneBounds { get; set; }
		public SkinWeight[] OriginalSkinWeights { get; set; } // SkinningOrigSkinWeights

		public void Serialize(IBinarySerializer s)
		{
			UnknownUshort = s.Serialize<ushort>(UnknownUshort, name: nameof(UnknownUshort));

			if (Pre_Version < 301)
				BonesCount = 24;
			else
				BonesCount = s.Serialize<ushort>(BonesCount, name: nameof(BonesCount));

			InfluencesPerVertex = s.Serialize<ushort>(InfluencesPerVertex, name: nameof(InfluencesPerVertex));
			JointIDs = s.SerializeObjectArray<JointID>(JointIDs, BonesCount, name: nameof(JointIDs));
			BoneMaxWeights = s.SerializeObjectArray<BoneMaxWeight>(BoneMaxWeights, BonesCount, name: nameof(BoneMaxWeights));
			BoneBounds = s.SerializeObjectArray<Box3>(BoneBounds, BonesCount, name: nameof(BoneBounds));
			
			if (s.Settings.Version != PsychonautsVersion.PS2)
				OriginalSkinWeights = s.SerializeObjectArray<SkinWeight>(OriginalSkinWeights, Pre_VerticesCount,
					(x, _) => x.Pre_Version = Pre_Version, name: nameof(OriginalSkinWeights));
		}
	}
}