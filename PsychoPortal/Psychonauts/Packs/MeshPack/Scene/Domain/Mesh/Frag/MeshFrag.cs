﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PsychoPortal
{
	public class MeshFrag : IBinarySerializable
	{
		public uint Pre_Version { get; set; }

		public Box3 Bounds { get; set; }
		public ModelFlags ModelFlags { get; set; } // Most are set during runtime
		public MaterialFlags MaterialFlags { get; set; }
		public uint Uint_20 { get; set; }
		public byte DistantLOD { get; set; }
		public byte BlendFlags { get; set; } // TODO: Create enum, first flag determines if it's used
		public uint Proto_BlendShapeValue { get; set; }
		public Vec4 MaterialColor { get; set; }
		public int[] TextureIndices { get; set; } // Can't be more than 3
		public int[] LightMapTextureIndices { get; set; }
		public String4b SwitchLightName { get; set; }

		// Creates blinn info for these
		public int BumpmapTextureIndex { get; set; } = -1;
		public int GlossmapTextureIndex { get; set; } = -1;
		public float SpecularFloat { get; set; } // If flags 5, 6 & 23 are set it is snapped to a specific power of 2 value
		public Vec3 SpecularVector { get; set; }
		public int ReflectionMapTextureIndex { get; set; } = -1; // Has to be a cubemap
		public Vec3 Vector_68 { get; set; } // Is discarded if Flag_18 is not set

		public int DetailTextureIndex { get; set; } = -1;
		public Vec2 DetailFactor { get; set; }
		public float GlareIntensity { get; set; }
		public Vec2 TexCoordTransVel { get; set; }
		public uint Uint_84 { get; set; } // Game discards this?
		public VertexNotexNorm[] Vertices { get; set; }
		public uint HasVertexColors { get; set; }
		public BGRA8888Color[] VertexColors { get; set; }
		public uint UVChannelsCount { get; set; } // 1-4
		public float UVScale { get; set; }
		public UVSet[] UVSets { get; set; }
		public uint HasVertexStreamBasis { get; set; }
		public VertexStreamBasis[] VertexStreamBasis { get; set; }
		public uint PolygonCount { get; set; }
		public uint DegenPolygonCount { get; set; }
		public short[] PolygonIndexBuffer { get; set; }
		public ModelAnimInfo AnimInfo { get; set; }
		public Octree Proto_Octree { get; set; }
		public BlendshapeData BlendshapeData { get; set; }

		// PS2
		public uint PS2_VerticesCount { get; set; } // Leftover value
		public byte[] PS2_GIFTag { get; set; } // Appears to be irrelevant even though the game loads it into memory
		public float PS2_VertexOffset { get; set; } // Subtract vertices by this to get actual value
		public byte[] PS2_VIFCommands { get; set; } // Buffer of VIF code commands for loading GIF data into memory

		public IEnumerable<BGRA8888Color> GetVertexColors()
		{
			// Newer versions only store the colors when they are used
			if (Pre_Version >= 317)
				return VertexColors;

			// By default the colors are stored as black if not used. These flags determine if they should be used.
			if (!MaterialFlags.HasFlag(MaterialFlags.Flag_6) && !MaterialFlags.HasFlag(MaterialFlags.Flag_15))
				return null;

			return UVSets?.Select(x => x.Proto_VertexColor);
		}

		public void Serialize(IBinarySerializer s)
		{
			Bounds = s.SerializeObject<Box3>(Bounds, name: nameof(Bounds));

			if (Pre_Version > 317)
				ModelFlags = s.Serialize<ModelFlags>(ModelFlags, name: nameof(ModelFlags));

			MaterialFlags = s.Serialize<MaterialFlags>(MaterialFlags, name: nameof(MaterialFlags));
			Uint_20 = s.Serialize<uint>(Uint_20, name: nameof(Uint_20));

			DistantLOD = s.Serialize<byte>(DistantLOD, name: nameof(DistantLOD));

			if (Pre_Version >= 308)
				BlendFlags = s.Serialize<byte>(BlendFlags, name: nameof(BlendFlags));
			else
				Proto_BlendShapeValue = s.Serialize<uint>(Proto_BlendShapeValue, name: nameof(Proto_BlendShapeValue));

			MaterialColor = s.SerializeObject<Vec4>(MaterialColor, name: nameof(MaterialColor));
			TextureIndices = s.SerializeArraySize<int, uint>(TextureIndices, name: nameof(TextureIndices));
			TextureIndices = s.SerializeArray<int>(TextureIndices, name: nameof(TextureIndices));

			if (MaterialFlags.HasFlag(MaterialFlags.Lightmap))
			{
				LightMapTextureIndices = s.SerializeArray<int>(LightMapTextureIndices, 2, name: nameof(LightMapTextureIndices));

				if (LightMapTextureIndices[1] != -1)
					SwitchLightName = s.SerializeObject<String4b>(SwitchLightName, name: nameof(SwitchLightName));
			}

			if (MaterialFlags.HasFlag(MaterialFlags.Bumpmap))
				BumpmapTextureIndex = s.Serialize<int>(BumpmapTextureIndex, name: nameof(BumpmapTextureIndex));

			GlossmapTextureIndex = s.Serialize<int>(GlossmapTextureIndex, name: nameof(GlossmapTextureIndex));

			if (MaterialFlags.HasFlag(MaterialFlags.Specular))
			{
				SpecularFloat = s.Serialize<float>(SpecularFloat, name: nameof(SpecularFloat));
				SpecularVector = s.SerializeObject<Vec3>(SpecularVector, name: nameof(SpecularVector));
			}

			ReflectionMapTextureIndex = s.Serialize<int>(ReflectionMapTextureIndex, name: nameof(ReflectionMapTextureIndex));

			Vector_68 = s.SerializeObject<Vec3>(Vector_68, name: nameof(Vector_68));

			if (MaterialFlags.HasFlag(MaterialFlags.DetailTexture))
			{
				DetailTextureIndex = s.Serialize<int>(DetailTextureIndex, name: nameof(DetailTextureIndex));
				DetailFactor = s.SerializeObject<Vec2>(DetailFactor, name: nameof(DetailFactor));
			}

			GlareIntensity = s.Serialize<float>(GlareIntensity, name: nameof(GlareIntensity));
			TexCoordTransVel = s.SerializeObject<Vec2>(TexCoordTransVel, name: nameof(TexCoordTransVel));
			Uint_84 = s.Serialize<uint>(Uint_84, name: nameof(Uint_84));

			if (s.Settings.Version == PsychonautsVersion.PS2)
			{
				// Mostly leftover data from other platforms, but the uv count is used to
				// determine how much data should be read after running the VIF commands
				PS2_VerticesCount = s.Serialize<uint>(PS2_VerticesCount, name: nameof(PS2_VerticesCount));
				UVChannelsCount = s.Serialize<uint>(UVChannelsCount, name: nameof(UVChannelsCount));
				HasVertexStreamBasis = s.Serialize<uint>(HasVertexStreamBasis, name: nameof(HasVertexStreamBasis));
				PolygonCount = s.Serialize<uint>(PolygonCount, name: nameof(PolygonCount));
				DegenPolygonCount = s.Serialize<uint>(DegenPolygonCount, name: nameof(DegenPolygonCount));
			}
			else
			{
				Vertices = s.SerializeArraySize<VertexNotexNorm, uint>(Vertices, name: nameof(Vertices));
				Vertices = s.SerializeObjectArray<VertexNotexNorm>(Vertices, name: nameof(Vertices));

				if (Pre_Version >= 317)
				{
					HasVertexColors = s.Serialize<uint>(HasVertexColors, name: nameof(HasVertexColors));

					if (HasVertexColors != 0)
						VertexColors = s.SerializeObjectArray<BGRA8888Color>(VertexColors, Vertices.Length, name: nameof(VertexColors));
				}

				UVChannelsCount = s.Serialize<uint>(UVChannelsCount, name: nameof(UVChannelsCount));

				if (UVChannelsCount != 0)
				{
					if (Pre_Version >= 317)
						UVScale = s.Serialize<float>(UVScale, name: nameof(UVScale));
					else
						UVScale = 1;

					UVSets = s.SerializeObjectArray<UVSet>(UVSets, Vertices.Length, onPreSerialize: (x, _) =>
					{
						x.Pre_UVsCount = UVChannelsCount;
						x.Pre_Version = Pre_Version;
					}, name: nameof(UVSets));
				}

				HasVertexStreamBasis = s.Serialize<uint>(HasVertexStreamBasis, name: nameof(HasVertexStreamBasis));

				if (HasVertexStreamBasis != 0)
					VertexStreamBasis = s.SerializeObjectArray<VertexStreamBasis>(VertexStreamBasis, Vertices.Length, name: nameof(VertexStreamBasis));

				PolygonCount = s.Serialize<uint>(PolygonCount, name: nameof(PolygonCount));
				DegenPolygonCount = s.Serialize<uint>(DegenPolygonCount, name: nameof(DegenPolygonCount));

				uint indexBufferCount = MaterialFlags.HasFlag(MaterialFlags.Tristrip) ? PolygonCount + 2 : PolygonCount * 3;
				PolygonIndexBuffer = s.SerializeArray<short>(PolygonIndexBuffer, indexBufferCount, name: nameof(PolygonIndexBuffer));
			}

			if (MaterialFlags.HasFlag(MaterialFlags.Skinned))
				AnimInfo = s.SerializeObject<ModelAnimInfo>(AnimInfo, x =>
				{
					x.Pre_Version = Pre_Version;
					x.Pre_VerticesCount = Vertices?.Length ?? 0;
				}, name: nameof(AnimInfo));

			if (Pre_Version < 317 && MaterialFlags.HasFlag(MaterialFlags.Flag_25))
				Proto_Octree = s.SerializeObject<Octree>(Proto_Octree, x =>
				{
					x.Pre_Version = Pre_Version;
					x.Pre_OldHasPrimitives = true;
				}, name: nameof(Proto_Octree));

			if (s.Settings.Version == PsychonautsVersion.PS2)
			{
				PS2_GIFTag = s.SerializeArray<byte>(PS2_GIFTag, 0x10, name: nameof(PS2_GIFTag));
				PS2_VertexOffset = s.Serialize<float>(PS2_VertexOffset, name: nameof(PS2_VertexOffset));
				PS2_VIFCommands = s.SerializeArraySize<byte, uint>(PS2_VIFCommands, name: nameof(PS2_VIFCommands));
				PS2_VIFCommands = s.SerializeArray<byte>(PS2_VIFCommands, name: nameof(PS2_VIFCommands));
			}

			if (ModelFlags.HasFlag(ModelFlags.HasBlendAnimations) || (BlendFlags & 1) != 0)
			{
				if (Pre_Version >= 311)
				{
					BlendshapeData = s.SerializeObject<BlendshapeData>(BlendshapeData, x =>
					{
						x.Pre_Version = Pre_Version;
						x.Pre_MeshVerticesCount = Vertices?.Length ?? 0;
						x.Pre_Flags = BlendFlags;
					}, name: nameof(BlendshapeData));
				}
				else
				{
					throw new NotImplementedException("Not implemented old blend shapes data format");
				}
			}
		}
	}
}