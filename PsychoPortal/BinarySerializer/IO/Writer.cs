﻿using System;
using System.IO;
using System.Text;

namespace PsychoPortal
{
	// TODO: Use value buffer like in the Reader to avoid allocating a new byte array on each write call

	public class Writer : BinaryWriter
	{
		#region Constructors

		public Writer(Stream stream, Endian endian = Endian.Little, bool leaveOpen = false) : base(stream, new UTF8Encoding(), leaveOpen)
		{
			Endian = endian;
		}

		#endregion

		#region Public Properties

		public Endian Endian { get; set; }

		#endregion

		#region Write Methods

		public override void Write(int value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public override void Write(short value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public override void Write(uint value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public override void Write(ushort value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public override void Write(long value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public override void Write(ulong value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public void Write(UInt24 value)
		{
			uint v = value;

			if (Endian == Endian.Little)
			{
				Write((byte)(v & 0xFF));
				Write((byte)((v >> 8) & 0xFF));
				Write((byte)((v >> 16) & 0xFF));
			}
			else
			{
				Write((byte)((v >> 16) & 0xFF));
				Write((byte)((v >> 8) & 0xFF));
				Write((byte)(v & 0xFF));
			}
		}

		public override void Write(float value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public override void Write(double value)
		{
			var data = BitConverter.GetBytes(value);
			if (Endian == Endian.Little != BitConverter.IsLittleEndian) 
				Array.Reverse(data);
			Write(data);
		}

		public void WriteNullDelimitedString(string value, Encoding encoding)
		{
			if (encoding == null)
				throw new ArgumentNullException(nameof(encoding));

			value ??= "";
			byte[] data = encoding.GetBytes(value + '\0');
			Write(data);
		}

		public void WriteString(string value, long size, Encoding encoding)
		{
			if (encoding == null)
				throw new ArgumentNullException(nameof(encoding));

			value ??= "";
			byte[] data = encoding.GetBytes(value + '\0');
			if (data.Length != size)
				Array.Resize(ref data, (int)size);
			Write(data);
		}

		public override void Write(byte[] buffer)
		{
			if (buffer == null)
				return;

			base.Write(buffer);
		}

		public override void Write(sbyte value) => Write((byte)value);

		#endregion

		#region Alignment

		public void Align(int alignBytes, int offset = 0)
		{
			if ((BaseStream.Position - offset) % alignBytes != 0)
			{
				int length = alignBytes - (int)((BaseStream.Position - offset) % alignBytes);
				byte[] data = new byte[length];
				Write(data);
			}
		}

		#endregion
	}
}