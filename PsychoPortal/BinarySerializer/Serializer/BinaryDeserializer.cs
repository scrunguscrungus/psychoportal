﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PsychoPortal
{
	/// <summary>
	/// Default binary deserializer, for reading from a stream
	/// </summary>
	public class BinaryDeserializer : IBinarySerializer
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="stream">The stream to deserialize from</param>
		/// <param name="settings">The settings to use when serializing</param>
		/// <param name="endian">The endianness to use</param>
		/// <param name="logger">An optional logger to use for logging</param>
		public BinaryDeserializer(Stream stream, PsychonautsSettings settings, Endian endian = Endian.Little, IBinarySerializerLogger logger = null)
		{
			Stream = stream;
			Settings = settings;
			Logger = logger;
			Reader = new Reader(stream, endian);

			DefaultEncoding = Encoding.UTF8;
		}

		private const string DefaultName = "<no name>";

		/// <summary>
		/// Reads a value of the specified type from the stream
		/// </summary>
		/// <typeparam name="T">The type of value to read</typeparam>
		/// <returns>The read value</returns>
		protected object Read<T>()
		{
			// Get the type
			var type = typeof(T);

			TypeCode typeCode = Type.GetTypeCode(type);

			switch (typeCode)
			{
				case TypeCode.Boolean:
					byte b = Reader.ReadByte();

					if (b != 0 && b != 1 && IsLogEnabled)
						Logger.WriteLogLine($"{LogPrefix}({typeof(T)}): Binary boolean was not correctly formatted ({b})");

					return b != 0;

				case TypeCode.SByte:
					return Reader.ReadSByte();

				case TypeCode.Byte:
					return Reader.ReadByte();

				case TypeCode.Int16:
					return Reader.ReadInt16();

				case TypeCode.UInt16:
					return Reader.ReadUInt16();

				case TypeCode.Int32:
					return Reader.ReadInt32();

				case TypeCode.UInt32:
					return Reader.ReadUInt32();

				case TypeCode.Int64:
					return Reader.ReadInt64();

				case TypeCode.UInt64:
					return Reader.ReadUInt64();

				case TypeCode.Single:
					return Reader.ReadSingle();

				case TypeCode.Double:
					return Reader.ReadDouble();

				case TypeCode.String:
					return Reader.ReadNullDelimitedString(DefaultEncoding);

				case TypeCode.Decimal:
				case TypeCode.Char:
				case TypeCode.DateTime:
				case TypeCode.Empty:
				case TypeCode.DBNull:
				case TypeCode.Object:
				default:
					if (type == typeof(UInt24))
					{
						return Reader.ReadUInt24();
					}
					else if ( type == typeof(OctreeLeaf) )
					{
						//This can probably be turned into an object
						return Reader.ReadOctreeLeaf();
					}
					else if (type == typeof(byte?))
					{
						byte nullableByte = Reader.ReadByte();

						if (nullableByte == 0xFF) 
							return (byte?)null;

						return nullableByte;
					}
					else
					{
						throw new NotSupportedException("The specified generic type can not be read from the reader");
					}
			}
		}

		/// <summary>
		/// Serializes a value
		/// </summary>
		/// <typeparam name="T">The value type</typeparam>
		/// <param name="value">The value</param>
		/// <param name="name">The value name, for logging</param>
		/// <returns>The serialized value</returns>
		public T Serialize<T>(T value, string name = null)
		{
			string logPrefix = LogPrefix;

			// Read and cast the value
			T v = (T)Read<T>();

			if (IsLogEnabled)
				Logger.WriteLogLine($"{logPrefix}({typeof(T)}) {name ?? DefaultName}: {v}");

			return v;
		}

		/// <summary>
		/// Serializes the size of an array
		/// </summary>
		/// <typeparam name="T">The array value type</typeparam>
		/// <typeparam name="U">The size value type</typeparam>
		/// <param name="array">The array</param>
		/// <param name="name">The array name, for logging</param>
		/// <returns>The serialized array</returns>
		public T[] SerializeArraySize<T, U>(T[] array, string name = null)
		{
			U size = Serialize<U>(default, name: $"{name}.Length");

			// Convert size to int
			int intSize = (int)Convert.ChangeType(size, typeof(int));

			if (array == null)
				array = new T[intSize];
			else if (array.Length != intSize)
				Array.Resize(ref array, intSize);

			return array;
		}

		/// <summary>
		/// Serializes an array
		/// </summary>
		/// <typeparam name="T">The array value type</typeparam>
		/// <param name="array">The array</param>
		/// <param name="length">The array length, or -1 to use its current length</param>
		/// <param name="name">The array name, for logging</param>
		/// <returns>The serialized array</returns>
		public T[] SerializeArray<T>(T[] array, long length = -1, string name = null)
		{
			if (length == -1)
				length = array?.Length ?? 0;

			// Use byte reading method if it's a byte array
			if (typeof(T) == typeof(byte))
			{
				string logPrefix = LogPrefix;

				byte[] bytes = Reader.ReadBytes((int)length);

				if (IsLogEnabled)
				{
					string normalLog = $"{logPrefix}({typeof(T)}[{length}]) {name ?? DefaultName}: ";
					Logger.WriteLogLine($"{normalLog}{bytes.ToHexString(16, new string(' ', normalLog.Length), maxLines: 10)}");
				}

				return (T[])(object)bytes;
			}

			if (IsLogEnabled)
				Logger.WriteLogLine($"{LogPrefix}({typeof(T)}[{length}]) {name ?? DefaultName}");

			// Create the buffer to read to
			T[] buffer;

			if (array != null)
			{
				buffer = array;

				if (buffer.Length != length)
					Array.Resize(ref buffer, (int)length);
			}
			else
			{
				buffer = new T[length];
			}

			// Read each value to the buffer
			for (int i = 0; i < length; i++)
				// Read the value
				buffer[i] = Serialize<T>(default, name: name == null || !IsLogEnabled ? null : $"{name}[{i}]");

			// Return the buffer
			return buffer;
		}

		/// <summary>
		/// Serializes a string of a specified length
		/// </summary>
		/// <param name="value">The string value</param>
		/// <param name="length">The string length</param>
		/// <param name="encoding">The string encoding to use, or null for the default one</param>
		/// <param name="name">The string value name, for logging</param>
		/// <returns>The serialized string value</returns>
		public string SerializeString(string value, long length, Encoding encoding = null, string name = null)
		{
			string logPrefix = LogPrefix;

			// Read the string from the reader
			string v = Reader.ReadString(length, encoding ?? DefaultEncoding);

			if (IsLogEnabled)
				Logger.WriteLogLine($"{logPrefix}(string) {name ?? DefaultName}: {v}");

			return v;
		}

		/// <summary>
		/// Serializes a serializable object
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="value">The serializable object</param>
		/// <param name="onPreSerialize">Optional action to run before serializing</param>
		/// <param name="name">The object value name, for logging</param>
		/// <returns>The serialized object</returns>
		public T SerializeObject<T>(T value, Action<T> onPreSerialize = null, string name = null) 
			where T : IBinarySerializable, new()
		{
			// Create a new instance of the object
			T instance = new T();

			string logPrefix = LogPrefix;

			bool isLogTemporarilyDisabled = false;
			if (!DisableLogForObject && instance is IBinaryShortLog)
			{
				DisableLogForObject = true;
				isLogTemporarilyDisabled = true;
			}

			if (IsLogEnabled)
				Logger.WriteLogLine($"{logPrefix}(Object: {typeof(T)}) {name ?? DefaultName}");

			Depth++;

			// Call pre-serializing action
			onPreSerialize?.Invoke(instance);
			
			// Serialize the object
			instance.Serialize(this);

			Depth--;

			if (isLogTemporarilyDisabled && instance is IBinaryShortLog log)
			{
				DisableLogForObject = false;

				if (IsLogEnabled)
					Logger.WriteLogLine($"{logPrefix}({typeof(T)}) {name ?? DefaultName}: {log.ShortLog ?? "null"}");
			}

			// Return the object
			return instance;
		}

		/// <summary>
		/// Serializes an array of serializable objects
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="array">The serializable object array</param>
		/// <param name="length">The array length, or -1 to use its current length</param>
		/// <param name="onPreSerialize">Optional action to run before serializing each object</param>
		/// <param name="name">The object value name, for logging</param>
		/// <returns>The serialized object array</returns>
		public T[] SerializeObjectArray<T>(T[] array, long length = -1, Action<T, int> onPreSerialize = null, string name = null)
			where T : IBinarySerializable, new()
		{
			if (length == -1)
				length = array?.Length ?? 0;

			if (IsLogEnabled)
				Logger.WriteLogLine($"{LogPrefix}(Object[]: {typeof(T)}[{length}]) {name ?? DefaultName}");

			// Create the buffer
			T[] buffer;

			if (array != null)
			{
				buffer = array;

				if (buffer.Length != length)
					Array.Resize(ref buffer, (int)length);
			}
			else
			{
				buffer = new T[length];
			}

			// Read each object to the buffer
			for (int i = 0; i < length; i++)
				// Read the object
				buffer[i] = SerializeObject<T>(default, onPreSerialize: onPreSerialize != null ? x => onPreSerialize(x, i) : null, name: name == null || !IsLogEnabled ? null : $"{name}[{i}]");

			return buffer;
		}

		/// <summary>
		/// Serializes an object array of undefined size until a specified condition is met
		/// </summary>
		/// <typeparam name="T">The object type</typeparam>
		/// <param name="obj">The object array</param>
		/// <param name="conditionCheckFunc">The condition for ending the array serialization</param>
		/// <param name="getLastObjFunc">If specified the last object when read will be ignored and this will be used to prepend an object when writing</param>
		/// <param name="onPreSerialize">Optional action to run before serializing</param>
		/// <param name="name">The name</param>
		/// <returns>The object array</returns>
		public T[] SerializeObjectArrayUntil<T>(T[] obj, Func<T, int, bool> conditionCheckFunc, Func<T> getLastObjFunc = null,
			Action<T, int> onPreSerialize = null, string name = null)
			where T : IBinarySerializable, new()
		{
			if (IsLogEnabled)
				Logger.WriteLogLine($"{LogPrefix}(Object[]: {typeof(T)}[..]) {name ?? DefaultName}");

			var objects = new List<T>();
			int index = 0;

			while (true)
			{
				T serializedObj = SerializeObject<T>(
					value: default,
					onPreSerialize: onPreSerialize == null ? null : x => onPreSerialize(x, index),
					name: $"{name}[{index}]");

				if (conditionCheckFunc(serializedObj, index))
				{
					if (getLastObjFunc == null)
						objects.Add(serializedObj);

					break;
				}

				index++;

				objects.Add(serializedObj);
			}

			return objects.ToArray();
		}

		/// <summary>
		/// Serializes bit field values
		/// </summary>
		/// <typeparam name="T">The integer value type</typeparam>
		/// <param name="serializeAction">The serializer action</param>
		public void DoBits<T>(Action<IBitSerializer> serializeAction)
		{
			string logPrefix = LogPrefix;
			long value = Convert.ToInt64(Serialize<T>(default, name: "Value"));
			serializeAction(new BitDeserializer(this, logPrefix, value));
		}

		/// <summary>
		/// Seeks to the specified stream position
		/// </summary>
		/// <param name="position">The position to go to</param>
		public void GoTo(long position)
		{
			Stream.Position = position;
		}

		private bool DisableLogForObject { get; set; }

		public bool IsLogEnabled => !DisableLogForObject && Logger is { IsActive: true };

		/// <summary>
		/// The current stream position
		/// </summary>
		public long Position => Stream.Position;

		/// <summary>
		/// The current stream length
		/// </summary>
		public long Length => Stream.Length;

		/// <summary>
		/// The settings to use when serializing
		/// </summary>
		public PsychonautsSettings Settings { get; }

		/// <summary>
		/// The stream to deserialize from
		/// </summary>
		public Stream Stream { get; }

		/// <summary>
		/// An optional logger to use for logging
		/// </summary>
		public IBinarySerializerLogger Logger { get; }

		/// <summary>
		/// The binary reader
		/// </summary>
		protected Reader Reader { get; }

		/// <summary>
		/// The default string encoding to use
		/// </summary>
		public Encoding DefaultEncoding { get; set; }

		/// <summary>
		/// The log prefix to use when writing a log
		/// </summary>
		protected string LogPrefix => !IsLogEnabled ? null : $"(R) 0x{Stream.Position:X8}:{new string(' ', (Depth + 1) * 2)}";

		/// <summary>
		/// The depths, for logging
		/// </summary>
		protected int Depth { get; set; }
	}
}