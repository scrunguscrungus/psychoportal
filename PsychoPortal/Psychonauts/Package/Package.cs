﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PsychoPortal
{
	public class Package : IBinarySerializable
	{
		public int Version { get; set; } // Always 1
		public uint HeaderLength { get; set; }
		public uint DirectoriesLookupOffset { get; set; }
		public uint FileNamesBufferOffset { get; set; }
		public uint FileExtensionsBufferOffset { get; set; }
		public byte[] Reserved { get; set; }
		public PackageFileEntry[] Files { get; set; }

		public PackageDirectoryLookupEntry[] DirectoriesLookup { get; set; }

		public (ushort Start, ushort End)? GetDirectoryFileRange(string directoryPath)
		{
			int index = 0;
			int charIndex = 0;

			do
			{
				PackageDirectoryLookupEntry dir = DirectoriesLookup[index];
				if (directoryPath[charIndex] == dir.NameLetterChar)
				{
					charIndex++;

					if (charIndex >= directoryPath.Length)
						return (dir.FilesStartIndex, dir.FilesEndIndex);

					index = dir.IndexSame;
				}
				else if (directoryPath[charIndex] < dir.NameLetterChar)
				{
					index = dir.IndexLess;
				}
				else if (dir.NameLetterChar < directoryPath[charIndex])
				{
					index = dir.IndexGreater;
				}
			} while (index != 0);

			return null;
		}

		public IEnumerable<(string DirectorPath, ushort Start, ushort End)> GetDirectories()
		{
			var tempData = new List<(string Name, int Index)>();

			string currentName = String.Empty;

			for (int i = 0; i < DirectoriesLookup.Length; i++)
			{
				PackageDirectoryLookupEntry entry = DirectoriesLookup[i];

				if (entry.IndexLess != 0)
					tempData.Add((currentName, entry.IndexLess));

				if (entry.IndexGreater != 0)
					tempData.Add((currentName, entry.IndexGreater));

				currentName += entry.NameLetterChar;

				if (entry.HasFiles)
					yield return (currentName, entry.FilesStartIndex, entry.FilesEndIndex);

				int nextIndex = i + 1;

				for (int r = 0; r < tempData.Count; r++)
				{
					if (tempData[r].Index != nextIndex) 
						continue;
					
					currentName = tempData[r].Name;
					tempData.RemoveAt(r);
				}
			}
		}

		public IEnumerable<(string FilePath, PackageFileEntry FileEntry)> GetFiles()
		{
			foreach (var dir in GetDirectories())
			{
				for (int i = dir.Start; i < dir.End; i++)
				{
					PackageFileEntry file = Files[i];
					yield return ($"{dir.DirectorPath}/{file.FullFileName}", file);
				}
			}
		}

		public PackageFileEntry GetFileEntry(string filePath)
		{
			filePath = filePath.ToLower();

			string dir = Path.GetDirectoryName(filePath).Replace('\\', '/');
			string fileName = Path.GetFileNameWithoutExtension(filePath);
			string ext = Path.GetExtension(filePath).Substring(1);

			var range = GetDirectoryFileRange(dir);

			if (range == null)
				return null;

			for (int i = range.Value.Start; i < range.Value.End; i++)
			{
				PackageFileEntry file = Files[i];

				if (file.FileName != fileName || file.FileExtension != ext)
					continue;

				return file;
			}

			return null;
		}

		public void ExportFiles(string outputDir, Stream packageStream)
		{
			foreach (var file in GetFiles())
			{
				byte[] fileBuffer = new byte[file.FileEntry.FileSize];
				packageStream.Position = file.FileEntry.FileDataOffset;
				int read = packageStream.Read(fileBuffer, 0, fileBuffer.Length);

				if (read != fileBuffer.Length)
					throw new EndOfStreamException();

				string outputFilePath = Path.Combine(outputDir, file.FilePath);
				Directory.CreateDirectory(Path.GetDirectoryName(outputFilePath));
				File.WriteAllBytes(outputFilePath, fileBuffer);
			}
		}

		public void Serialize(IBinarySerializer s)
		{
			s.SerializeMagicString("ZPKG", 4);
			Version = s.Serialize<int>(Version, name: nameof(Version));
			HeaderLength = s.Serialize<uint>(HeaderLength, name: nameof(HeaderLength));
			Files = s.SerializeArraySize<PackageFileEntry, int>(Files, name: nameof(Files));
			DirectoriesLookupOffset = s.Serialize<uint>(DirectoriesLookupOffset, name: nameof(DirectoriesLookupOffset));
			DirectoriesLookup = s.SerializeArraySize<PackageDirectoryLookupEntry, uint>(DirectoriesLookup, name: nameof(DirectoriesLookup));
			FileNamesBufferOffset = s.Serialize<uint>(FileNamesBufferOffset, name: nameof(FileNamesBufferOffset));
			FileExtensionsBufferOffset = s.Serialize<uint>(FileExtensionsBufferOffset, name: nameof(FileExtensionsBufferOffset));
			Reserved = s.SerializeArray<byte>(Reserved, 480, name: nameof(Reserved));
			Files = s.SerializeObjectArray<PackageFileEntry>(Files, onPreSerialize: (x, _) => x.Pre_Package = this, name: nameof(Files));

			s.DoAt(DirectoriesLookupOffset, () => 
				DirectoriesLookup = s.SerializeObjectArray<PackageDirectoryLookupEntry>(DirectoriesLookup, name: nameof(DirectoriesLookup)));
		}
	}
}