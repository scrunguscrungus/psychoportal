﻿namespace PsychoPortal
{
	public class PS2_CompQuat : IBinarySerializable
	{
		public ushort Ushort_00 { get; set; }
		public ushort Ushort_02 { get; set; }
		public ushort Ushort_04 { get; set; }
		
		public void Serialize(IBinarySerializer s)
		{
			Ushort_00 = s.Serialize<ushort>(Ushort_00, name: nameof(Ushort_00));
			Ushort_02 = s.Serialize<ushort>(Ushort_02, name: nameof(Ushort_02));
			Ushort_04 = s.Serialize<ushort>(Ushort_04, name: nameof(Ushort_04));
		}

		public Quat ToQuat()
		{
			// 12 bits per value

			const float factor = 0.0009765625f;

			float x = (Ushort_00 & 0xFFF) * factor - 1.0f;
			float y = (((uint)(Ushort_02 & 0xFF) << 4) | (uint)(Ushort_00 >> 0xC)) * factor - 1.0f;
			float z = (((uint)(Ushort_04 & 0x0F) << 8) | (uint)(Ushort_02 >> 0x8)) * factor - 1.0f;
			float w = (Ushort_04 >> 4) * factor - 1.0f;

			return new Quat(x, y, z, w);
		}
	}
}